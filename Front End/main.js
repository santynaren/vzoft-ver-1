process.env.NODE_ENV = "production";
//handle setupevents as quickly as possible
const setupEvents = require('./installers/setupEvents');
const storage = require('electron-json-storage');
// const es = require("electron-statusbar");
if (setupEvents.handleSquirrelEvent()) {
      // squirrel event handled and app will exit in 1000ms, so don't do anything else
      return;
}
var d = new Date();

const {
      app,
      BrowserWindow,
      dialog,
      netLog,
      Menu
} = require('electron')
const url = require('url')
// const modal = require('electron-modal')
var fs = require('fs');
var imp = 0;
global.path_file = "";
const os = require('os');
const Swal = require('sweetalert2');
const {
      remote
} = require('electron');
// dialog = remote.dialog;
// remote.getCurrentWindow().webContents.getOwnerBrowserWindow().getBounds();
// console.log(remote.getCurrentWindow().webContents.getOwnerBrowserWindow().getBounds());
const {
      ipcMain
} = require('electron');


const electron = require('electron');
const ipc = electron.ipcMain;
const shell = electron.shell;
const path = require('path');
const editJsonFile = require("edit-json-file");
const Menuu = electron.Menu;
const MenuItem = electron.MenuItem;
const {
      ProgId,
      ShellOption,
      Regedit
} = require('electron-regedit');
let win, login



// console.log(app.getPath('temp'));
// module.exports = `file://`+app.getPath('temp')+`/test.json`;


new ProgId({
      description: 'vGeo File',
      icon: './assets/icons/win/icon.ico',
      extensions: ['vg'],
      shell: [
            new ShellOption({
                  verb: ShellOption.OPEN
            }),
            new ShellOption({
                  verb: ShellOption.EDIT,
                  args: ['--edit']
            }),
            new ShellOption({
                  verb: ShellOption.PRINT,
                  args: ['--print']
            })
      ]
});

Regedit.installAll();


let testJsonPath = app.getPath('temp') + `/test.json`;

function createWindow(splashState = false, filePath = testJsonPath) {

      if (splashState == true) {
            newTemp = `${__dirname}/new_file.json`;
            let newTempFile = editJsonFile(newTemp);
            newTest = app.getPath('temp') + `/test.json`;

            var content = JSON.stringify(newTempFile.toObject());
            fs.writeFile(newTest, content, (err) => {
                  if (err) console.log(err);
                  // console.log("REwrited successful");
            });
            // Main Window
            const {
                  width,
                  height
            } = electron.screen.getPrimaryDisplay().workAreaSize;
            win = new BrowserWindow({

                  width,
                  height,
                  title: "vGeo Suite",
                  show: false,
                  icon: path.join(__dirname, 'assets/icons/mac/icon.icns'),
                  icon: path.join(__dirname, 'assets/icons/win/icon.ico'),
                  icon: path.join(__dirname, 'assets/icons/png/icon.png')
            });
            // win.isFullScreenable(false);
            win.setResizable(true);
            win.on('close', function (e) {
                  var choice = require('electron').dialog.showMessageBox(this, {
                        type: 'question',
                        buttons: ['Yes', 'Cancel', 'No'],
                        title: 'Are you sure you want to quit?',
                        message: 'Do you want to save the Changes ?'
                  });
                  if (choice == 2) {

                  } else if (choice == 0) {
                        // dialog.showSaveDialog(win, options, (filename) => {
                        //    console.log(filename)
                        //   })
                        // alert("Saved");
                        Swal.fire(
                              'Saved !',
                              'Your file has been Saved.',
                              'success'
                        )


                  } else {
                        e.preventDefault();
                  }
            });
            // win.setSimpleFullScreen(true);
            // win.isMovable(true);
            const ses = win.webContents.session
            console.log(ses.getUserAgent());
            // Splash Screen Window
            splash = new BrowserWindow({
                  width: 600,
                  height: 377,
                  transparent: true,
                  frame: false,
                  center: true,
                  alwaysOnTop: true,
                  icon: path.join(__dirname, 'assets/icons/png/icon.png')
            });
            splash.loadURL(`file://${__dirname}/splash.html`);

            storage.get('userDetails', function (error, data) {
                  if (error) throw error;

                  // console.log(data);
                  if (data.loggedIn == false) {
                        if (new Date <= new Date(2020, 05, 05)) {
                              win.loadURL(url.format({
                                    pathname: path.join(__dirname, 'login_screen.html'),
                                    protocol: 'file:',
                                    slashes: true
                              }));

                        }
                        //  else {
                        //       win.loadURL(url.format({
                        //             pathname: path.join(__dirname, 'trail.html'),
                        //             protocol: 'file:',
                        //             slashes: true
                        //       }));

                        // }
                  } else if (data.loggedIn == true) {
                        if (new Date <= new Date(2020, 05, 05)) {
                              win.loadURL(url.format({
                                    pathname: path.join(__dirname, 'software_menu.html'),
                                    protocol: 'file:',
                                    slashes: true
                              }));

                        }
                  } else {

                        storage.set('userDetails', {
                              loggedIn: false
                        }, function (error) {
                              if (error) throw error;
                        });
                        if (new Date <= new Date(2020, 05, 05)) {
                              win.loadURL(url.format({
                                    pathname: path.join(__dirname, 'login_screen.html'),
                                    protocol: 'file:',
                                    slashes: true
                              }));

                        }
                  }
            });



            win.once('ready-to-show', () => {
                  setTimeout(function () {

                        splash.destroy();
                        if (new Date <= new Date(2020, 05, 05)) {
                              win.show();
                        } else {

                        }

                        win.webContents.send('store-data', filePath);
                  }, 3000);
            });

      } else {
            newTemp = filePath;
            global.path_file = filePath;
            let newTempFile = editJsonFile(newTemp);
            newTest = testJsonPath;

            var content = JSON.stringify(newTempFile.toObject());
            fs.writeFile(newTest, content, (err) => {
                  if (err) console.log(err);
                  // console.log("REwrited successful");
            });
            // Directly Load without splas Screen
            const {
                  width,
                  height
            } = electron.screen.getPrimaryDisplay().workAreaSize;
            win = new BrowserWindow({

                  width,
                  height,
                  title: "vGeo Suite",
                  show: false,
                  icon: path.join(__dirname, 'assets/icons/mac/icon.icns'),
                  icon: path.join(__dirname, 'assets/icons/win/icon.ico'),
                  icon: path.join(__dirname, 'assets/icons/png/icon.png')
            });
            // win.isFullScreenable(false);
            win.setResizable(true);
            win.on('close', function (e) {
                  var choice = require('electron').dialog.showMessageBox(this, {
                        type: 'question',
                        buttons: ['Yes', 'Cancel', 'No'],
                        title: 'Are you sure you want to quit?',
                        message: 'Do you want to save the Changes ?'
                  });
                  if (choice == 2) {

                  } else if (choice == 0) {
                        // dialog.showSaveDialog(win, options, (filename) => {
                        //    console.log(filename)
                        //   })
                        // alert("Saved");
                        Swal.fire(
                              'Saved !',
                              'Your file has been Saved.',
                              'success'
                        )


                  } else {
                        e.preventDefault();
                  }
            });
            win.loadURL(url.format({
                  pathname: path.join(__dirname, 'login_screen.html'),
                  protocol: 'file:',
                  slashes: true
            }));
            win.webContents.on('did-finish-load', () => {
                  win.webContents.send('store-data', filePath);
            })

      }
      // Create a temp file
      // app.getPath('temp')

      // ! Login Function (commented by pravindia)
      // login = new BrowserWindow({parent: win,width:400,height:300,frame:false});
      // login.loadURL(url.format({
      //    pathname:path.join(__dirname,'login.html'),
      //    protocol:'file',
      //    slashes:true
      // }))

}



app.on('ready', function () {
      createWindow(true);
      const mainMenu = Menu.buildFromTemplate(mainMenuTemplate);
      Menu.setApplicationMenu(mainMenu);


});
app.on('before-quit', () => {
      // var choice = require('electron').dialog.showMessageBox(this,
      //      {
      //        type: 'question',
      //        buttons: ['Yes', 'No'],
      //        title: 'Confirm',
      //        message: 'Are you sure you want to quit?'
      //     });
      //     if(choice == 1){
      //       e.preventDefault();
      //     }
      //  });
      win.removeAllListeners('close');
});
app.on('window-close-all', () => {
      app.quit();
});
// Login
app.on('login', (event, webContents, request, authInfo, callback) => {
      event.preventDefault()
      callback('username', 'secret')
})

const mainMenuTemplate = [{
            label: 'File',
            submenu: [{
                        label: 'New vGeo Project',
                        accelerator: process.platform == 'drawin' ? 'Command+N' : 'Ctrl+N',
                        click() {
                              createWindow(true);
                        }
                  },
                  {
                        label: 'vGeo Homepage',
                        accelerator: process.platform == 'drawin' ? 'Command+N' : 'Ctrl+N',
                        click() {
                              open_home();
                        }
                  },
                  {
                        label: 'Add Stratum',
                        accelerator: process.platform == 'drawin' ? 'Command+N+S' : 'Ctrl+N+S',
                        click() {
                              // open_home();
                        }
                  },
                  {
                        label: 'Open',
                        accelerator: process.platform == 'drawin' ? 'Command+O' : 'Ctrl+O',
                        click() {
                              openFile();
                        }
                  }, {
                        label: 'Project Details',
                        accelerator: process.platform == 'drawin' ? 'Command+D' : 'Ctrl+D',
                        click() {
                              openProject();
                        }
                  },
                  {
                        label: 'Account  Details',
                        accelerator: process.platform == 'drawin' ? 'Command+A' : 'Ctrl+A',
                        click() {
                              accountProfile();
                        }
                  },
                  // {
                  //    label: 'Open Recent',
                  //    accelerator: process.platform == 'drawin' ? 'Command+Alt+O' : 'Ctrl+Alt+O',
                  // },
                  {
                        label: 'Save As',
                        accelerator: process.platform == 'drawin' ? 'Command+S+A' : 'Ctrl+S+A',
                        click() {
                              // saveChanges();
                              win.webContents.send('save', 2);
                        }
                  },
                  {
                        label: 'Save',
                        accelerator: process.platform == 'drawin' ? 'Command+S' : 'Ctrl+S',
                        click() {
                              win.webContents.send('save', 1);
                              // alert("Saved");
                              Swal.fire(
                                    'Saved !',
                                    'Your file has been Saved.',
                                    'success'
                              )
                        }
                  },
                  // {
                  //    label: 'Save Workspace as',
                  //    accelerator: process.platform == 'drawin' ? 'Command+Shift+S' : 'Ctrl+Shift+S',

                  // },
                  {
                        label: 'Quit',
                        accelerator: process.platform == 'drawin' ? 'Command+E' : 'Ctrl+Q',
                        click() {
                              app.quit();
                        }
                  }
            ]
      },
      {
            label: 'View',
            submenu: [{
                        role: 'resetzoom'
                  },
                  {
                        role: 'zoomin'
                  },
                  {
                        role: 'zoomout'
                  },
                  {
                        role: 'togglefullscreen'
                  }
            ]
      },
      {
            role: 'window',
            submenu: [{
                        role: 'minimize'
                  },
                  {
                        role: 'close'
                  }
            ]
      },
      {
            role: 'help',
            submenu: [{
                  label: 'vGeo Help',
                  click() {
                        require('electron').shell.openExternal('https://www.vzoft.com')
                  }
            }]
      }
];

if (process.env.NODE_ENV !== 'production') {
      mainMenuTemplate.push({

            label: 'Smazee Tools',
            submenu: [{
                        label: 'Reload',
                        accelerator: process.platform == 'drawin' ? 'Command+R' : 'Ctrl+R',
                        role: 'reload'
                  },
                  {
                        label: 'Developer Window',
                        accelerator: process.platform == 'drawin' ? 'Command+Shift+I' : 'Ctrl+Shift+I',
                        click(item, focusedWindow) {
                              focusedWindow.toggleDevTools();
                        }
                  }
            ]

      });
}



function open_home() {

      win.loadURL(url.format({
            pathname: path.join(__dirname, 'software_menu.html'),
            protocol: 'file:',
            slashes: true
      }));
}

function saveChanges() {

      dialog.showSaveDialog({
            title: "Save As",
            filters: [{
                  name: "vGeo",
                  extensions: ['vg']
            }]
      }, function (fileName) {

            if (fileName === undefined) {
                  console.log("File Not Saved! Enter a valid file name");
                  return;
            }
            // var content = document.getElementById('content').value;

            let file = editJsonFile(testJsonPath);
            if (file.get("saved") == false) {
                  // file.set("saved", true);
            } else {
                  // TODO Encoding and Decoding !
                  // console.log(Buffer.from('Hello World!').toString('base64'));
                  // console.log(Buffer.from(b64Encoded, 'base64').toString());

            }
            var content = JSON.stringify(file.toObject());
            fs.writeFile(fileName, content, (err) => {
                  if (err) console.log(err);
            })

      });
}

function openProject() {
      win.loadURL(url.format({
            pathname: path.join(__dirname, 'software_menu.html'),
            protocol: 'file:',
            slashes: true
      }));

      // location.href = "index.html";
}

function accountProfile() {
      win.loadURL(url.format({
            pathname: path.join(__dirname, 'login_screen.html'),
            protocol: 'file:',
            slashes: true
      }));
}

function openFile() {
      //      alert("dfasdfads");

      const files = dialog.showOpenDialog(win, {
            properties: ['openFile'],
            filters: [{
                        name: 'vGeo Files',
                        extensions: ['json', 'vg']
                  } // TODO Our file type name

            ]
      });
      if (!files) return;

      const filePath = files[0];
      const fileContent = fs.readFileSync(filePath).toString(); // Do read all datas and Store in localhost
      // updateRecents(filePath);
      app.addRecentDocument(filePath);
      // alert(filePath);
      createWindow(false, filePath);
      // opWindow(false, filePath);




}

function stratumNew() {
      win.loadURL(url.format({
            pathname: path.join(__dirname, 'stratum.html'),
            protocol: 'file:',
            slashes: true
      }));
}


function updateRecents(path, clear = false) {
      const currentMenu = mainMenuTemplate;
      if (!currentMenu) return;

      const recents = getItemByKey(currentMenu, 'recents');
      if (!recents) return;

      // Clear menu if requested.
      if (clear) {
            config.set('recentDocuments', []);
            recents.submenu.clear();
            recents.submenu.append(new MenuItem({
                  key: 'null',
                  label: 'No Recent Documents',
                  enabled: false
            }));
            Menu.setApplicationMenu(currentMenu);
            return;
      }

      const item = new MenuItem({
            label: require('path').basename(path),
            click: () => this.open(path)
      });

      // If first recent item clear empty placeholder.
      if (recents.submenu.items[0].key == 'null') {
            recents.submenu.clear();
            recents.submenu.append(item);
      }
      // Limit to maximum 10 recents.
      else if (recents.submenu.items.length >= 10) {
            const items = recents.submenu.items;
            recents.submenu.clear();
            items.push(item);
            items.slice(10).forEach((i) => recents.submenu.append(i));
      }
      // Otherwise just add item.
      else recents.submenu.append(item);

      // Update application menu.
      Menu.setApplicationMenu(currentMenu);
}





ipcMain.on('request-mainprocess-action', (event) => {
      var pathz = saveChanges();

});
ipcMain.on("setPath", (event, val) => {
      global.path_file = val;
});

ipc.on('print', function (event, val) {

      dialog.showSaveDialog({
            title: "Export Report as PDF",
            filters: [{
                  name: 'PDF',
                  extensions: ['pdf']
            }]
      }, function (filename) {
            if (filename === undefined) {

return;

            } else {


                  const pdfPath = path.join(filename);
                  const win = BrowserWindow.fromWebContents(event.sender);
                  win.webContents.printToPDF({
                        marginsType: 1,
                        pageSize: val,
                        landscape: false
                  }, function (error, data) {
                        if (error) {
                              return console.log(error.message);

                        } else {
                              fs.writeFile(pdfPath, data, function (err) {
                                    if (err) return console.log(err.message);
                                    shell.openExternal('file://' + pdfPath);

                              });

                        }
                  })
            }
      });

});
// win.webContents.on('did-finish-load', () => {
//    win.webContents.printToPDF({ marginsType:2, pageSize:"A3", landscape:false }, (error, data) => {
//        if (error) throw error
//        fs.writeFile('output.pdf', data, (error) => {

//        //getTitle of Window
//        console.log(win.webContents.getTitle())

//        //Silent Print

//        if (error) throw error
//        console.log('Write PDF successfully.')
//        })
//    })
// })
