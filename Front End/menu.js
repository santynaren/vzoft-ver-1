
const mainMenuTemplate = [{
      label: 'File',
      submenu: [{
            label: 'New ParaSoil Project',
            accelerator: process.platform == 'drawin' ? 'Command+N' : 'Ctrl+N',
            click() {
               createWindow();
            }
         },
         {
            label: 'Open',
            accelerator: process.platform == 'drawin' ? 'Command+O' : 'Ctrl+O',
            click() {
               openFile();
            }
         }, {
            label: 'Project Details',
            accelerator: process.platform == 'drawin' ? 'Command+D' : 'Ctrl+D',
            click() {
               openProject();
            }
         },
         // {
         //    label: 'Open Recent',
         //    accelerator: process.platform == 'drawin' ? 'Command+Alt+O' : 'Ctrl+Alt+O',
         // },
         {
            label: 'Save',
            accelerator: process.platform == 'drawin' ? 'Command+S' : 'Ctrl+S',
            click() {
               saveChanges();
            }
         },
         // {
         //    label: 'Save Workspace as',
         //    accelerator: process.platform == 'drawin' ? 'Command+Shift+S' : 'Ctrl+Shift+S',

         // },
         {
            label: 'Quit',
            accelerator: process.platform == 'drawin' ? 'Command+E' : 'Ctrl+Q',
            click() {
               app.quit();
            }
         }
      ]
   },
   {
      label: 'View',
      submenu: [{
            role: 'resetzoom'
         },
         {
            role: 'zoomin'
         },
         {
            role: 'zoomout'
         },
         {
            role: 'togglefullscreen'
         }
      ]
   },
   {
      role: 'window',
      submenu: [{
            role: 'minimize'
         },
         {
            role: 'close'
         }
      ]
   },
   {
      role: 'help',
      submenu: [{
         label: 'Help',
         click() {
            require('electron').shell.openExternal('https://www.letspile.com');
         }
      }]
   }
];

if (process.env.NODE_ENV !== 'production') {
   mainMenuTemplate.push({

      label: 'Smazee Tools',
      submenu: [{
            label: 'Reload',
            accelerator: process.platform == 'drawin' ? 'Command+R' : 'Ctrl+R',
            role: 'reload'
         },
         {
            label: 'Developer Window',
            accelerator: process.platform == 'drawin' ? 'Command+Shift+I' : 'Ctrl+Shift+I',
            click(item, focusedWindow) {
               focusedWindow.toggleDevTools();
            }
         }
      ]

   });
}

function updateRecents(path, clear = false) {
   const currentMenu = mainMenuTemplate;
   if (!currentMenu) return;

   const recents = getItemByKey(currentMenu, 'recents');
   if (!recents) return;

   // Clear menu if requested.
   if (clear) {
      config.set('recentDocuments', []);
      recents.submenu.clear();
      recents.submenu.append(new MenuItem({
         key: 'null',
         label: 'No Recent Documents',
         enabled: false
      }));
      Menu.setApplicationMenu(currentMenu);
      return;
   }

   const item = new MenuItem({
      label: require('path').basename(path),
      click: () => this.open(path)
   });

   // If first recent item clear empty placeholder.
   if (recents.submenu.items[0].key == 'null') {
      recents.submenu.clear();
      recents.submenu.append(item);
   }
   // Limit to maximum 10 recents.
   else if (recents.submenu.items.length >= 10) {
      const items = recents.submenu.items;
      recents.submenu.clear();
      items.push(item);
      items.slice(10).forEach((i) => recents.submenu.append(i));
   }
   // Otherwise just add item.
   else recents.submenu.append(item);

   // Update application menu.
   Menu.setApplicationMenu(currentMenu);
}

