   // General Variable Declaration
   var ipcRenderer = require('electron').ipcRenderer;
   var editJsonFile = require("edit-json-file");
   var section_num_val;
   var remote = require('electron').remote;
   const app = remote.app;
   var Swal = require("sweetalert2");
   var del_section_arr = [];
  //  var tempFilePath = app.getPath('temp');
  var tempFilePath = remote.getGlobal('path_file');
   var Color = ["#aeabab", "#ffff00", "#9cc2e5", "#c55a11"];
   var myData = new Array();
   var val = new Array();
   var sections = new Array();
   var section_list = new Array();
   var section_list_data = new Array();
   var borehole_count;
   var line_count = 0;
   var i;
   var ctx1;
   var curr_sec;
   var del = false;
   var section_number;
   var design_array_section = new Array();
   design_array_section[0] = "Null Section";
   var chart_two;
   var strata_name_array = [];
   strata_name_array[0] = "Null Section";
   var design_strata = new Array();
   var click_gm = 0;
   let file = editJsonFile(tempFilePath);
   var project_title = document.getElementById('projectTitle');
   var project_name = file.get("projectName");
   var project_date = file.get("dateCreated");
   // project_title.innerHTML = "" + project_name + ".para | " + project_date + "";
   document.title = "" + project_name + " | Sigma Soil V 2019.2.0.1 | Let's Pile ";
   var type_of_mode = file.get("projectDetails.typeOfMode");
   var save_bh_count = file.get("soilData.noOfBh");
   const prompt = require('electron-prompt');
   var section_count = file.get("soilData.sectionData.noOfSection");
   // alert(section_count);
   console.log(section_count);
   var menu_div = document.getElementById("menu_f");
   var line_count = 0;
   var sec; // Groundmodel here
   sec = 0;
   var item, prevsec = 0;
   var oldsec, newsec;
   var section_array = new Object();
   var points = [];
   var arr = new Array();
   var stratum_defined_list = ["Made Ground", "Rock","Clay",
"Sand",
"Gravel",
"Silt",
"Chalk",
"Limestone",
"Mudstone",
"Sandstone",
"Void",
"Flint",
"Peat"];
   var lc_arr = new Array();
   const {Menu, MenuItem} = remote

    const menu = new Menu()
    // menu.append(new MenuItem({label: 'Section Tools',
    // submenu: [
    //    { 'label': 'Add Section Line'  },
    //    { 'label': ' Line'  }
    //  ]
    //  }))
    menu.append(new MenuItem({label: 'Add Ground Model',
   click(){
       section_fun()
   }}))

    menu.append(new MenuItem({label: 'Delete  Borehole in Ground Model',
   click(){
    del_section_var()
   }}))

   menu.append(new MenuItem({type: 'separator'}))
   menu.append(new MenuItem({label: 'Show Tour',
   click(){
    introJs().start();
   }}))
   menu.append(new MenuItem({type: 'separator'}))
    // Prevent default action of right click in chromium. Replace with our menu.
    window.addEventListener('contextmenu', (e) => {
       e.preventDefault()
       menu.popup(remote.getCurrentWindow())
    }, false)
   // To check the Ground Model Saved or Not
   if (file.get("soilData.groundModelData.save") == 0) {
     var sec = 0;
   } else {
     var sec = file.get("soilData.groundModelData.noOfGm");
     //  alert(sec);
     $(document).ready(function () {
       // your code

       load_section();
     });

   }
   // Add Ground Model Function Calls
   function section_fun() {

    if(del_section_arr.length == 0){
      sec = sec + 1;
      var  curr = sec;
      // alert(sec);
      menu.append(new MenuItem({'label': 'Ground Model '+sec,
         click(){
            sec_call(''+curr+'')
         },
         'id':curr,


         type: 'checkbox', checked: false
     }))
      section_array[sec] = new Array();
      console.log(section_array);
      var sec_tool = document.getElementById("section_toolbar");
     //  var sec_but = document.createElement("Button");
     //  sec_but.setAttribute("class", "btn btn-outline-success");
     //  sec_but.setAttribute("style", "margin: 10px");
     //  sec_but.setAttribute("onclick", "sec_call(" + sec + ",this)");
     //  var sec_but_text = document.createTextNode("Ground Model " + sec);
     //  sec_but.appendChild(sec_but_text);
     //  sec_tool.appendChild(sec_but);


      var div_section_tool = document.createElement("div");
      div_section_tool.setAttribute("class", "btn-group ");
      div_section_tool.setAttribute("style", "margin-left:10px;margin-top:5px;");
      div_section_tool.setAttribute("role", "group");
      div_section_tool.setAttribute("id", "sec_div_"+sec);

      var sec_but = document.createElement("Button");
      // sec_but.setAttribute("type", "button");
      sec_but.setAttribute("class", "btn btn-outline-success");
      sec_but.setAttribute("id", "gm_but_" + sec);
      // sec_but.setAttribute("style", "");
      // Section is Called Here
      sec_but.setAttribute("onclick", "sec_call(" + sec + ",this)");
      // sec_but.setAttribute("data-tooltip", "Click Here to View Borehole of Section" + sec + " ");
      // sec_but.setAttribute("data-tooltip-position", "top");

      var sec_but_text = document.createTextNode("G" + sec);

      var del_sec_but = document.createElement("button");
      // del_sec_but.setAttribute('type', 'button');
      del_sec_but.setAttribute('class', 'btn  btn-outline-success');
      del_sec_but.setAttribute('onclick', 'delete_section(this,' + sec + ')');
      del_sec_but.innerHTML = '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path fill="#555" d="M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zM19 4h-3.5l-1-1h-5l-1 1H5v2h14V4z"/><path d="M0 0h24v24H0z" fill="none"/></svg>'

      var add_sec_line_but = document.createElement("img");
      // add_sec_line_but.setAttribute('type', 'button');
      add_sec_line_but.setAttribute('class', 'btn btn-outline-success ');
      add_sec_line_but.setAttribute('style', 'width:50px;height :50px;');
      add_sec_line_but.setAttribute('src', './assets/img/add_line.svg');
      add_sec_line_but.setAttribute('onclick', 'add_sec_line(this,' + sec + ')');
      // del_sec_but.innerHTML = '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path fill="#555" d="M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zM19 4h-3.5l-1-1h-5l-1 1H5v2h14V4z"/><path d="M0 0h24v24H0z" fill="none"/></svg>'

      sec_but.appendChild(sec_but_text);


      div_section_tool.appendChild(sec_but);
     //  div_section_tool.appendChild(add_sec_line_but);
      div_section_tool.appendChild(del_sec_but);
      sec_tool.appendChild(div_section_tool);
    }else{

      var min_del = Math.min(...del_section_arr);
      var min_del_index = del_section_arr.indexOf(min_del);
      del_section_arr.splice(min_del_index,1);




      // sec = sec + 1;
      var  curr = min_del;
      // alert(sec);
      menu.append(new MenuItem({'label': 'Ground Model '+min_del,
         click(){
            sec_call(''+curr+'')
         },
         'id':curr,


         type: 'checkbox', checked: false
     }))
      section_array[min_del] = new Array();
      console.log(section_array);
      var sec_tool = document.getElementById("section_toolbar");
     //  var sec_but = document.createElement("Button");
     //  sec_but.setAttribute("class", "btn btn-outline-success");
     //  sec_but.setAttribute("style", "margin: 10px");
     //  sec_but.setAttribute("onclick", "sec_call(" + sec + ",this)");
     //  var sec_but_text = document.createTextNode("Ground Model " + sec);
     //  sec_but.appendChild(sec_but_text);
     //  sec_tool.appendChild(sec_but);


      var div_section_tool = document.createElement("div");
      div_section_tool.setAttribute("class", "btn-group ");
      div_section_tool.setAttribute("style", "margin-left:10px;margin-top:5px;");
      div_section_tool.setAttribute("role", "group");
      div_section_tool.setAttribute("id", "sec_div_"+min_del);

      var sec_but = document.createElement("Button");
      // sec_but.setAttribute("type", "button");
      sec_but.setAttribute("class", "btn btn-outline-success");
      sec_but.setAttribute("id", "gm_but_" + min_del);
      // sec_but.setAttribute("style", "");
      // Section is Called Here
      sec_but.setAttribute("onclick", "sec_call(" + min_del + ",this)");
      // sec_but.setAttribute("data-tooltip", "Click Here to View Borehole of Section" + sec + " ");
      // sec_but.setAttribute("data-tooltip-position", "top");

      var sec_but_text = document.createTextNode("G" + min_del);

      var del_sec_but = document.createElement("button");
      // del_sec_but.setAttribute('type', 'button');
      del_sec_but.setAttribute('class', 'btn  btn-outline-success');
      del_sec_but.setAttribute('onclick', 'delete_section(this,' + min_del + ')');
      del_sec_but.innerHTML = '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path fill="#555" d="M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zM19 4h-3.5l-1-1h-5l-1 1H5v2h14V4z"/><path d="M0 0h24v24H0z" fill="none"/></svg>'

      var add_sec_line_but = document.createElement("img");
      // add_sec_line_but.setAttribute('type', 'button');
      add_sec_line_but.setAttribute('class', 'btn btn-outline-success ');
      add_sec_line_but.setAttribute('style', 'width:50px;height :50px;');
      add_sec_line_but.setAttribute('src', './assets/img/add_line.svg');
      add_sec_line_but.setAttribute('onclick', 'add_sec_line(this,' + min_del + ')');
      // del_sec_but.innerHTML = '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path fill="#555" d="M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zM19 4h-3.5l-1-1h-5l-1 1H5v2h14V4z"/><path d="M0 0h24v24H0z" fill="none"/></svg>'

      sec_but.appendChild(sec_but_text);


      div_section_tool.appendChild(sec_but);
     //  div_section_tool.appendChild(add_sec_line_but);
      div_section_tool.appendChild(del_sec_but);
      sec_tool.appendChild(div_section_tool);
    }




   }
   // Load Ground Model for Saved File


   // Get GM for New file or new GM Created
   function get_sec() {

     for (var l = 1; l <= sec; l++) {
       // alert(section_array[l]);
       file.set("soilData.groundModelData.gm" + l + "", section_array[l]);
       // alert("Options selected are " + str);
       file.save();
     }


   }
   // Get GM for New file or new GM Created

   function sec_call(sec_key,el) {
    //  el.classList.add("active");
    // console.log(document.getElementById("section_toolbar"));
    var div_but = document.getElementById("section_toolbar");
  var buts = div_but.getElementsByClassName("btn btn-outline-success");
    // console.log(el);
    for (var i = 0; i < buts.length; i++) {
      console.log(buts[i]);
      buts[i].addEventListener("click", function() {
      var current = document.getElementsByClassName("active");
      current[0].className = current[0].className.replace(" active", "");
      this.className += " active";
      });
    }
     var sec_det = document.getElementById("section_details");
     sec_det.innerHTML = "Ground Model " + sec_key + ": " + section_array[sec_key] + "";
     curr_sec = sec_key;

    //  alert("Selected :" + curr_sec);
   }

   // GM Call for Saved file for Borehole Included
   function sec_call_load(section_Arr_load, sec_key_id) {
     var sec_det = document.getElementById("section_details");
     console.log(section_Arr_load);
     // section_array[sec_key] = new Array();
     console.log(section_array[sec_key_id]);
     sec_det.innerHTML = "Ground Model " + sec_key_id + ": " + section_array[sec_key_id] + "";
     curr_sec = sec_key_id;
    //  alert("Selected :" + curr_sec);
   }
   // Displaying Section from previous Screen
   function get_section() {

     for (var f = 1; f <= section_count; f++) {
       // section_list_data = localStorage.getItem("select_" + f + "");
if(file.get("soilData.sectionData.section_" + f) == null){
  continue;
}else{
  var section_button = document.createElement("BUTTON");

  section_button.setAttribute("class", " btn btn-outline-warning");
  section_button.setAttribute("style", "margin-top : 10px;margin-left:10px");
  section_button.setAttribute("onclick", "display_section('" + f + "')");

  var section_text = document.createTextNode("Section " + f);
  section_button.appendChild(section_text);
  document.getElementById("menu_f").appendChild(section_button);
}

       // console.log(section_list_data);
     }
     introJs().start();
     var div_but = document.getElementById("menu_f");
     var buts = div_but.getElementsByClassName("btn btn-outline-warning");
       // console.log(el);
       for (var i = 0; i < buts.length; i++) {
         console.log(buts[i]);
         buts[i].addEventListener("click", function() {
         var current = document.getElementsByClassName("active");
         current[0].className = current[0].className.replace(" active", "");
         this.className += " active";
         });
       }
    }
   // On click the Graph with Corresponding Boreholes appears
   function display_section(section_number) {
     //  alert(section_number);
     section_list_data = file.get("soilData.sectionData.section_" + section_number);
     console.log(section_list_data);

     // sections = JSON.parse(section_list_data);
     // console.log(sections);

     $.myjQuery(section_number);
     if (file.get("groundModelLines.save") == 0) {
       // var arr = new Array();
     } else {

       arr = file.get("groundModelLines.Coordinates");
       // lc_arr = file.get("sectionLines.Coordinates");
       line_count = file.get("groundModelLines.lastLineNo");
       console.log(arr);
       drawdefinedLine(chart_two, file.get("groundModelLines.total"));
     }
     design_array_section[sec] = new Array();
     strata_name_array[sec] = new Array();
   }



   // Erase Selected Line
   function eraseSel() {

     var disp = document.getElementById("displayData");

     var tn = document.createTextNode("Enter the line number that you want to erase");

     var ip = document.createElement("input");
     ip.setAttribute("id", "ln");
     ip.setAttribute("type", "number");
     ip.setAttribute("value", "");

     disp.appendChild(tn);
     disp.appendChild(ip);

     var button = document.createElement('button');
     button.setAttribute('type', 'button');
     button.setAttribute('id', 'btn');
     button.innerHTML = "remove";
     button.setAttribute('onclick', 'remove()');

     disp.appendChild(button);
   }
   // Load GM
   function load_section() {
     for (var i = 1; i <= sec; i++) {
       console.log(file.get("soilData.groundModelData.gm" + i));
       section_fun_load(i, file.get("soilData.groundModelData.gm" + i));
       // section_array[i] = new Array();
     }

   }
   // To Delete Lines Not Needed
   function remove() {

     var l_num = document.getElementById("ln").value;
     console.log(l_num);
     //  alert(l_num);
     // var removed = arr.splice(l_num - 1, l_num - 1);
     var coords = file.get("groundModelLines.Coordinates.gm" + curr_sec);
     // var removed = arr.splice(l_num - 1, l_num - 1);
     for (var i = 0; i < coords.length; i++) {
       if (coords[i].line_no == l_num) {
         coords.splice(i, 1);
         console.log(coords);
       }
     }
     // if(l_num == 1) {
     //     var rmv = arr.splice(l_num - 1,l_num);
     // }

     // else {
     //     rmv = arr.splice(l_num - 1,1);
     // }
     // console.log(rmv);
     console.log(arr);
     var remove_val = file.get("groundModelLines.total");

     file.set("groundModelLines.Coordinates.gm" + curr_sec, coords);
     file.set("groundModelLines.total", remove_val - 1);
     file.save();

     //  getLines();

   }
   // Get Lines to Verify New file or Saved File
   function getLines() {
     chart_two.render();
     if (file.get("groundModelLines.save") == 0) {
       // var arr = new Array();
     } else {
       // var arr = new Array();
       arr = file.get("groundModelLines.Coordinates");
       console.log(arr);
       drawdefinedLine(chart, file.get("groundModelLines.total"));
     }

   }


   // Old Code
   myData = localStorage.getItem(myData);
   console.log(section_list_data);
   console.log(myData);
   //  i = localStorage.getItem(borehole_count);
   // console.log(i);
   if (myData) {
     val = JSON.parse(myData);
   }



   i = val[0];

   console.log(sections);

   function myFunction() {
     click_gm++;
     // alert(click_gm);
     var myDiv = document.getElementById("demo");

     //Create array of options to be added
     var array = new Array();
     for (var j = 1; j <= save_bh_count; j++) {
       array.push("Borehole " + j);
       // console.log(val[j]);
     }
     // console.log(val[1]);
     //Create and append select list
     var selection_id = "select_" + click_gm + "";
     var selectList = document.createElement("select");
     selectList.setAttribute("multiple", "multiple");
     selectList.setAttribute("name", "selectlist");
     selectList.setAttribute("class", "col-sm-2 form-control");
     selectList.setAttribute("id", selection_id);
     myDiv.appendChild(selectList);
     var btn = document.createElement("BUTTON");
     btn.setAttribute("onclick", "printAll('" + selection_id + "')"); // Create a <button> element
     btn.setAttribute("class", "btn btn-primary"); // Create a <button> element
     var t = document.createTextNode("Group this for Groundmodel " + click_gm + ""); // Create a text node
     btn.appendChild(t); // Append the text to <button>
     myDiv.appendChild(btn);
     //Create and append the options
     for (var m = 0; m < save_bh_count; m++) {
       var option = document.createElement("option");

       option.value = array[m];
       option.text = array[m];
       selectList.appendChild(option);
     }
   }
   // Design Screen Transition
   function sixth() {
     // for(var i =  0 ; )
     get_sec();
     // file.set("designData.stratumData", design_array_section);
     // file.set("designData.strataName", strata_name_array);
     file.set("soilData.groundModelData.noOfGm", sec);
     file.set("soilData.groundModelData.save", 1);

     file.save();
     console.log(file.get("soilData.groundModelData.noOfGm"));
     // alert(file.get("designData.stratumData.designData.1.ref"));
     location.href = "fifth_design.html";

   }
   // old COde
   function printAll(num_selection) {

     var str = "";
     var i;
     var bh_gm_arr = new Array();
     var gm = [];
     var mySelect = document.getElementById(num_selection);
     // section.push("1");
     // var arr_+click_Section_number = new Array();
     for (i = 0; i < mySelect.options.length; i++) {
       if (mySelect.options[i].selected) {
         str = str + i + " ";
         gm.push(i + 1);
         var s = i + 1;
         bh_gm_arr.push("bh_" + s);
       }
     }
     console.log(gm);
     console.log(bh_gm_arr);
     file.set("soilData.groundModelData.gm" + click_gm + "", bh_gm_arr);
     // alert("Options selected are " + str);
     file.save();

     // alert("Options selected are " + str);
   }


   // console.log(myData[2]);
   // localStorage.removeItem( 'objectToPass' ); // Clear the localStorage
   // var firstData = myData[0];
   // var secondData = myData[1];
   // alert('firstData: ' + firstData + '\nsecondData: ' + secondData);
   const CanvasJS = require('./assets/javascript/canvasjs.min.js');
   // const JQuery =  require('./assets/javascript/jquery.min.js');
   // var one = val[0];
   $.myjQuery = function (section_num) {
     var dps = []; // dataPoints
     var s_one = []; // dataPoints
     var s_two = []; // dataPoints
     var s_three = []; // dataPoints
     var strips = [];
     for (var j = 1; j <= i; j++) {
       dps.push({
         label: String(val[j]),
         x: parseInt(val[j + i], 10),
         y: parseInt(val[j + 2 * i], 10)
       });

     }
     var y_data = [];
     var save_data = new Array();
     // Chart for Selecting Ground Model
     chart_two = new CanvasJS.Chart("chartContainer", {
       animationEnabled: true,
       dataPointWidth: 15,
       //  title: {
       //    text: "Borehole  Cross Section",
       //    fontFamily: "Roboto",
       //    fontWeight: 'bold',
       //    fontSize : 12,
       //   //  padding: 10,
       //    horizontalAlign: "center"
       //  },
       axisX2: {
        title: "Chainage",
        titleFontSize: 12,
        labelFontSize: 8,
        titleFontStyle: "oblique",
        titleFontColor: "black",
        labelFontColor: "black",
        // minimum : 0,
        //   maximum: 30000000,
        // title: "Eastings",
        // interval: 1500000,
        intervalType: "number",
        valueFormatString: "#,##0.##",
        gridDashType: "dot",
        gridThickness: 0.1,
        crosshair: {
            enabled: true //disable here
        }
       },
       axisY: y_data,
       toolTip: {
         content: "{name_to_call} <br>{name} in {label}<br> (Start,End) : {y}"
       },
       legend: {
         cursor: "crosshair"
       },
       data: save_data

     });
     if (type_of_mode == "mbgl") {
       y_data.push({
         'stripLines': strips,
         'title': "Depth ( m bgl )",
         'titleFontSize': 12,
         'labelFontSize': 8,
         'titleFontStyle': "oblique",
         'titleFontColor': "black",
         'labelFontColor': "black",
         'includeZero':false,
         // minimum : 0,
         //   maximum: 30000000,
         // title: "Eastings",
         // interval: 1500000,
         'intervalType': "number",
         'valueFormatString': "#,##0.##",
         'gridDashType': "dot",
         'gridThickness': 0.1,
         'crosshair': {
           enabled: true //disable here
         }, 'reversed': true
       });
     } else {
       y_data.push({
         'stripLimes': strips,
         'title': "Elevation ( mAOD )",
         'titleFontSize': 12,
         'labelFontSize': 8,
         'titleFontStyle': "oblique",
         'titleFontColor': "black",
         'labelFontColor': "black",
         'includeZero':false,
         // minimum : 0,
         //   maximum: 30000000,
         // title: "Eastings",
         // interval: 1500000,
         'intervalType': "number",
         'valueFormatString': "#,##0.##",
         'gridDashType': "dot",
         'gridThickness': 0.1,
         'crosshair': {
           enabled: true //disable here
         },  'reversed': false

       });
     }

     function set(y1, stra, sec) {
       // alert(sec + stra + y1);
       //  design_strata[section_num_val] = [];
       var stratum = String(stra);
       // design_strata[section_num_val][stratum] =  [];
       // design_strata[section_num_val][stratum].push(y1);

       // alert(section_number);
       design_array_section[sec].push({
         [stratum]: y1
       });
       strata_name_array[sec].push(stratum);
       console.log(design_strata);
       console.log(strata_name_array);
       console.log(design_array_section);

     }

     var strata_name_dps = new Array();
     var gw_name_dps = new Array();
     save_data.push({
       'type': 'rangeColumn',
       'axisXType': "secondary",
       'cursor': 'crosshair',

       'LegendText': 'Stratum',
       'click': gm_divide,
       'dataPoints': strata_name_dps
     });
     save_data.push({
       'type': 'scatter',
       'lineDashType': 'dash',
       'lineColor': 'blue',
       'markerType': 'none',
       'markerColor': '#0000ff',
       // 'animationEnabled': true,
       'axisXType': "secondary",

       'LegendText': 'Ground Water Level',
       'dataPoints': gw_name_dps
     });
     for (var g = 1; g <= file.get("soilData.sectionData.section_" + section_num + ".length"); g++) {
       var jj = g - 1;
       var bh = file.get("soilData.sectionData.section_" + section_num + "." + jj);
       console.log(file.get("soilData.distanceData.section_" + section_num + "." + jj));
       // var bh = bh_name.jj;

       for (var f = 3; f < file.get("soilData.params." + bh + ".length"); f++) {

         var strata_name = file.get("soilData.params." + bh + "." + f);
         // alert( bh + "'s" + strata_name);
         // var next = f + 1;
         // if (next < file.get("soilData.noOfParams")) {
         // var value1, value2;
         // var strata_name2 = file.get("soilData.params." + next);

         // alert(bh);
         var val1 = file.get("soilData." + bh + ".stratumData." + strata_name + ".start");
         var val2 = file.get("soilData." + bh + ".stratumData." + strata_name + ".end");
         // if (val1 == 0) {
         //   value1 = 0;
         // } else {
         //   value1 = 100 - val1;
         // }
         // if (val2 == 0) {
         //   value2 = 0;
         // } else {
         //   value2 = 100 - val2;
         // }
         // alert(bh);
         //  console.log(bh);
         console.log(file.get("soilData.distanceData.section_" + section_num + "." + bh))
         strata_name_dps.push({

          // indexLabel: String(bh),
           name_to_call: String(bh),
           name: strata_name,

           x: parseInt(file.get("soilData." + bh + ".stratumData.Eastings"), 10),
           //  x: file.get("sectionLines.data.section_"+section_num+"."+bh),
           //  x: file.get("sectionLines.data.section_"+section_num+"."+bh),
           y: [parseInt(val1, 10), parseInt(val2, 10)],
           color: file.get("soilData." + bh + ".stratumData." + strata_name + ".strataColor")

         });
         //  console.log
         // } else {

         // alert(bh);
         // var val1 = file.get("soilData." + bh + ".stratumData." + strata_name);
         // var val2 = file.get("soilData." + bh + ".stratumData." + strata_name2);
         // alert(bh);
         // if (val1 == 0) {
         //   value1 = 0;
         // } else {
         //   value1 = 100 - val1;
         // }
         // var f = parseInt(val1) + 10;
        //  strata_name_dps.push({
          //  indexLabel: String(bh),
          //  y: [parseInt(val1, 10), f]
        //  });
         // }




       }
       for (var f = 3; f < file.get("soilData.noOfParams"); f++) {
         // var jj = g - 1;
         // var bh = file.get("soilData.boreholeArray." + jj);
         // for (var f = 3; f < file.get("soilData.noOfParams"); f++) {
         var pos_x = parseInt(file.get("soilData." + bh + ".stratumData.Eastings"), 10);
         var val_x = pos_x;
         // var strata_name = file.get("soilData.params." + f);

         // var val1 = file.get("soilData." + bh + ".stratumData." + strata_name + ".start");
         // var val2 = file.get("soilData." + bh + ".stratumData." + strata_name + ".end");

         gw_name_dps.push({


           // name: strata_name,
           //  x: file.get("sectionLines.data.section_"+section_num+"."+bh),
           x: pos_x,
           y: parseInt(file.get("soilData." + bh + ".stratumData.Groundwater"), 10),
           markerImageUrl: "./assets/img/1.png"
           // color: Color[f - 3]
         });
         //  }
       }

       // console.log(file.get("soilData.bh_" + v + ".stratumData.soilData.params." + f));
     }

     if (file.get("groundModelLines.save") == 0) {

     } else {
       for (var design_strata_index = 0; design_strata_index < file.get("soilData.groundModelData.noOfGm"); design_strata_index++) {

         var dum = design_strata_index + 1;
         for (var d_s = 0; d_s < file.get("groundModelLines.Coordinates.gm" + curr_sec + ".length"); d_s++) {
           // var design_Strata = file.get("designData.strataName." + current_gm + "." + d_s);

           drawDesignLine(chart_two, file.get("groundModelLines.Coordinates.gm" + curr_sec + "." + d_s));

           // console.log(file.get("designData.stratumData." + current_gm + "." + d_s + "." + design_Strata));
         }

       }
     }


     chart_two.render();
     // Lines Drwan are Noted for Saved File

     var customMarkers = [];
     ctx1 = chart_two.ctx;
     UndoCanvas.enableUndo(ctx1);
     addMarkerImages(chart_two);

     if (file.get("groundModelLines.save") == 0) {
       // var arr = new Array();
       for (var t = 1; t <= section_count; t++) {
         arr[t] = new Array();
         // line_in_gm = file.get("designData.designLines.Coordinates.gm"+t).length;
         // console.log(line_in_gm);



       }
       // chart_two.render();
     } else {
       for (var t = 1; t <= section_count; t++) {
         arr[t] = new Array();
         // line_in_gm = file.get("groundModelLines.Coordinates.gm" + t).length;
         arr[t] = file.get("groundModelLines.Coordinates.gm" + t);
         console.log(arr[t]);
         // alert(section_number);
         // drawdefinedLine(chart_two, file.get("groundModelLines.total"), curr_sec, ctx1);
       }
       // var arr = new Array();


       // chart_two.render();
       // arr = file.get("sectionLines.Coordinates");
       // console.log(arr);

     }

     function addMarkerImages(chart_two) {
       for (var i = 0; i < chart_two.data[1].dataPoints.length; i++) {

         customMarkers.push($("<img>").attr("src", chart_two.data[1].dataPoints[i].markerImageUrl)
           .css("display", "none")
           .css("height", 8)
           .css("width", 8)
           .appendTo($("#chartContainer>.canvasjs-chart-container"))
         );
         positionMarkerImage(customMarkers[i], i);
       }
     }

     function positionMarkerImage(customMarker, index) {
       var pixelX = chart_two.axisX2[0].convertValueToPixel(chart_two.options.data[1].dataPoints[index].x);
       var pixelY = chart_two.axisY[0].convertValueToPixel(chart_two.options.data[1].dataPoints[index].y);

       customMarker.css({
         "position": "absolute",
         "display": "block",
         "top": pixelY - customMarker.height() / 2,
         "left": pixelX - customMarker.width() / 2
       });
     }

     $(window).resize(function () {
       for (var i = 0; i < chart_two.data[1].dataPoints.length; i++) {
         positionMarkerImage(customMarkers[i], i);
       }
     });
     // var lineCoordinates = {};
     // var parentOffset = $(chart_two.container).offset();

     // jQuery(chart_two.container).on({
     //   //         mouseover: function (e) {
     //   //            // var strata = alert("Please enter the design strata");
     //   // console.log(e.clientY , e.clientX);

     //   //         },
     //   mousedown: function (e) {
     //     // var strata = alert("Please enter the design strata");
     //     if (e.clientX <= 170) {
     //       lineCoordinates.x1 = 0;
     //       lineCoordinates.y1 = e.clientY - parentOffset.top;

     //       lineCoordinates.x2 = chart_two.width;
     //       lineCoordinates.y2 = lineCoordinates.y1;

     //       drawLine(chart_two, lineCoordinates);
     //     } else {
     //       //  alert("Kindly Design at the Correct Region");
     //     }


     //   }


     // });



     var j = 0;

     function drawDesignLine(chart, y) {
       // var starta_area = document.getElementById("stratum_declare_area");
       // alert(y);
       console.log(y);
       // console.log(pos);
       // var h =  24;
       // var pos = Math.round(chart.convertPixelToValue(y));
       // var select_div = document.createElement("div");
       // select_div.offsetTop =  lineCoordinates.y1;
       // select_div.setAttribute("style", "position : absolute; top :" + co_ord + "px");
       // var select_checkbox = document.createElement("input");
       // select_checkbox.setAttribute("type", "radio");
       // select_checkbox.setAttribute("name", "check");
       // select_checkbox.setAttribute("id","check");
       // select_checkbox.setAttribute("value", design_Stratum);

       // var select = document.createElement("input");
       // select.setAttribute("Value", design_Stratum);
       // select.setAttribute("type", "text");
       // select.setAttribute("list", "stratum");
       // select.setAttribute("id", "stratum_" + test_curr + "_" + gm_curr + "_" + design_Stratum);
       // select.setAttribute("style","marginTop : 300px");
       // select_div.appendChild(select);
       // select_div.appendChild(select_checkbox);
       // select.offsetTop = lineCoordinates.y1;
       // select.offsetLeft = lineCoordinates.x1;
       // $("")
       // select.setAttribute("list","stratum");
       // starta_area.appendChild(select_div);
       // var design_plot = Math.round(chart.axisY[0].convertPixelToValue(y.y1));
       // alert(design_plot);
       strips.push({
         value: y.plot_value,
         label: y.line_no,
         lineDashType: "dot",
         thickness: 3
       })
     }

   }
   // Scaling GM STRATUM Plot
   function chartt() {
     var scale_x_val = document.getElementById("scale_x").value;
     var scale_y_val = document.getElementById("scale_y").value;
     chart_two.axisY[0].set("maximum", scale_y_val);
     chart_two.axisX[1].set("maximum", scale_x_val);

     // alert("ds");
   }
   // Getting Svaed Lines if available
   function drawdefinedLine(chart_two, total) {

     //  var ctx = chart_two.ctx;

     for (var i = 0; i < total; i++) {
       // ctx.beginPath();     //Change Line Width/Thickness
       var ff = [];
       // ff = file.get("sectionLines.Coordinates."+i);
       ff = arr[i];
       // alert(total);
       //  drawLinedefined(ff,chart_two);

       console.log(ff);

     }
   }
   // Drwaing the Lines got from saved file
   function drawLinedefined(ff, ctx) {
     // alert("came");
     console.log(ff.x1);
     //  var ctx = chart_two.ctx;

     console.log(ctx1);
     ctx1.beginPath();
     ctx1.moveTo(10, 20);
     ctx1.lineTo(120, 200);
     ctx1.stroke();

     drawLabel(ctx1, ff.line_no, ff, "center", 0);
     //  chart_two.visibiity = hidden;
     // document.getElementById("chartContainer").hidden = true;
   }

   // Add line Funciton Call

   function add_line_design() {
     // alert("done level one");
     var temp = 0;
     // chart.get("zoomEnabled")
     // chart.set("zoomEnabled", false);
     // chart.set("zoomEnabled",  false);
     // chart.set("zoomType", "xy");
     var lineCoordinates = {};

     var parentOffset = $(chart_two.container).offset();
     jQuery(chart_two.container).on({
       mousedown: function (e) {
         // chart.set("zoomEnabled", false);
         lineCoordinates.x1 = 0;
         lineCoordinates.y1 = e.pageY - parentOffset.top;

         lineCoordinates.x2 = chart_two.width;
         lineCoordinates.y2 = lineCoordinates.y1;
         if (temp == 0) {
           // chart.set("zoomEnabled", false);
           drawLine(chart_two, lineCoordinates, "line_count", "center", 10, curr_sec);

           // drawLabel(chart,line_count,lineCoordinates, "center",10);
           temp++;
           // chart.set("zoomEnabled", true);
         }

       }
     });



   }

   function data_get(limit) {
     var dps = [];
     for (var j = 1; j <= save_bh_count; j++) {
       var jj = j - 1;
       var bh = file.get("soilData.boreholeArray." + jj + "");
       var x_val = parseInt(file.get("soilData." + bh + ".stratumData.Eastings"), 10);
       var y_val = parseInt(file.get("soilData." + bh + ".stratumData.Northings"), 10);
       // alert(x_val+','+y_val);
       dps.push({
         indexLabel: String(bh),
         label: String(bh),
         x: x_val,

         y: y_val

       });



       // var points = [];




       // }
       // console.log(points);
       // return points;


     }

     return dps;
   }

   function dataGenerator(limit) {
     var y = 100;
     var points = [];
     for (var i = 0; i < limit; i += 1) {
       y += (Math.random() * 100 - 50);
       points.push({
         x: i - limit / 2,
         y: y
       });
     }
     return points;
   }

   function drawLabel(ctx1, text, lineCoordinates, alignment, padding) {
     if (!alignment) alignment = 'center';
     if (!padding) padding = 0;
     var p1 = [lineCoordinates.x1, lineCoordinates.y1];
     var p2 = [lineCoordinates.x2, lineCoordinates.y2];
     // alert(text);
     var dx = lineCoordinates.x2 - lineCoordinates.x1;
     var dy = lineCoordinates.y2 - lineCoordinates.y1;
     var len = Math.sqrt(dx * dx + dy * dy);
     var avail = len - 2 * padding;
     var angle = Math.atan2(dy, dx);
     if (angle < -Math.PI / 2 || angle > Math.PI / 2) {
       var p = p1;
       p1 = p2;
       p2 = p;
       dx *= -1;
       dy *= -1;
       angle -= Math.PI;
     }

     var p, pad;
     if (alignment == 'center') {
       p = p1;
       pad = 1 / 2;
     } else {
       var left = alignment == 'left';
       p = left ? p1 : p2;
       pad = padding / len * (left ? 1 : -1);
     }


     var ctx2 = chart_two.ctx;
     ctx1.save();
     ctx1.textAlign = alignment;
     ctx1.translate(100, lineCoordinates.y1);
     // alert(lineCoordinates.x1,lineCoordinates.y1);
     ctx1.rotate(Math.atan2(dy, dx));
     ctx1.fillStyle = "black"
     ctx1.fillText(text, 0, 0);
     ctx1.restore();
   }

   function drawLine(chart, lineCoordinates, text, alignment, padding, curr_sec) {
     // if (!alignment) alignment = 'center';
     // if (!padding) padding = 0;

     // var dx = lineCoordinates.x2 - lineCoordinates.x1;
     // var dy = lineCoordinates.y2 - lineCoordinates.y1;
     // var p, pad;
     // if (alignment=='center'){
     //   p = [lineCoordinates.x1,lineCoordinates.y1];
     //   pad = 1/2;
     // } else {
     //   var left = alignment=='left';
     //   p = left ?[lineCoordinates.x1,lineCoordinates.y1] :[lineCoordinates.x2,lineCoordinates.y2];
     //   pad = padding / Math.sqrt(dx*dx+dy*dy) * (left ? 1 : -1);
     // }


     // ctx1.beginPath();
     // ctx1.strokeStyle = $('#selColor').val();     //Change Line Color
     // ctx1.lineWidth = $('#selWidth').val();
     // ctx1.moveTo(lineCoordinates.x1, lineCoordinates.y1);
     // ctx1.lineTo(lineCoordinates.x2, lineCoordinates.y2);


     // file.set("section_deign")
     // ctx.textAlign = alignment;
     // ctx.translate(p.x+dx*pad,p.y+dy*pad);
     // ctx.rotate(Math.atan2(dy,dx));
     // ctx.fillText(text,0,0);
     // ctx.restore();
     // ctx1.stroke();
     var design_plot = Math.round(chart.axisY[0].convertPixelToValue(lineCoordinates.y1));

     line_count++;
     arr[curr_sec].push({
       // arr.push({
       line_no: line_count,
       x1: lineCoordinates.x1,
       x2: lineCoordinates.x2,
       y1: lineCoordinates.y1,
       y2: lineCoordinates.y2,
       plot_value: design_plot
     });
     // arr.push(lc_arr);

     file.set("groundModelLines.Coordinates.gm" + curr_sec, arr[curr_sec]);
     var total = 0;
     for (var g = 1; g <= save_bh_count; g++) {
       total = total + arr[g].length;
     }
     file.set("groundModelLines.total", total);
     file.set("groundModelLines.lastLineNo", line_count);
     file.set("groundModelLines.save", 1)
     file.save();
     console.log(lc_arr);
     console.log(arr);

     ctx1.beginPath();
     ctx1.strokeStyle = $('#selColor').val(); //Change Line Color
     ctx1.lineWidth = $('#selWidth').val();
     ctx1.moveTo(lineCoordinates.x1, lineCoordinates.y1);
     ctx1.lineTo(lineCoordinates.x2, lineCoordinates.y2);
     ctx1.stroke();

     drawLabel(ctx1, line_count, lineCoordinates, "center", 0);

   }

   function redo() {

     ctx1.redo();
   }

   function undo() {

     ctx1.undo();
   }

   function eraseAll() {

     w = chart_two.width;
     h = chart_two.height;

     var ctx = chart_two.ctx;

     var m = confirm("Want to clear");

     if (m) {
       chart_two.render();
     }
   }

   function del_section_var() {
     del = true;
   }

   function gm_divide(e) {


     // section_array[sec].push(e.dataPoint.label);
     // alert(section_array[sec].push(e.dataPoint.label));
     if (del == true) {
       for (var i = 0; i < section_array[curr_sec].length; i++) {
         if (section_array[curr_sec][i] === e.dataPoint.name_to_call) {
           section_array[curr_sec].splice(i, 1);
         }
       }

       function getUnique(arr) {

         const final = [];

         arr.map((e, i) => !final.includes(e) && final.push(e))

         return final
       }
       section_array[curr_sec] = getUnique(section_array[curr_sec]);

       del = false;
       sec_call(''+curr_sec+'');
     } else {
       section_array[curr_sec].push(e.dataPoint.name_to_call);

       function getUnique(arr) {

         const final = [];

         arr.map((e, i) => !final.includes(e) && final.push(e))

         return final
       }
       section_array[curr_sec] = getUnique(section_array[curr_sec]);
       sec_call(''+curr_sec+'');
     }


   }
