var ipcRenderer = require('electron').ipcRenderer;
var editJsonFile = require("edit-json-file");
var current_gm;
// var Color = ["#aeabab", "#ffff00", "#9cc2e5", "#c55a11"];
var remote = require('electron').remote;
const app = remote.app;
var gm_curr;
const Swal = require('sweetalert2');

var test_val_Arr = [];
var stratum_count = [];
var stratum_display_count = [];
var strips = [];
var maxx_val = ["60", "250", "50", "100"];
var value_limits = [];
var test_valll = ["SPT", "Cu", "IcS", "UcMap"];
// var gm_list_data = [];
// var tempFilePath = app.getPath('temp');
var tempFilePath = remote.getGlobal('path_file');
// const prompt = require('electron-prompt');
var myData = new Array();
var val = new Array();
var sections = new Array();
var section_list = new Array();
var section_list_data = new Array();
var borehole_count;
const prompt = require('electron-prompt');
var design_strata = new Array();
var i;
var test_curr;
var chart_two;
var line_count;
var points = [];
var gm_number;
var sec_curr;
var design_array_section = new Array();
var strata_name_array = [];
var var_val;
var click_Section_number = 0;
var store = new Array();
let file = editJsonFile(tempFilePath);
// var dbut = document.getElementById("delete_line");
// dbut.setAttribute("hidden",true);
document.getElementById("chartContainer").style.cursor = "crosshair";
// if(typeof(file.get("result"))=="undefined"){
for (var index_gm_val = 1; index_gm_val <= file.get("soilData.groundModelData.noOfGm"); index_gm_val++) {
    // for(var index_test_val = 1;index_test_val <= 4;index_test_val++){
    store[index_gm_val] = new Array();
    stratum_display_count[index_gm_val] = new Array();
    stratum_count[index_gm_val] = new Array();
    // }
    for (var index_test_val = 0; index_test_val < 4; index_test_val++) {
        store[index_gm_val][index_test_val] = new Array();
        stratum_count[index_gm_val][index_test_val] = new Array();
        stratum_display_count[index_gm_val][index_test_val] = new Array();
    }


}
// var max;
for (var index_gm_val = 1; index_gm_val <= file.get("soilData.groundModelData.noOfGm"); index_gm_val++) {
    strips[index_gm_val] = [];
    for (var d_s = 0; d_s < file.get("designData.strataName.gm" + index_gm_val + ".length"); d_s++) {
        var design_Strata = file.get("designData.strataName.gm" + index_gm_val + "." + d_s);
        if (design_Strata != "ground_Water") {
            strips[index_gm_val].push({
                value: file.get("designData.stratumData.gm" + index_gm_val + "." + d_s + "." + design_Strata),
                label: design_Strata,
                labelPlacement: "inside",
                color: '#000',
                labelFontColor: '#000'

            })
        }

    }

}
// }else{



// }

var project_title = document.getElementById('projectTitle');
var project_name = file.get("projectName");
var project_date = file.get("dateCreated");
var stratum_defined_list = ["Made Ground", "Rock", "Clay",
    "Sand",
    "Gravel",
    "Silt",
    "Chalk",
    "Limestone",
    "Mudstone",
    "Sandstone",
    "Void",
    "Flint",
    "Peat"
];
// project_title.innerHTML = "" + project_name + ".para | " + project_date + "";
document.title = "" + project_name + " | Sigma Soil V 2019.2.0.1 | Let's Pile ";
var type_of_mode = file.get("projectDetails.typeOfMode");
var save_bh_count = file.get("soilData.noOfBh");
var gm_count = file.get("soilData.groundModelData.noOfGm");
var gm_array = new Array();
const {
    Menu,
    MenuItem
} = remote

const menu = new Menu()

menu.append(new MenuItem({
    label: 'Add Profile Line',
    click() {
        add_line_design_prof(test_curr, current_gm);
    }
}))

// menu.append(new MenuItem({
//     label: 'Delete Section',
//     click() {
//         del_section_var()
//     }
// }))

menu.append(new MenuItem({
    type: 'separator'
}))
menu.append(new MenuItem({
    label: 'Show Tour',
    click() {
        introJs().start();
    }
}))

// Prevent default action of right click in chromium. Replace with our menu.
window.addEventListener('contextmenu', (e) => {
    e.preventDefault()
    menu.popup(remote.getCurrentWindow())
}, false)
if (file.get("designData.save") == 0) {

    design_array_section[0] = "Null Section";

    strata_name_array[0] = "Null Section";
} else {
    // var design_array_section = new Array();
    for (var k = 0; k < file.get("designData.stratumData").length; k++) {
        design_array_section[k] = file.get("designData.stratumData." + k);
        // var strata_name_array = [];
        strata_name_array[k] = file.get("designData.strataName." + k);
    }

}

for (var gm_index = 0; gm_index < gm_count; gm_index++) {
    var gm_val = gm_index + 1;
    gm_array[gm_index] = new Array();
    gm_array[gm_index] = file.get("soilData.groundModelData.gm" + gm_val);

}


var design = file.get("designData.stratumData.1.0.ref"); // section_number for loop
// alert(design);
section_count = localStorage.getItem("section_number");
console.log(section_count);
var menu_div = document.getElementById("menu_f");

function get_test(gm_val) {
    for (var f = 0; f < file.get("soilData.noOfTest"); f++) {

        var test_menu = document.getElementById("test_ul_" + gm_val);
        var test_li = document.createElement("li");
        var test_button = document.createElement("button");

        test_li.setAttribute("class", "nav-item");

        test_button.setAttribute("onclick", "set_var_val('" + f + "')");

        var tabs_a = document.createElement("a");
        tabs_a.setAttribute("class", "nav-link ");
        // tabs_a.setAttribute("data-toggle", "tab");
        // tabs_a.setAttribute("role", "tab");

        var tabs_text = document.createTextNode(file.get("soilData.test." + f));

        tabs_a.appendChild(tabs_text);
        test_button.appendChild(tabs_a);

        test_li.appendChild(test_button)

        test_menu.appendChild(test_li);

    }

}





function display_gm(gm_number) {
    // stratum_display_count[gm_number] = [];
    current_gm = gm_number;
    sec_curr = gm_number;
    if (file.get("designData.save") == 0) {
        design_array_section[sec_curr] = new Array();
        strata_name_array[sec_curr] = new Array();
    }
    document.getElementById("stratum_declare_area").innerHTML = "";
    // alert(current_gm);
    // alert(section_number);
    gm_list_data = file.get("soilData.groundModelData.gm" + gm_number);
    // section_list_data = localStorage.getItem("select_" + section_number + "");
    console.log(gm_list_data);
    // sections = JSON.parse(section_list_data);
    // console.log(sections);
    $.myjQuery(current_gm);
}
myData = localStorage.getItem(myData);
console.log(section_list_data);
console.log(myData);
//  i = localStorage.getItem(borehole_count);
// console.log(i);
if (myData) {
    val = JSON.parse(myData);
}



i = val[0];

console.log(sections);



function printAll() {

    var str = "",
        i;
    var section = new Array();
    section.push("1");
    // var arr_+click_Section_number = new Array();
    for (i = 0; i < mySelect.options.length; i++) {
        if (mySelect.options[i].selected) {
            // str = str + i + " ";
            section.push(i);
        }
    }
    console.log(section);

    // alert("Options selected are " + str);
}



const CanvasJS = require('./assets/javascript/canvasjs.min.js');
// const JQuery =  require('./assets/javascript/jquery.min.js');
// var one = val[0];
$.myjQuery = function (f) {

    var dps = []; // dataPoints
    var s_one = []; // dataPoints
    var s_two = []; // dataPoints
    var s_three = []; // dataPoints

    var y_data = [];
    var gm_data = new Array();
    chart_two = new CanvasJS.Chart("chartContainer", {
        // orientation:portrait,\

        height: 600,
        width: 400,
        animationEnabled: true,
        title: {
            // text: "Test Plot"
        },
        axisX: {
            titleFontSize: 12,
            labelFontSize: 8,
        },

        axisX2: {

            titleFontSize: 12,
            labelFontSize: 8,
            includeZero: true,
            labelFontSize: 8,
            minimum: 0,
            viewportMinimum: 0,
            viewportMaximum: maxx_val[test_curr],
            title: test_valll[test_curr],
            gridDashType: "dot",
            gridThickness: 2,

            crosshair: {
                enabled: true //disable here
            }

        },
        axisY: y_data,

        toolTip: {
            content: "{name} <br> (SPT) : {y}"
        },
        legend: {


        },
        data: gm_data

    });

    if (type_of_mode == "mbgl") {
        y_data.push({
            'stripLines': strips[f],
            'title': "Depth ( m bgl )",

            'titleFontSize': "12",
            'minimum': 0,
            'maximum': 250,
            'labelFontSize': "8",
            'gridDashType': "dot",
            'gridThickness': 1,
            'viewportMinimum': 0,
            'valueFormatString': "##.##",
            'reversed': true,
            // 'tickLength': 15,
            // 'tickColor': "red",
            'crosshair': {
                'enabled': true //disable here
            }
        });
    } else {
        y_data.push({
            'stripLines': strips[f],
            'title': "Elevation ( mAOD )",

            'minimum': 0,
            'maximum': 250,
            'viewportMinimum': 0,
            'titleFontSize': "12",
            'labelFontSize': "8",
            'gridDashType': "dot",
            'gridThickness': 1,
            'valueFormatString': "##.##",
            // 'tickLength': 15,
            // 'tickColor': "red",
            'reversed': false,
            'crosshair': {
                'enabled': true
            } //disable here
        });
    }

    var strata_name_dps = new Array();
    gm_data.push({
        'type': 'scatter',
        'axisXType': 'secondary',
        // 'mousemove':onMousemove,
        // 'toolTipContent': "<b>Bore hole: </b>{x} sq.ft<br/><b>Price: </b>${y}k",
        'dataPoints': strata_name_dps
    });
    var val_maxxx = new Array();
    console.log(gm_list_data);
    var maxxi_x = 0;
    for (var g = 0; g < gm_list_data.length; g++) {
        var getting_index = new Array();

        getting_index = gm_list_data[g].split("_");
        console.log(gm_list_data[g]);
        // console.log(getting_index);
        var test_name = file.get("soilData.test." + var_val);
        console.log(test_name);
        console.log(file.get("soilData." + gm_list_data[g] + ".testData"));
        var lenn_check = file.get("soilData." + gm_list_data[g] + ".testData." + test_name + ".testValue");
        if (typeof lenn_check === "undefined") {
            var lenn = 0;
            var lenn_val = 0;
        } else {
            var lenn = Object.keys(lenn_check).length;
            var lenn_val = lenn / 2;
        }

        for (var val = 1; val <= lenn_val; val++) {


            val_maxxx.push(parseInt(file.get("soilData." + gm_list_data[g] + ".testData." + test_name + ".testValue.x" + val + "")));
            console.log(val_maxxx);
            console.log(Math.max(val_maxxx));
            strata_name_dps.push({

                // label: gm_list_data[g],
                name: gm_list_data[g],

                y: parseInt(file.get("soilData." + gm_list_data[g] + ".testData." + test_name + ".testValue.x" + val +
                    ""), 10),

                x: parseInt(file.get("soilData." + gm_list_data[g] + ".testData." + test_name + ".testValue.y" + val +
                    ""), 10)

                // color: file.get("soilData." + gm_list_data[g] + ".stratumData." + strata_name + ".strataColor")
            });
            var max_x = parseInt(file.get("soilData." + gm_list_data[g] + ".testData." + test_name + ".testValue.y" + val +
                ""), 10);
            if (max_x > maxxi_x) {
                maxxi_x = max_x;
            }
        }


    }
    console.log(maxxi_x);




    for (v = 1; v <= i; v++) {
        for (h = 0; h <= sections.length; h++) {
            if (v == sections[h]) {
                s_one.push({
                    label: String(val[v]),
                    y: parseInt(val[v + 4 * i], 10)
                });
                s_two.push({
                    label: String(val[v]),
                    y: parseInt(val[v + 5 * i], 10)
                });
                s_three.push({
                    label: String(val[v]),
                    y: parseInt(val[v + 6 * i], 10)

                });
            }
        }
        // console.log(sections[v]);




    }


    for (var design_strata_index = 0; design_strata_index < file.get("soilData.groundModelData.noOfGm"); design_strata_index++) {
        // alert(current_gm);

        var len = file.get("result.gm_" + current_gm + ".Depth");
        console.log(len);
        var dum = design_strata_index + 1;
        for (var d_s = 0; d_s < file.get("designData.strataName.gm" + current_gm + ".length"); d_s++) {
            var design_Strata = file.get("designData.strataName.gm" + current_gm + "." + d_s);
            // alert(design_Strata);
            if (design_Strata != "ground_Water") {
                drawDesignLine(chart_two, file.get("designData.stratumData.gm" + current_gm + "." + d_s + "." + design_Strata), design_strata_index, test_curr, current_gm, file.get("designData.stratumData.gm" + current_gm + "." + d_s + "." + design_Strata + "_Co_Ordinate"), design_Strata);

            }

            console.log(file.get("designData.stratumData.gm" + current_gm + "." + d_s + "." + design_Strata));
        }

    }
    // console.log(Object.keys(strips).length);
    // console.log(strips.length);
    // console.log(strips[1][strips.length - 1]["value"] + 20);


    chart_two.render();
    document.getElementById("chartContainer").onmousemove = function () {
        document.getElementsByClassName("canvasjs-chart-canvas")[1].style.cursor = "crosshair";
    }

    function onMousemove(e) {
        // alert(  e.dataSeries.type+ ", dataPoint { x:" + e.dataPoint.x + ", y: "+ e.dataPoint.y + " }" );
    }

    // }
    ctx1 = chart_two.ctx;
    UndoCanvas.enableUndo(ctx1);

    var lineCoordinates = {};
    var parentOffset = $(chart_two.container).offset();

    var temp;

    function drawDesignLine(chart, y, col, test_curr, gm_curr, co_ord, design_Stratum) {
        var starta_area = document.getElementById("stratum_declare_area");

        var select_div = document.createElement("div");
        // select_div.offsetTop =  lineCoordinates.y1;
        select_div.setAttribute("style", "position : absolute; top :" + co_ord + "px");
        var select_checkbox = document.createElement("input");
        select_checkbox.setAttribute("type", "radio");
        select_checkbox.setAttribute("name", "check");
        select_checkbox.setAttribute("class", "form-check-input");
        // select_checkbox.setAttribute("id","check");
        select_checkbox.setAttribute("value", design_Stratum);

        var select = document.createElement("input");
        select.setAttribute("Value", design_Stratum);
        select.setAttribute("type", "text");
        select.setAttribute("list", "stratum");
        select.setAttribute("style", "height:20px;");
        select.setAttribute("class", "test form-control");
        select.setAttribute("id", "stratum_" + test_curr + "_" + gm_curr + "_" + design_Stratum);
        // select.setAttribute("style","marginTop : 300px");

        if (select.id == "stratum_" + test_curr + "_" + gm_curr + "_" + design_Stratum) {
            select_div.appendChild(select_checkbox);
            select_div.appendChild(select);

        }
        // alert(current_gm + gm_curr);



    }
    // console.log(y);

    // chart_two.axisY[0].set("viewportMaximum",strips[1][strips.length-1]["value"]+10);


    fix_graph(chart_two, test_curr, f, current_gm,maxxi_x);


}

function fix_graph(chart_two, test, val, gm,maxxi_x) {
    var max_val = 0;
    for (var i = 0; i < file.get("soilData.groundModelData.gm" + gm).length; i++) {
        var bh = file.get("soilData.groundModelData.gm" + gm + "." + i);
        var bh_len = file.get("soilData.params." + bh).length;
        var bh_lent = file.get("soilData.params." + bh).length - 1;
        var bh_lentt = file.get("soilData.params." + bh).length - 2;
        if (file.get("soilData.params." + bh + "." + bh_lent) == "") {
            var bh_Strata = file.get("soilData.params." + bh + "." + bh_lentt);
            var bh_max = file.get("soilData." + bh + ".stratumData." + bh_Strata + ".end");
        } else {
            var bh_Strata = file.get("soilData.params." + bh + "." + bh_lent);
            var bh_max = file.get("soilData." + bh + ".stratumData." + bh_Strata + ".end");
        }
        if (bh_max > max_val) {
            max_val = bh_max;
        }
    }
    console.log(max_val);
    var maxxi = parseInt(max_val) + 20;
    chart_two.axisY[0].set("maximum", maxxi);
    chart_two.axisX2[0].set("maximum",maxxi_x+10);
}

function set(y1, stra, sec) {
    // alert(sec + stra + y1);

    var stratum = String(stra);

    design_array_section[sec].push({
        [stratum]: y1
    });
    strata_name_array[sec].push(stratum);
    console.log(design_strata);
    console.log(strata_name_array);
    console.log(design_array_section);

}
var tt_curr;
// var gm_curr;


function add_line_design(type) {
    // alert("done level one");

    var temp = 0;

    var lineCoordinates = {};



    var parentOffset = $(chart_two.container).offset();
    jQuery(chart_two.container).on({
        mousedown: function (e) {

            lineCoordinates.x1 = 0;
            lineCoordinates.y1 = e.pageY - parentOffset.top;

            lineCoordinates.x2 = document.getElementsByClassName("canvasjs-chart-canvas")[1].width;
            lineCoordinates.y2 = lineCoordinates.y1;
            // alert(lineCoordinates.y2);
            if (temp == 0) {
                // chart.set("zoomEnabled", false);
                alert("You have CHosen to Create Design for " + type);
                drawLine_new(chart_two, lineCoordinates, "center", 10, sec_curr, type);

                // drawLabel(chart,line_count,lineCoordinates, "center",10);
                temp++;
                // chart.set("zoomEnabled", true);
            }


        }
    });



}


function drawLine_new(chart, lineCoordinates, alignment, padding, section_number, type) {
    line_count++;

    if (!alignment) alignment = 'center';
    if (!padding) padding = 0;

    var dx = lineCoordinates.x2 - lineCoordinates.x1;
    var dy = lineCoordinates.y2 - lineCoordinates.y1;
    var p, pad;
    if (alignment == 'center') {
        p = [lineCoordinates.x1, lineCoordinates.y1];
        pad = 1 / 2;
    } else {
        var left = alignment == 'center';
        p = left ? [lineCoordinates.x1, lineCoordinates.y1] : [lineCoordinates.x2, lineCoordinates.y2];
        pad = padding / Math.sqrt(dx * dx + dy * dy) * (left ? 1 : -1);
    }


    ctx1.beginPath();
    if (type == 'ground_water') {
        ctx1.strokeStyle = "#0000ff"; //Change Line Color
        // ctx1.lineWidth = $('#selWidth').val();
    } else {
        ctx1.strokeStyle = $('#selColor').val(); //Change Line Color
        ctx1.lineWidth = $('#selWidth').val();
    }

    ctx1.moveTo(lineCoordinates.x1, lineCoordinates.y1);
    if (lineCoordinates.y1 > lineCoordinates.y2) {

    } else {
        ctx1.lineTo(lineCoordinates.x2, lineCoordinates.y2);
    }


    var design_plot = Math.round(chart.axisY[0].convertPixelToValue(lineCoordinates.y1));


    ctx1.stroke();



    prompt({
            title: 'Stratum input',
            label: 'Stratum:',
            type: 'select',
            selectOptions: {


            },
            value: ''
        })
        .then((r) => {
            if (r === null) {
                console.log('user cancelled');
            } else {
                // alert(sec_key);
                if (type == 'ground_water') {

                    set_new(design_plot, "ground_Water", section_number, lineCoordinates.y1);
                    // drawLabel(chart, "Ground Water Line", lineCoordinates, "center", 10);

                    file.save();
                } else {
                    set_new(design_plot, r, section_number, lineCoordinates.y1);
                    // drawLabel(chart, r, lineCoordinates, "center", 10);

                    file.save();

                }


            }
        })
        .catch(console.error);

}
var line, temp, isDown;
var t = 0;
var limits = [];
var stratas = [];
var arrofstrata = {};
// var store = new Array();
var color_val;

function add_line_design_prof(tt_curr, gm_curr) {
    // window.load();
    var i = 0;
    var stratum_for_design, stratum_for_design_index, miniii, maxiii, stratum_display;
    document.getElementById("chartContainer").style.cursor = "crosshair";
    // alert("done level one");
    isDown = false;
    temp = 0;
    console.log(temp);



    console.log(file.get("designData.strataName.gm" + current_gm));
    for (var index_s = 0; index_s < file.get("designData.strataName.gm" + current_gm).length; index_s++) {
        var val = file.get("designData.strataName.gm" + current_gm + "." + index_s);
        if (val != "ground_Water") {
            arrofstrata[val] = '' + val + '';
        }

    }


    console.log(arrofstrata);
    // arrofstrata = file.get("designData.stratumName.gm" + current_gm );
    Swal.mixin({
        input: 'text',
        confirmButtonText: 'Next &rarr;',
        showCancelButton: true,
        progressSteps: ['A', 'B']
    }).queue([{
            title: 'Select Stratum',
            input: 'select',
            inputOptions: arrofstrata,
            inputPlaceholder: 'Select a Stratum',
            showCancelButton: true,
            inputValidator: (value) => {
                if (!value) {
                    return 'You need to choose something!'
                }
            }
        },
        {
            title: 'Select Line Type',
            input: 'radio',
            footer: 'By Default Continuous is Selected',
            inputValue: 3,
            inputOptions: {
                0: "<h5>Start to Intermediate</h5> <br> <h6>Discontinuous</h6>",
                1: "<h5>Intermediate to End</h5><br> <h6>Discontinuous</h6>",
                2: "<h5>Intermediate to Intermediate</h5><br> <h6>Discontinuous</h6>",
                3: "<h5>Start to End</h5><br> <h6>Continuous</h6>"
            },
            inputValidator: (value) => {
                if (!value) {
                    return 'You need to choose something!'
                }
            }

        }
        // ,
        // {
        //     title: 'Done !',
        //     text:'Kindly Draw the Design Line',
        //     showConfirmButton: false,
        //     icon: 'success',
        //     timer: 1500,
        //     showCancelButton: false
        // }

    ]).then((result) => {
        if (result.value) {
            if (result.value[0] === null) {
                console.log('user cancelled');
            } else {
                console.log(result.value[1]);
                var value_line_type = result.value[1];
                Swal.fire(
                    'Done!',
                    'Kindly Draw the Design Line ',
                    'success'
                )

                if (result.value[0] == "Rock") {
                    color_val = "#fff";
                } else {
                    color_val = "#000";
                }
                stratas.push(result.value[0]);
                stratum_for_design = result.value[0];
                stratum_display = result.value[0];



                console.log(stratum_for_design, stratum_display, result.value[0]);

                stratas.push(result.value[0]);
                console.log('result', result.value[0]);
                console.log('gm', gm_curr);


                arr1 = file.get("designData.strataName.gm" + gm_curr);
                console.log(arr1);
                stratum_for_design_index = arr1.indexOf(result.value[0]);
                var next = stratum_for_design_index + 1;
                miniii = file.get("designData.stratumData.gm" + gm_curr + "." + stratum_for_design_index + "." + arr1[stratum_for_design_index]);
                if (arr1[next] == "ground_Water") {
                    next = stratum_for_design_index + 2;
                } else {

                }
                maxiii = file.get("designData.stratumData.gm" + gm_curr + "." + next + "." + arr1[next]);

                if (typeof maxiii == 'undefined') {
                    if (type_of_mode == "mbgl") {
                        maxiii = 0 + (miniii + 20);
                    } else {
                        maxiii = miniii - 20;
                    }

                } else {

                }

                console.log(strips[0]);
                strips[gm_curr].push({
                    startValue: miniii,
                    endValue: maxiii,
                    color: file.get("strataColor." + result.value[0] + "")
                });
                console.log(strips);

                chart_two.render();
                refresh();
                // chart_two.axisY[0].set("viewportMaximum", maxiii);


                var parentOffset = $(chart_two.container).offset();

                var lineCoordinates = {};
                // max = 0;
                t++;
                limits[t] = [];
                limits[t].push({
                    "max": maxiii,
                    "min": miniii,
                    "type": result.value[1],
                    "color": color_val
                });
                // console.log("before: "+t);
                console.log(limits);
                jQuery(chart_two.container).on({

                    mousedown: function (e) {
                        // document.getElementsByClassName("canvasjs-chart-canvas")[1].style.cursor = "nw-resize";
                        e.stopPropagation();

                        if (limits[t][0].max == maxiii) {
                            if (temp == 0 && isDown == false) {
                                // console.log(chart_two.axisY[0].convertPixelToValue(e.pageY - parentOffset.top), f_min, f_max, stratum_for_design);

                                if (type_of_mode == "mbgl") {
                                    if (chart_two.axisY[0].convertPixelToValue(e.pageY - parentOffset.top) >= miniii && chart_two.axisY[0].convertPixelToValue(e.pageY - parentOffset.top) <= maxiii) {
                                        isDown = true;
                                        // chart.set("zoomEnabled", false);
                                        if (limits[t][0].type == 0 || limits[t][0].type == 3) {
                                            console.log("from start");
                                            lineCoordinates.x1 = e.pageX - parentOffset.left - 10;
                                            lineCoordinates.y1 = chart_two.axisY[0].convertValueToPixel(miniii)
                                        } else {
                                            console.log("not from start");
                                            lineCoordinates.x1 = e.pageX - parentOffset.left;
                                            lineCoordinates.y1 = e.pageY - parentOffset.top;
                                        }
                                    }
                                } else {
                                    if (chart_two.axisY[0].convertPixelToValue(e.pageY - parentOffset.top) >= maxiii && chart_two.axisY[0].convertPixelToValue(e.pageY - parentOffset.top) <= miniii) {
                                        isDown = true;
                                        // chart.set("zoomEnabled", false);
                                        if (limits[t][0].type == 0 || limits[t][0].type == 3) {
                                            console.log("from start ele");
                                            lineCoordinates.x1 = e.pageX - parentOffset.left;
                                            lineCoordinates.y1 = chart_two.axisY[0].convertValueToPixel(miniii)
                                        } else {
                                            console.log("not from start ele");
                                            lineCoordinates.x1 = e.pageX - parentOffset.left;
                                            lineCoordinates.y1 = e.pageY - parentOffset.top;
                                        }
                                    }
                                }

                            }
                        } else {
                            // document.getElementsByClassName("canvasjs-chart-canvas")[1].style.cursor = "no-drop";
                            // document.getElementById("chartContainer").onmousemove = function(){
                            // document.getElementsByClassName("canvasjs-chart-canvas")[1].style.cursor = "no-drop";
                            // }
                        }

                    },
                    mouseup: function (e) {
                        e.stopPropagation();

                        if (limits[t][0].max == maxiii) {
                            // document.getElementsByClassName("canvasjs-chart-canvas")[1].style.cursor = "crosshair";
                            if (temp == 0 && isDown == true) {

                                if (type_of_mode == "mbgl") {
                                    console.log(store);
                                    isDown = false;
                                    console.log(result.value[1]);
                                    console.log(value_line_type);
                                    if (limits[t][0].type == 1 || limits[t][0].type == 3) {
                                        console.log("till end");
                                        lineCoordinates.y2 = chart_two.axisY[0].convertValueToPixel(maxiii);
                                        console.log(lineCoordinates);
                                        console.log("Current " + tt_curr);
                                        store[current_gm][test_curr].push({
                                            x1: lineCoordinates.x1,
                                            y1: lineCoordinates.y1,
                                            x2: lineCoordinates.x2,
                                            y2: lineCoordinates.y2,
                                            col: color_val
                                        });

                                        console.log(store);
                                        if (Math.round(chart_two.axisY[0].convertPixelToValue(lineCoordinates.y2)) >= miniii && Math.round(chart_two.axisY[0].convertPixelToValue(lineCoordinates.y2)) <= maxiii) {
                                            drawLine(chart_two, lineCoordinates, line_count, "center", 10, current_gm, stratum_for_design, test_curr, current_gm);
                                            console.log("Calling from" + test_valll[test_curr]);
                                            drawHorizontalLine(chart_two, lineCoordinates, line_count, "center", 10, current_gm, stratum_for_design, tt_curr, current_gm);
                                            console.log("one h");
                                            temp++;
                                        }

                                    } else {
                                        // lineCoordinates.y2 = convertValueToPixel(maxiii);
                                        console.log("not till end ");
                                        console.log(lineCoordinates);
                                        console.log("Current " + tt_curr);
                                        store[current_gm][test_curr].push({
                                            x1: lineCoordinates.x1,
                                            y1: lineCoordinates.y1,
                                            x2: lineCoordinates.x2,
                                            y2: lineCoordinates.y2,
                                            col: color_val
                                        });

                                        console.log(store);
                                        if (Math.round(chart_two.axisY[0].convertPixelToValue(lineCoordinates.y2)) >= miniii && Math.round(chart_two.axisY[0].convertPixelToValue(lineCoordinates.y2)) <= maxiii) {
                                            drawLine(chart_two, lineCoordinates, line_count, "center", 10, current_gm, stratum_for_design, test_curr, current_gm);
                                            console.log("Calling from" + test_valll[test_curr]);
                                            drawHorizontalLine(chart_two, lineCoordinates, line_count, "center", 10, current_gm, stratum_for_design, tt_curr, current_gm);
                                            console.log("two h");
                                            temp++;
                                        }
                                    }



                                } else {
                                    console.log(store);
                                    isDown = false;
                                    if (limits[t][0].type == 1 || limits[t][0].type == 3) {
                                        console.log("till end");
                                        lineCoordinates.y2 = chart_two.axisY[0].convertValueToPixel(maxiii);
                                        console.log(lineCoordinates);
                                        console.log("Current " + tt_curr);
                                        store[current_gm][test_curr].push({
                                            x1: lineCoordinates.x1,
                                            y1: lineCoordinates.y1,
                                            x2: lineCoordinates.x2,
                                            y2: lineCoordinates.y2,
                                            col: color_val
                                        });

                                        console.log(store);
                                        if (Math.round(chart_two.axisY[0].convertPixelToValue(lineCoordinates.y2)) >= maxiii && Math.round(chart_two.axisY[0].convertPixelToValue(lineCoordinates.y2)) <= miniii) {
                                            drawLine(chart_two, lineCoordinates, line_count, "center", 10, current_gm, stratum_for_design, test_curr, current_gm);
                                            console.log("Calling from" + test_valll[test_curr]);
                                            drawHorizontalLine(chart_two, lineCoordinates, line_count, "center", 10, current_gm, stratum_for_design, tt_curr, current_gm);
                                            console.log("three h");
                                            temp++;
                                        }

                                    } else {
                                        // lineCoordinates.y2 = convertValueToPixel(maxiii);
                                        console.log("not till end");
                                        console.log(lineCoordinates);
                                        console.log("Current " + tt_curr);
                                        store[current_gm][test_curr].push({
                                            x1: lineCoordinates.x1,
                                            y1: lineCoordinates.y1,
                                            x2: lineCoordinates.x2,
                                            y2: lineCoordinates.y2,
                                            col: color_val
                                        });

                                        console.log(store);
                                        if (Math.round(chart_two.axisY[0].convertPixelToValue(lineCoordinates.y2)) >= maxiii && Math.round(chart_two.axisY[0].convertPixelToValue(lineCoordinates.y2)) <= miniii) {
                                            drawLine(chart_two, lineCoordinates, line_count, "center", 10, current_gm, stratum_for_design, test_curr, current_gm);
                                            console.log("Calling from" + test_valll[test_curr]);
                                            drawHorizontalLine(chart_two, lineCoordinates, line_count, "center", 10, current_gm, stratum_for_design, tt_curr, current_gm);
                                            console.log("four h");
                                            temp++;
                                        }
                                    }


                                }

                            }
                        } else {
                            // document.getElementsByClassName("canvasjs-chart-canvas")[1].style.cursor = "no-drop";
                            // document.getElementById("chartContainer").onmousemove = function(){
                            //     document.getElementsByClassName("canvasjs-chart-canvas")[1].style.cursor = "no-drop";
                            // }
                            console.log("not allowed     wala");

                            // document.getElementsByClassName("canvasjs-chart-canvas")[1].style.cursor = "no-drop";
                            // document.getElementsByClassName("canvasjs-chart-canvas")[1].style.cursor = "no-drop";
                        }
                        refresh(color_val);

                    },
                    mousemove: function (e) {
                        //    console.log(e);
                        // console.log($(chart_two.container).data());

                        // if (chart_two.axisY[0].convertPixelToValue(e.pageY - parentOffset.top) < maxiii && chart_two.axisY[0].convertPixelToValue(e.pageY - parentOffset.top) > miniii) {
                        if (limits[t][0].max == maxiii) {
                            document.getElementsByClassName("canvasjs-chart-canvas")[1].style.cursor = "crosshair";
                            // console.log(document.getElementsByClassName("canvasjs-chart-canvas")[1].width);
                            console.log("allowed");

                            if (!isDown) return;
                            if (temp == 0 && isDown == true) {
                                if (type_of_mode == "mbgl") {
                                    lineCoordinates.x2 = e.pageX - parentOffset.left;
                                    lineCoordinates.y2 = e.pageY - parentOffset.top;
                                    console.log(lineCoordinates);

                                    if (lineCoordinates.y1 < lineCoordinates.y2 && chart_two.axisY[0].convertPixelToValue(lineCoordinates.y2) >= miniii && chart_two.axisY[0].convertPixelToValue(lineCoordinates.y2) <= maxiii) {
                                        // console.log("min at move for "+stratum_for_design+" : "+min,"max at move "+stratum_for_design+" : "+max);
                                        console.log(maxiii, miniii);
                                        console.log(result.value[0]);
                                        refresh();
                                        drawLine_mov(chart_two, lineCoordinates, color_val);
                                    } else {

                                    }
                                } else {
                                    lineCoordinates.x2 = e.pageX - parentOffset.left;
                                    lineCoordinates.y2 = e.pageY - parentOffset.top;
                                    console.log(lineCoordinates);

                                    if (lineCoordinates.y1 < lineCoordinates.y2 && chart_two.axisY[0].convertPixelToValue(lineCoordinates.y2) >= maxiii && chart_two.axisY[0].convertPixelToValue(lineCoordinates.y2) <= miniii) {
                                        // console.log("min at move for "+stratum_for_design+" : "+min,"max at move "+stratum_for_design+" : "+max);
                                        console.log(maxiii, miniii);
                                        console.log(result.value[0]);
                                        refresh();
                                        drawLine_mov(chart_two, lineCoordinates, color_val);
                                    } else {

                                    }
                                }

                            }



                        } else {
                            console.log("not allowed");

                            // document.getElementsByClassName("canvasjs-chart-canvas")[1].style.cursor = "no-drop";
                            document.getElementsByClassName("canvasjs-chart-canvas")[1].style.cursor = "no-drop";
                        }

                    }
                });




            }
            // Swal.fire({
            //     title: 'All done!',
            //     html: `
            //   Your answers:
            //   <pre><code>${result.value[0]}</code></pre>
            // `,
            //     confirmButtonText: 'Lovely!'
            // })
        }
    })



    // prompt({
    //         title: 'Add Stratum',
    //         label: 'Kindly Type the Stratum',
    //         type: 'select',
    //         value: '',
    //         selectOptions: arrofstrata


    //     }).then((ress) => {
    //         if (ress === null) {
    //             console.log('user cancelled');
    //         } else {




    //             stratas.push(ress);
    //             stratum_for_design = ress;
    //             stratum_display = ress;



    //             console.log(stratum_for_design, stratum_display, ress);

    //             stratas.push(ress);
    //             console.log('result', ress);
    //             console.log('gm', gm_curr);


    //             arr1 = file.get("designData.strataName.gm" + gm_curr);
    //             console.log(arr1);
    //             stratum_for_design_index = arr1.indexOf(ress);
    //             var next = stratum_for_design_index + 1;
    //             miniii = file.get("designData.stratumData.gm" + gm_curr + "." + stratum_for_design_index + "." + arr1[stratum_for_design_index]);
    //             if (arr1[next] == "ground_Water") {
    //                 next = stratum_for_design_index + 2;
    //             } else {

    //             }
    //             maxiii = file.get("designData.stratumData.gm" + gm_curr + "." + next + "." + arr1[next]);

    //             if (typeof maxiii == 'undefined') {
    //                 if (type_of_mode == "mbgl") {
    //                     maxiii = 0 + (miniii * 2);
    //                 } else {
    //                     maxiii = 0;
    //                 }

    //             } else {

    //             }

    //             console.log(strips[0]);
    //             strips[gm_curr].push({
    //                 startValue: miniii,
    //                 endValue: maxiii,
    //                 color: file.get("strataColor." + ress + "")
    //             });
    //             console.log(strips);

    //             chart_two.render();
    //             refresh();
    //             // chart_two.axisY[0].set("viewportMaximum", maxiii);


    //             var parentOffset = $(chart_two.container).offset();

    //             var lineCoordinates = {};
    //             // max = 0;
    //             t++;
    //             limits[t] = [];
    //             limits[t].push({
    //                 "max": maxiii,
    //                 "min": miniii
    //             });
    //             // console.log("before: "+t);
    //             console.log(limits);
    //             jQuery(chart_two.container).on({

    //                 mousedown: function (e) {
    //                     // document.getElementsByClassName("canvasjs-chart-canvas")[1].style.cursor = "nw-resize";
    //                     e.stopPropagation();

    //                     if (limits[t][0].max == maxiii) {
    //                         if (temp == 0 && isDown == false) {
    //                             // console.log(chart_two.axisY[0].convertPixelToValue(e.pageY - parentOffset.top), f_min, f_max, stratum_for_design);

    //                             if (type_of_mode == "mbgl") {
    //                                 if (chart_two.axisY[0].convertPixelToValue(e.pageY - parentOffset.top) >= miniii && chart_two.axisY[0].convertPixelToValue(e.pageY - parentOffset.top) < maxiii) {
    //                                     isDown = true;
    //                                     // chart.set("zoomEnabled", false);
    //                                     lineCoordinates.x1 = e.pageX - parentOffset.left;
    //                                     lineCoordinates.y1 = e.pageY - parentOffset.top;
    //                                 }
    //                             } else {
    //                                 if (chart_two.axisY[0].convertPixelToValue(e.pageY - parentOffset.top) >= maxiii && chart_two.axisY[0].convertPixelToValue(e.pageY - parentOffset.top) < miniii) {
    //                                     isDown = true;
    //                                     // chart.set("zoomEnabled", false);
    //                                     lineCoordinates.x1 = e.pageX - parentOffset.left;
    //                                     lineCoordinates.y1 = e.pageY - parentOffset.top;
    //                                 }
    //                             }

    //                         }
    //                     } else {
    //                         // document.getElementsByClassName("canvasjs-chart-canvas")[1].style.cursor = "no-drop";
    //                         // document.getElementById("chartContainer").onmousemove = function(){
    //                         // document.getElementsByClassName("canvasjs-chart-canvas")[1].style.cursor = "no-drop";
    //                         // }
    //                     }

    //                 },
    //                 mouseup: function (e) {
    //                     e.stopPropagation();

    //                     if (limits[t][0].max == maxiii) {
    //                         // document.getElementsByClassName("canvasjs-chart-canvas")[1].style.cursor = "crosshair";
    //                         if (temp == 0 && isDown == true) {

    //                             if (type_of_mode == "mbgl") {
    //                                 console.log(store);
    //                                 isDown = false;
    //                                 console.log(lineCoordinates);
    //                                 console.log("Current " + tt_curr);
    //                                 store[current_gm][test_curr].push({
    //                                     x1: lineCoordinates.x1,
    //                                     y1: lineCoordinates.y1,
    //                                     x2: lineCoordinates.x2,
    //                                     y2: lineCoordinates.y2
    //                                 });

    //                                 console.log(store);
    //                                 if (chart_two.axisY[0].convertPixelToValue(lineCoordinates.y2) >= miniii && chart_two.axisY[0].convertPixelToValue(lineCoordinates.y2) <= maxiii) {
    //                                     drawLine(chart_two, lineCoordinates, line_count, "center", 10, current_gm, stratum_for_design, test_curr, current_gm);
    //                                     console.log("Calling from" + test_valll[test_curr]);
    //                                     drawHorizontalLine(chart_two, lineCoordinates, line_count, "center", 10, current_gm, stratum_for_design, tt_curr, current_gm);
    //                                     temp++;
    //                                 }

    //                             } else {
    //                                 console.log(store);
    //                                 isDown = false;
    //                                 console.log(lineCoordinates);
    //                                 console.log("Current " + tt_curr);
    //                                 store[current_gm][test_curr].push({
    //                                     x1: lineCoordinates.x1,
    //                                     y1: lineCoordinates.y1,
    //                                     x2: lineCoordinates.x2,
    //                                     y2: lineCoordinates.y2
    //                                 });

    //                                 console.log(store);
    //                                 if (chart_two.axisY[0].convertPixelToValue(lineCoordinates.y2) >= maxiii && chart_two.axisY[0].convertPixelToValue(lineCoordinates.y2) <= miniii) {
    //                                     drawLine(chart_two, lineCoordinates, line_count, "center", 10, current_gm, stratum_for_design, test_curr, current_gm);
    //                                     console.log("Calling from" + test_valll[test_curr]);
    //                                     drawHorizontalLine(chart_two, lineCoordinates, line_count, "center", 10, current_gm, stratum_for_design, tt_curr, current_gm);
    //                                     temp++;
    //                                 }

    //                             }

    //                         }
    //                     } else {
    //                         // document.getElementsByClassName("canvasjs-chart-canvas")[1].style.cursor = "no-drop";
    //                         // document.getElementById("chartContainer").onmousemove = function(){
    //                         //     document.getElementsByClassName("canvasjs-chart-canvas")[1].style.cursor = "no-drop";
    //                         // }
    //                         console.log("not allowed up wala");

    //                         // document.getElementsByClassName("canvasjs-chart-canvas")[1].style.cursor = "no-drop";
    //                         // document.getElementsByClassName("canvasjs-chart-canvas")[1].style.cursor = "no-drop";
    //                     }


    //                 },
    //                 mousemove: function (e) {
    //                     //    console.log(e);
    //                     // console.log($(chart_two.container).data());

    //                     // if (chart_two.axisY[0].convertPixelToValue(e.pageY - parentOffset.top) < maxiii && chart_two.axisY[0].convertPixelToValue(e.pageY - parentOffset.top) > miniii) {
    //                     if (limits[t][0].max == maxiii) {
    //                         document.getElementsByClassName("canvasjs-chart-canvas")[1].style.cursor = "crosshair";
    //                         // console.log(document.getElementsByClassName("canvasjs-chart-canvas")[1].width);
    //                         console.log("allowed");

    //                         if (!isDown) return;
    //                         if (temp == 0 && isDown == true) {
    //                             if (type_of_mode == "mbgl") {
    //                                 lineCoordinates.x2 = e.pageX - parentOffset.left;
    //                                 lineCoordinates.y2 = e.pageY - parentOffset.top;
    //                                 console.log(lineCoordinates);

    //                                 if (lineCoordinates.y1 < lineCoordinates.y2 && chart_two.axisY[0].convertPixelToValue(lineCoordinates.y2) >= miniii && chart_two.axisY[0].convertPixelToValue(lineCoordinates.y2) <= maxiii) {
    //                                     // console.log("min at move for "+stratum_for_design+" : "+min,"max at move "+stratum_for_design+" : "+max);
    //                                     console.log(maxiii, miniii);
    //                                     console.log(ress);
    //                                     refresh();
    //                                     drawLine_mov(chart_two, lineCoordinates);
    //                                 } else {

    //                                 }
    //                             } else {
    //                                 lineCoordinates.x2 = e.pageX - parentOffset.left;
    //                                 lineCoordinates.y2 = e.pageY - parentOffset.top;
    //                                 console.log(lineCoordinates);

    //                                 if (lineCoordinates.y1 < lineCoordinates.y2 && chart_two.axisY[0].convertPixelToValue(lineCoordinates.y2) >= maxiii && chart_two.axisY[0].convertPixelToValue(lineCoordinates.y2) <= miniii) {
    //                                     // console.log("min at move for "+stratum_for_design+" : "+min,"max at move "+stratum_for_design+" : "+max);
    //                                     console.log(maxiii, miniii);
    //                                     console.log(ress);
    //                                     refresh();
    //                                     drawLine_mov(chart_two, lineCoordinates);
    //                                 } else {

    //                                 }
    //                             }

    //                         }



    //                     } else {
    //                         console.log("not allowed");

    //                         // document.getElementsByClassName("canvasjs-chart-canvas")[1].style.cursor = "no-drop";
    //                         document.getElementsByClassName("canvasjs-chart-canvas")[1].style.cursor = "no-drop";
    //                     }

    //                 }
    //             });




    //         }
    //     })
    //     .catch(console.error);


    jQuery(chart_two.container).off({
        mousemove: function (e) {
            console.log("Test");
        }
    });

}

function drawLine_mov(chart, lineCoordinates, col) {
    var ctx = chart.ctx;
    console.log(test_valll[test_curr]);
    ctx.beginPath();
    ctx.strokeStyle = col; //Change Line Color
    ctx.lineWidth = 0.2; //Change Line Width/Thickness
    ctx.moveTo(lineCoordinates.x1, lineCoordinates.y1);
    ctx.lineTo(lineCoordinates.x2, lineCoordinates.y2);
    ctx.stroke();
}

function refresh(col) {
    chart_two.render();
    for (var r = 0; r < store[current_gm][test_curr].length; r++) {
        var ctx = chart_two.ctx;
        var ctx1 = chart_two.ctx;
        ctx.beginPath();
        ctx1.beginPath();
        // console.table(store[current_gm][r]);
        ctx.strokeStyle = store[current_gm][test_curr][r].col; //Change Line Color
        ctx1.strokeStyle = store[current_gm][test_curr][r].col; //Change Line Color
        ctx.lineWidth = 2; //Change Line Width/Thickness
        ctx1.lineWidth = 2; //Change Line Width/Thickness
        ctx.moveTo(store[current_gm][test_curr][r].x1, store[current_gm][test_curr][r].y1);
        // if()
        ctx.lineTo(store[current_gm][test_curr][r].x2, store[current_gm][test_curr][r].y2);
        ctx1.moveTo(35, store[current_gm][test_curr][r].y2);
        // if()
        ctx1.lineTo(document.getElementsByClassName("canvasjs-chart-canvas")[1].width, store[current_gm][test_curr][r].y2);




        ctx.stroke();

        ctx1.stroke();
    }
}

function mouse_fun(min, max, current_gm, stratum_for_design, tt_curr) {

}

function data_get(limit) {
    var dps = [];
    for (var j = 1; j <= save_bh_count; j++) {
        var jj = j - 1;
        var bh = file.get("soilData.boreholeArray." + jj + "");
        var x_val = parseInt(file.get("soilData." + bh + ".stratumData.Eastings"), 10);
        var y_val = parseInt(file.get("soilData." + bh + ".stratumData.Northings"), 10);
        // alert(x_val+','+y_val);
        dps.push({
            indexLabel: String(bh),
            label: String(bh),
            x: x_val,

            y: y_val

        });





    }

    return dps;
}

function dataGenerator(limit) {
    var y = 100;
    var points = [];
    for (var i = 0; i < limit; i += 1) {
        y += (Math.random() * 100 - 50);
        points.push({
            x: i - limit / 2,
            y: y
        });
    }
    return points;
}

function set_new(y1, stra, sec, co_ordinate) {
    // alert(sec + stra + y1);

    var stratum = String(stra);
    var co_ordinate_val = stratum + "_Co_Ordinate";
    design_array_section[sec].push({
        [stratum]: y1,
        [co_ordinate_val]: co_ordinate,
        stratumName: [stratum]
    });
    strata_name_array[sec].push(stratum);

    console.log(design_strata);
    console.log(strata_name_array);
    console.log(design_array_section);

}



function get_sub(array_count, value) {
    var i = 0;
    for (var t = 0; t < array_count.length; t++) {
        if (array_count[t] == value) {
            i++;
        }
    }
    return i;
}



function delete_line(gm,test) {
    // alert(sec_curr);
    // console.log(arr);
    // var arr = file
    // alert(gm + ","+test);
    var stratarr = new Array();
    stratarr = file.get("result.gm_"+gm+"."+test_valll[test]+".Stratum");
    console.log(stratarr);
    Swal.fire({
        title: 'Select the Design Line to delete',
        input: 'select',
        inputOptions: stratarr,
        inputAttributes: {
            autocapitalize: 'off'
        },
        showCancelButton: true,
        confirmButtonText: 'Select',
        showLoaderOnConfirm: true,
        allowOutsideClick: false
    }).then((result) => {
        // alert(result.value);
        var res = result.value;

        file.unset("result.gm_"+gm+"."+test_valll[test]+"."+stratarr[res]);
        file.unset("result.gm_"+gm+"."+test_valll[test]+".Depth."+stratarr[res]);
        file.get("result.gm_"+gm+"."+test_valll[test]+".Stratum").splice(res,1);
        file.save();
        // chart_two.render();

        set_var_val(test);
        //  for(var k = 0 ; k <  file.get("designData.designLines.Coordinates.gm" + sec_curr).length;k++){
        //         if(file.get("designData.designLines.Coordinates.gm" + sec_curr)[k].line_top_of == result.value){
        //             // alert("hurrah");
        //             // arr[sec_curr][k] = null;
        //             alert("First Level");
        //             file.unset("designData.designLines.Coordinates.gm" + sec_curr+"."+k);
        //             // file.set("designData.designLines.Coordinates.gm" + sec_curr+"."+k,null);
        //             file.save();
        //             // console.log
        //         }else{
        //             continue;
        //         }
        //     }
        //  for(var k = 0 ; k < file.get("designData.stratumData.gm" + sec_curr).length;k++){
        //         if(file.get("designData.stratumData.gm" + sec_curr)[k].stratumName == result.value){
        //             // alert("hurrah");
        //             // arr[sec_curr][k] = null;
        //             alert("Second Level");
        //             file.unset("designData.stratumData.gm" + sec_curr+"."+k);
        //             // file.set("designData.stratumData.gm" + sec_curr+"."+k,null);
        //             file.save();
        //         }else{
        //             continue;
        //         }
        //     }
        //  for(var k = 0 ; k <   file.get("designData.strataName.gm" + sec_curr).length;k++){
        //         if(file.get("designData.strataName.gm" + sec_curr)[k] == result.value){
        //         // if(design_array_section[sec_curr][k] == result.value){
        //             // alert("hurrah");
        //             // arr[sec_curr][k] = null;
        //             alert("Third Level");
        //             file.unset("designData.strataName.gm" + sec_curr+"."+k);
        //             // file.set("designData.strataName.gm" + sec_curr+"."+k,null);
        //             file.save();
        //         }else{
        //             continue;
        //         }
        //     }
        //     chart_two.render();
    });


}

function next_output() {

}

function removeDuplicate(arr) {
    var c;
    var len = arr.length;
    var result = [];
    var obj = {};
    for (c = 0; c < len; c++) {
        obj[arr[c]] = 0;
    }
    for (c in obj) {
        result.push(c);
    }
    return result;
}


function eraseSel() {

    var disp = document.getElementById("displayData");

    var tn = document.createTextNode("Enter the stratum Design Line that you want to erase");

    var ip = document.createElement("input");
    ip.setAttribute("id", "ln");
    ip.setAttribute("type", "text");
    ip.setAttribute("value", "");

    disp.appendChild(tn);
    disp.appendChild(ip);

    var button = document.createElement('button');
    button.setAttribute('type', 'button');
    button.setAttribute('id', 'btn');
    button.innerHTML = "remove";
    button.setAttribute('onclick', 'remove()');

    disp.appendChild(button);
}

function remove() {

    var l_num = document.getElementById("ln").value;
    console.log(l_num);
    // alert(l_num);
    // var removed = arr.splice(l_num - 1, l_num - 1);
    var coords = file.get("designData.stratumData." + sec_curr);
    var coords1 = file.get("designData.strataName.1");
    // var removed = arr.splice(l_num - 1, l_num - 1);
    for (var i = 0; i < coords.length; i++) {
        if (coords[i].stratumName == l_num) {
            coords.splice(i, 1);
        }
    }
    for (var i = 0; i < coords1.length; i++) {
        if (coords1[i] == l_num) {
            coords1.splice(i, 1);
        }
    }

    // if(l_num == 1) {
    //     var rmv = arr.splice(l_num - 1,l_num);
    // }

    // else {
    //     rmv = arr.splice(l_num - 1,1);
    // }
    // console.log(rmv);
    // console.log(arr);
    var remove_val = file.get("sectionLines.total");

    file.set("designData.stratumData." + sec_curr + "", coords);
    console.log(file.set("designData.stratumData." + sec_curr, coords));
    file.set("designData.strataName.1", coords1);
    // file.set("sectionLines.total", remove_val - 1);
    file.save();
    chart_two.render();
    // getLines();
}

function redo() {

    ctx1.redo();
}

function undo() {

    // ctx1.undo();
    console.log(store);
    store.pop();
    console.log(store);

}

function chartt() {
    var scale_x_val = document.getElementById("scale_x").value;
    var scale_y_val = document.getElementById("scale_y").value;
    chart_two.axisY[0].set("maximum", scale_y_val);
    chart_two.axisX2[0].set("maximum", scale_x_val);

    // alert("ds");
}

function eraseAll() {

    w = chart_two.width;
    h = chart_two.height;

    var ctx = chart_two.ctx;

    var m = confirm("Want to clear");

    if (m) {
        chart_two.render();
    }
}

$(document).ready(function () {
    $("#profile_line").click(function () {
        add_line_design_prof(test_curr, current_gm);
        // alert(test_curr);
        // alert(current_gm);
    });

});

// UndoCanvas.disableUndo(ctx1);