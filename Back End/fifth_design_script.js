var sec_curr;
var ipcRenderer = require('electron').ipcRenderer;
var editJsonFile = require("edit-json-file");
var section_num_val;
var remote = require('electron').remote;
const app = remote.app;
var Swal = require("sweetalert2");
// var tempFilePath = app.getPath('temp');
var tempFilePath = remote.getGlobal('path_file');
// var Color = ["#aeabab", "#ffff00", "#9cc2e5", "#c55a11"];
var myData = new Array();
var val = new Array();
var sections = new Array();
var section_list = new Array();
var section_list_data = new Array();
var borehole_count;
var i;
var chart_two;
var points = [];
var section_number;
var design_array_section = new Array();
// design_array_section[0] = "Null Section";
var line_count;
var curr_sec;
var strata_name_array = [];
// strata_name_array[0] = "Null Section";
var design_strata = new Array();
var click_gm = 0;
let file = editJsonFile(tempFilePath);
var project_title = document.getElementById('projectTitle');
var project_name = file.get("projectName");
var project_date = file.get("dateCreated");
// project_title.innerHTML = "" + project_name + ".para | " + project_date + "";
document.title = "" + project_name + " | Sigma Soil V 2019.2.0.1 | Let's Pile ";
var save_bh_count = file.get("soilData.noOfBh");
const prompt = require('electron-prompt');
var section_count = file.get("soilData.sectionData.noOfSection");
var type_of_mode = file.get("projectDetails.typeOfMode");
var arr = new Array();
var lc_arr = new Array();
var line_count = 0;
var gm_count = file.get("soilData.groundModelData.noOfGm");
var sec_key;
console.log(section_count);
var menu_div = document.getElementById("menu_f");
var ctx1;
var sec;
sec = 0;
var item, prevsec = 0;
var oldsec, newsec;
var section_array = new Object();
var section_num;
var stratum_defined_list = ["Made Ground", "Rock", "Clay",
    "Sand",
    "Gravel",
    "Silt",
    "Chalk",
    "Limestone",
    "Mudstone",
    "Sandstone",
    "Void",
    "Flint",
    "Peat"
];
const {
    Menu,
    MenuItem
} = remote
var arrofstrata = {};
const menu = new Menu()
// menu.append(new MenuItem({
//     label: 'Section Tools',
//     submenu: [{
//             'label': 'Add Section Line'
//         },
//         {
//             'label': ' Line'
//         }
//     ]
// }))
menu.append(new MenuItem({
    label: 'Draw Design Line',
    click() {
        add_line_design('normal')
    }
}))
menu.append(new MenuItem({
    label: 'Draw GW Line',
    click() {
        add_line_design('ground_water')
    }
}))

menu.append(new MenuItem({
    label: 'Delete Design Line',
    click() {
        del_section_var()
    }
}))

// menu.append(new MenuItem({
//     type: 'separator'
// }))
// menu.append(new MenuItem({
//     label: 'Show Tour',
//     click() {
//         introJs().start();
//     }
// }))

// Prevent default action of right click in chromium. Replace with our menu.
window.addEventListener('contextmenu', (e) => {
    e.preventDefault()
    menu.popup(remote.getCurrentWindow())
}, false)
if (file.get("designData.save") == 0) {
    var sec = 0;

} else {
    // var sec = file.get("soilData.groundModelData.noOfGm");
    // alert(sec);
    // $(document).ready(function () {
    // your code

    // load_section();
    // });


}

function section_fun() {

    sec = sec + 1;

    section_array[sec] = new Array();
    var sec_tool = document.getElementById("section_toolbar");
    var sec_but = document.createElement("Button");
    sec_but.setAttribute("class", "btn-success");
    sec_but.setAttribute("onclick", "sec_call(" + sec + ")");
    var sec_but_text = document.createTextNode("Ground Model " + sec);
    sec_but.appendChild(sec_but_text);
    sec_tool.appendChild(sec_but);

    // int
}

function get_sec() {

    for (var l = 1; l <= sec; l++) {
        // alert(section_array[l]);
        // file.set("soilData.groundModelData.gm" + l + "", section_array[l]);


    }


}

function sec_call(sec_key) {
    // alert(sec_key);
    console.log(sec_key);
    var sec_det = document.getElementById("section_details");
    sec_det.innerHTML = "Section " + sec_key + ": " + section_array[sec_key] + "";
    section_number = sec_key;
}

function get_section() {
    for (var f = 1; f <= gm_count; f++) {
        if(file.get("soilData.groundModelData.gm"+f) == null){
            continue;
        }else{
        var section_button = document.createElement("BUTTON");
        section_button.setAttribute("class", "col-sm-2 btn btn-outline-success");
        section_button.setAttribute("style", "margin-top:10px; margin-left:10px");
        section_button.setAttribute("onclick", "display_section('" + f + "')");
        var section_text = document.createTextNode("Ground Model " + f);
        section_button.appendChild(section_text);
        document.getElementById("menu_f").appendChild(section_button);
        var div_but = document.getElementById("menu_f");
        var buts = div_but.getElementsByClassName("btn btn-outline-success");
        // console.log(el);
        for (var i = 0; i < buts.length; i++) {
            console.log(buts[i]);
            buts[i].addEventListener("click", function () {
                var current = document.getElementsByClassName("active");
                current[0].className = current[0].className.replace(" active", "");
                this.className += " active";
            });
        }
    }
    }
    var intro = introJs();
    intro.setOptions({
        steps: [{
                element: '#menu_f',
                intro: "Click here to view the Section Graph"
            },
            {
                element: '#chartContainer',
                intro: "Respective Groundmodel Selected",
                position: 'right',
                scrollTo: 'tooltip'
            },
            {
                element: '#toolbar_section',
                intro: 'Here are the ToolBar for Drawing Reference Line',
                position: 'left'
            },
            {
                element: '#but_add_line',
                intro: '<strong>Click Here </strong> To Design Stratum  .'


            },
            {
                element: '#chartContainer',
                intro: "Now click on the Borehole Symbol to group into Respective Groundmodel Selected Like this <br> <img style='width:100%;height:200px;' src='./assets/img/stratum_design.gif'>",
                position: 'bottom',
                scrollTo: 'tooltip'
            }
        ]
    });
    intro.start();

}


myData = localStorage.getItem(myData);
console.log(section_list_data);
console.log(myData);
if (myData) {
    val = JSON.parse(myData);
}



i = val[0];

console.log(sections);
var menu_arr=[];
function update_menu(arr_str_gm, str_gm){
var tt = Object.keys(arr_str_gm);
var labelarr = [];
console.log(tt[0]);
for(var i = 0 ;  i < tt.length;i++){
    // menu.append(new MenuItem({
    //     label: tt[i],
    //     click() {
    //         add_line_design(''+tt[i]+'')
    //     }
    // }))
    var lab =  tt[i]
labelarr.push({
    label:tt[i]

})
}
menu.append(new MenuItem({
    label: 'GM '+ str_gm,
    submenu: labelarr
}))

}


function sixth() {


    // file.set("soilData.groundModelData.noOfGm", sec);

    file.save();

    location.href = "sixth.html";

}


function design() {
    console.log(design_array_section);
}

const CanvasJS = require('./assets/javascript/canvasjs.min.js');

$.myjQuery = function (section_num) {
    var dps = []; // dataPoints
    var s_one = []; // dataPoints
    var s_two = []; // dataPoints
    var s_three = []; // dataPoints
    var y_data = [];
    var strips = [];
    var save_data = new Array();
    chart_two = new CanvasJS.Chart("chartContainer", {
        // zoomEnabled: true,
        dataPointWidth: 5,
        interactivityEnabled: true,
        zoomType: "xy",
        exportFileName: "Design plot",
        exportEnabled: true,
        animationEnabled: true,
        // title: {
        //     text: "Design Plot",
        //     fontFamily: "Roboto",
        //     fontWeight: 'bold',
        //     fontsize : 12,
        //     horizontalAlign: "center"
        // },
        axisX: {
            title: "Chainage ( m )",
            // title: "Eastings",
            titleFontSize: 12,
            labelFontSize: 8,
        },
        axisX2: {
            labelFontSize: 8,
            titleFontSize: 12,
            title: "Chainage ( m )",
            gridDashType: "dot",
            gridThickness: 1,
            tickLength: 15,
            tickColor: "black",
            crosshair: {
                enabled: true //disable here
            }
        },
        dataPointWidth: 20,
        axisY: y_data,
        toolTip: {
            content: "{name} in {label}<br> (Start,End) : {y} <br> Chainage of BH : {chainage}"
        },

        data: save_data

    });


    if (type_of_mode == "mbgl") {
        y_data.push({
            'stripLines': strips,
            'title': "Depth ( m bgl )",

            'titleFontSize': "12",
            'labelFontSize': "8",
            'gridDashType': "dot",
            'gridThickness': 1,
            'valueFormatString': "##.##",
            'reversed': true,
            'tickLength': 15,
            'tickColor': "black",
            'crosshair': {
                'enabled': true //disable here
            }
        });
    } else {
        y_data.push({
            'stripLines': strips,
            'title': "Elevation ( mAOD )",
            'gridDashType': "dot",
            'labelFontSize': "8",
            'titleFontSize': "12",
            'gridThickness': 1,
            'valueFormatString': "##.##",
            'tickLength': 15,
            'tickColor': "black",
            'reversed': false,
            'includeZero':false,
            'crosshair': {
                'enabled': true
            } //disable here
        });
    }
    // title: "Depth",
    // gridDashType: "dot",

    // gridThickness: 1,



    var strata_name_dps = new Array();
    var gw_name_dps = new Array();
    save_data.push({
        'type': 'rangeColumn',
        'axisXType': "secondary",
        'cursor': 'crosshair',
        // 'stripLines': strips,
        'LegendText': 'Stratum',

        'dataPoints': strata_name_dps
    });
    save_data.push({
        'type': 'scatter',
        'lineDashType': 'dash',
        'lineColor': 'blue',
        'markerType': 'none',
        'markerColor': '#0000ff',

        'axisXType': "secondary",
        // 'stripLines': strips,
        'LegendText': 'Ground Water Level',
        'dataPoints': gw_name_dps
    });

    for (var g = 1; g <= file.get("soilData.groundModelData.gm" + section_num + ".length"); g++) {
        var jj = g - 1;
        var bh = file.get("soilData.groundModelData.gm" + section_num + "." + jj);



        for (var f = 3; f < file.get("soilData.params." + bh + ".length"); f++) {

            var strata_name = file.get("soilData.params." + bh + "." + f);

            var val1 = file.get("soilData." + bh + ".stratumData." + strata_name + ".start");
            var val2 = file.get("soilData." + bh + ".stratumData." + strata_name + ".end");

            strata_name_dps.push({

                label: String(bh),
                name: strata_name,
                // chainage : file.get("sectionLines.data.section_"+section_num+"."+bh),
                // x: file.get("sectionLines.data.section_"+section_num+"."+bh),
                x: parseInt(file.get("soilData." + bh + ".stratumData.Eastings"), 10),
                y: [parseInt(val1, 10), parseInt(val2, 10)],
                color: file.get("soilData." + bh + ".stratumData." + strata_name + ".strataColor")
            });





        }
        for (var f = 3; f < file.get("soilData.params." + bh + ".length"); f++) {

            var pos_x = parseInt(file.get("soilData." + bh + ".stratumData.Eastings"), 10);
            var val_x = pos_x;

            gw_name_dps.push({



                x: parseInt(file.get("soilData." + bh + ".stratumData.Eastings"), 10),
                y: parseInt(file.get("soilData." + bh + ".stratumData.Groundwater"), 10),
                markerImageUrl: "./assets/img/1.png"

            });

        }


    }






    var customMarkers = [];
    // chart_two.render();
    ctx1 = chart_two.ctx;
    if (file.get("designData.designLines.save") == 0) {
        // var arr = new Array();
        for (var t = 1; t <= gm_count; t++) {
            arr[t] = new Array();
            // line_in_gm = file.get("designData.designLines.Coordinates.gm"+t).length;
            // console.log(line_in_gm);



        }
        // chart_two.render();
    } else {
        for (var l = 1; l <= gm_count; l++) {
            // alert(gm_count);
            arr[l] = new Array();
            // line_in_gm = file.get("designData.designLines.Coordinates.gm" + t).length;
            arr[l] = file.get("designData.designLines.Coordinates.gm" + l);
            if (typeof arr[l] === "undefined") {
                arr[l] = new Array();
            }
            console.log(arr[l]);
            // alert(section_number);
            // drawdefinedLine(chart_two, file.get("designData.designLines.total"), sec_curr, ctx1);


        }
        for (var design_strata_index = 0; design_strata_index < file.get("soilData.groundModelData.noOfGm"); design_strata_index++) {
            // alert(sec_curr);
            var dum = design_strata_index + 1;
            for (var d_s = 0; d_s < file.get("designData.strataName.gm" + sec_curr + ".length"); d_s++) {
                var design_Strata = file.get("designData.strataName.gm" + sec_curr + "." + d_s);
                console.log(design_Strata);
                if(design_Strata ==  null){
                    continue;
                }else{
                    drawDesignLine(chart_two, file.get("designData.stratumData.gm" + sec_curr + "." + d_s + "." + design_Strata), file.get("designData.stratumData.gm" + sec_curr + "." + d_s));
                }
                // alert(design_Strata);


                // strips.push({
                //     value: 8,
                //     // label: "labelw",
                //     lineDashType: "dot",
                //     thickness: 3
                // });

                // console.log(file.get("designData.stratumData." + current_gm + "." + d_s + "." + design_Strata));
            }

        }
        // var arr = new Array();


        // chart_two.render();
        // arr = file.get("sectionLines.Coordinates");
        // console.log(arr);

    }
    chart_two.render();
    UndoCanvas.enableUndo(ctx1);
    addMarkerImages(chart_two);



    function addMarkerImages(chart_two) {
        for (var i = 0; i < chart_two.data[1].dataPoints.length; i++) {

            customMarkers.push($("<img>").attr("src", chart_two.data[1].dataPoints[i].markerImageUrl)
                .css("display", "none")
                .css("height", 15)
                .css("width", 15)
                .appendTo($("#chartContainer>.canvasjs-chart-container"))
            );
            positionMarkerImage(customMarkers[i], i);
        }
    }

    function positionMarkerImage(customMarker, index) {
        var pixelX = chart_two.axisX2[0].convertValueToPixel(chart_two.options.data[1].dataPoints[index].x);
        var pixelY = chart_two.axisY[0].convertValueToPixel(chart_two.options.data[1].dataPoints[index].y);

        customMarker.css({
            "position": "absolute",
            "display": "block",
            "top": pixelY - customMarker.height() / 2,
            "left": pixelX - customMarker.width() / 2
        });
    }

    $(window).resize(function () {
        for (var i = 0; i < chart_two.data[1].dataPoints.length; i++) {
            positionMarkerImage(customMarkers[i], i);
        }
    });
    var lineCoordinates = {};
    var parentOffset = $(chart_two.container).offset();

    // jQuery(chart_two.container).on({

    //     mousedown: function (e) {

    //         if (e.clientX <= 170) {
    //             lineCoordinates.x1 = 0;
    //             lineCoordinates.y1 = e.pageY - parentOffset.top;

    //             lineCoordinates.x2 = chart_two.width;
    //             lineCoordinates.y2 = lineCoordinates.y1;
    //             alert(lineCoordinates.y2);

    //             drawLine(chart_two, lineCoordinates);
    //         } else {

    //         }


    //     }


    // });



    var j = 0;

    function drawDesignLine(chart, y, x) {
        // var starta_area = document.getElementById("stratum_declare_area");
        // alert(y);
        console.log(y);
        // console.log(pos);
        // var h =  24;
        // var pos = Math.round(chart.convertPixelToValue(y));
        // var select_div = document.createElement("div");
        // select_div.offsetTop =  lineCoordinates.y1;
        // select_div.setAttribute("style", "position : absolute; top :" + co_ord + "px");
        // var select_checkbox = document.createElement("input");
        // select_checkbox.setAttribute("type", "radio");
        // select_checkbox.setAttribute("name", "check");
        // select_checkbox.setAttribute("id","check");
        // select_checkbox.setAttribute("value", design_Stratum);

        // var select = document.createElement("input");
        // select.setAttribute("Value", design_Stratum);
        // select.setAttribute("type", "text");
        // select.setAttribute("list", "stratum");
        // select.setAttribute("id", "stratum_" + test_curr + "_" + gm_curr + "_" + design_Stratum);
        // select.setAttribute("style","marginTop : 300px");
        // select_div.appendChild(select);
        // select_div.appendChild(select_checkbox);
        // select.offsetTop = lineCoordinates.y1;
        // select.offsetLeft = lineCoordinates.x1;
        // $("")
        // select.setAttribute("list","stratum");
        // starta_area.appendChild(select_div);
        // var design_plot = Math.round(chart.axisY[0].convertPixelToValue(y.y1));
        // alert(design_plot);
// if(x.stratumName != null){
    // alert(x.strata_name);
    if(x.stratumName == "ground_Water"){
        strips.push({
            value: y,

            lineDashType: "dot",

            color: '#0000ff',
            lineDashType: "solid",
            showOnTop: true,
            thickness: 2
        });
    }else{
        strips.push({
            value: y,
            label: x.stratumName  ,
            lineDashType: "dot",
            labelFontColor:"black",
            color: '#000000',
            lineDashType: "solid",
            showOnTop: true,
            labelFontSize:8,
            thickness: 2
        });
    }

// }


    }
    console.log(strips);

}
function delete_line(){
    // alert(sec_curr);
    // console.log(arr);
    // var arr = file
    Swal.fire({
        title: 'Select the Stratum Line to delete',
        input: 'select',
        inputOptions:  arrofstrata[sec_curr],
        inputAttributes: {
            autocapitalize: 'off'
        },
        showCancelButton: false,
        confirmButtonText: 'Select',
        showLoaderOnConfirm: true,
        allowOutsideClick: false
    }).then((result) => {
        // alert(result)
 for(var k = 0 ; k <  file.get("designData.designLines.Coordinates.gm" + sec_curr).length;k++){
        if(file.get("designData.designLines.Coordinates.gm" + sec_curr)[k].line_top_of == result.value){
            // alert("hurrah");
            // arr[sec_curr][k] = null;
            alert("First Level");
            file.unset("designData.designLines.Coordinates.gm" + sec_curr+"."+k);
            // file.set("designData.designLines.Coordinates.gm" + sec_curr+"."+k,null);
            file.save();
            // console.log
        }else{
            continue;
        }
    }
 for(var k = 0 ; k < file.get("designData.stratumData.gm" + sec_curr).length;k++){
        if(file.get("designData.stratumData.gm" + sec_curr)[k].stratumName == result.value){
            // alert("hurrah");
            // arr[sec_curr][k] = null;
            alert("Second Level");
            file.unset("designData.stratumData.gm" + sec_curr+"."+k);
            // file.set("designData.stratumData.gm" + sec_curr+"."+k,null);
            file.save();
        }else{
            continue;
        }
    }
 for(var k = 0 ; k <   file.get("designData.strataName.gm" + sec_curr).length;k++){
        if(file.get("designData.strataName.gm" + sec_curr)[k] == result.value){
        // if(design_array_section[sec_curr][k] == result.value){
            // alert("hurrah");
            // arr[sec_curr][k] = null;
            alert("Third Level");
            file.unset("designData.strataName.gm" + sec_curr+"."+k);
            // file.set("designData.strataName.gm" + sec_curr+"."+k,null);
            file.save();
        }else{
            continue;
        }
    }
    chart_two.render();
    });


}
// Erase Selected
function eraseSel() {

    var disp = document.getElementById("displayData");

    var tn = document.createTextNode("Enter the line number that you want to erase");

    var ip = document.createElement("input");
    ip.setAttribute("id", "ln");
    ip.setAttribute("type", "number");
    ip.setAttribute("value", "");

    disp.appendChild(tn);
    disp.appendChild(ip);

    var button = document.createElement('button');
    button.setAttribute('type', 'button');
    button.setAttribute('id', 'btn');
    button.innerHTML = "remove";
    button.setAttribute('onclick', 'remove(' + sec_curr + ')');

    disp.appendChild(button);
}
// Load Sections
function load_section() {
    for (var i = 1; i <= sec; i++) {
        console.log(file.get("soilData.sectionData.section_" + i));
        section_fun_load(i, file.get("soilData.sectionData.section_" + i));
        // section_array[i] = new Array();
    }

}
// To Delete Lines Not Needed
function remove(sec_curr) {

    var l_num = document.getElementById("ln").value;
    console.log(l_num);
    // alert(l_num);
    // var removed = arr.splice(l_num - 1, l_num - 1);
    var coords = file.get("designData.designLines.Coordinates.gm" + sec_curr);
    // var removed = arr.splice(l_num - 1, l_num - 1);
    for (var i = 0; i < coords.length; i++) {
        if (coords[i].line_no == l_num) {
            arr[sec_curr].splice(i, 1);
        }
    }
    // if(l_num == 1) {
    //     var rmv = arr.splice(l_num - 1,l_num);
    // }

    // else {
    //     rmv = arr.splice(l_num - 1,1);
    // }
    // console.log(rmv);
    console.log(arr);
    var remove_val = file.get("designData.designLines.total");

    file.set("designData.designLines.Coordinates.gm" + sec_curr, arr[sec_curr]);
    file.set("designData.designLines.total", remove_val - 1);


    getLines();
}

function getLines() {
    // chart.render();
    chart_two.render();
    if (file.get("designData.designLines.save") == 0) {
        // var arr = new Array();
    } else {
        // var arr = new Array();
        for (var t = 1; t <= gm_count; t++) {
            arr[t] = new Array();
            line_in_gm = file.get("designData.designLines.Coordinates.gm" + t).length;
            arr[t] = file.get("designData.designLines.Coordinates.gm" + t);
            console.log(arr[t]);
            // alert(section_number);
            drawdefinedLine(chart_two, file.get("designData.designLines.total"), sec_curr, ctx);
        }
        line_count = file.get("designData.designLines.lastLineNo");
        // arr = file.get("sectionLines.Coordinates");
        // console.log(arr);
        // drawdefinedLine(chart, file.get("sectionLines.total"),section_number);
    }

}

function set(y1, stra, sec, co_ordinate , lc) {
    // alert(sec + stra + y1);

    var stratum = String(stra);
    var co_ordinate_val = stratum + "_Co_Ordinate";
    console.log(sec);
    console.log(design_array_section[sec]);
    design_array_section[sec].push({
        [stratum]: y1,
        [co_ordinate_val]: co_ordinate,
        stratumName: stratum,
        line_count : lc
    });
    console.log(design_array_section[sec]);
    strata_name_array[sec].push(stratum);

    console.log(design_strata);
    console.log(strata_name_array);
    console.log(design_array_section);

}
// Scaling Borehole Scatter Plot

function chartt() {
    var scale_x_val = document.getElementById("scale_x").value;
    var scale_y_val = document.getElementById("scale_y").value;
    chart.axisY[0].set("maximum", scale_y_val);
    chart.axisX[1].set("maximum", scale_x_val);

    // alert("ds");
}
// Getting Svaed Lines if available
function drawdefinedLine(chart, total, gm_key, ctx) {
    // chart.render();
    // alert(gm_key);
    // alert(sec_curr);
    // var ctx = chart.ctx;
    console.log(arr[gm_key]);
    for (var i = 0; i < arr[gm_key].length; i++) {
        // ctx.beginPath();     //Change Line Width/Thickness
        var ff = [];
        // ff = file.get("sectionLines.Coordinates."+i);
        ff = arr[gm_key][i];
        drawLinedefined(ff, ctx);

        // console.log(ff);

    }
}
// Drwaing the Lines got from saved file
function drawLinedefined(ff, ctx1) {
    // console.log(ff);
    // var ctx1 = chart_two.ctx;
    ctx1.beginPath();
    ctx1.moveTo(ff.x1, ff.y1);
    ctx1.lineTo(ff.x2, ff.y2);
    ctx1.stroke();

    drawLabel(chart_two, ff.line_top_of, ff, "center", 0);
}

function add_line_design(type) {
    // alert("done level one");

    var temp = 0;
    // chart.get("zoomEnabled")
    // chart.set("zoomEnabled", false);
    // chart.set("zoomEnabled",  false);
    // chart.set("zoomType", "xy");
    var lineCoordinates = {};

    var parentOffset = $(chart_two.container).offset();
    jQuery(chart_two.container).on({
        mousedown: function (e) {

            lineCoordinates.x1 = 50;
            lineCoordinates.y1 = e.pageY - parentOffset.top;

            // lineCoordinates.x2 = chart_two.width;
            lineCoordinates.x2 = chart_two.width;
            // lineCoordinates.x2 = chart_two.axisX2[0].convertValueToPixel(chart_two.axisX2[0].get("maximum"));
            lineCoordinates.y2 = lineCoordinates.y1;
            // alert(lineCoordinates.y2);
            if (temp == 0) {
                // chart.set("zoomEnabled", false);
                // alert("You have Chosen to Create Design for " + type);
                drawLine(chart_two, lineCoordinates, "center", 10, sec_curr, type);

                // drawLabel(chart,line_count,lineCoordinates, "center",10);
                temp++;
                // chart.set("zoomEnabled", true);
            }


        }
    });



}

function data_get(limit) {
    var dps = [];
    for (var j = 1; j <= save_bh_count; j++) {
        var jj = j - 1;
        var bh = file.get("soilData.boreholeArray." + jj + "");
        var x_val = parseInt(file.get("soilData." + bh + ".stratumData.Eastings"), 10);
        var y_val = parseInt(file.get("soilData." + bh + ".stratumData.Northings"), 10);
        // alert(x_val+','+y_val);
        dps.push({
            indexLabel: String(bh),
            label: String(bh),
            x: x_val,

            y: y_val

        });



        // var points = [];




        // }
        // console.log(points);
        // return points;


    }

    return dps;
}

function dataGenerator(limit) {
    var y = 100;
    var points = [];
    for (var i = 0; i < limit; i += 1) {
        y += (Math.random() * 100 - 50);
        points.push({
            x: i - limit / 2,
            y: y
        });
    }
    return points;
}

function drawLabel(chart, text, lineCoordinates, alignment, padding , lc) {
    if (!alignment) alignment = 'center';
    if (!padding) padding = 0;
    var p1 = [lineCoordinates.x1, lineCoordinates.y1];
    var p2 = [lineCoordinates.x2, lineCoordinates.y2];
    // alert(text);
    var text1 = text;
     var dx = lineCoordinates.x2 - lineCoordinates.x1;
    var dy = lineCoordinates.y2 - lineCoordinates.y1;
    var len = Math.sqrt(dx * dx + dy * dy);
    var avail = len - 2 * padding;
    var angle = Math.atan2(dy, dx);
    if (angle < -Math.PI / 2 || angle > Math.PI / 2) {
        var p = p1;
        p1 = p2;
        p2 = p;
        dx *= -1;
        dy *= -1;
        angle -= Math.PI;
    }

    var p, pad;
    if (alignment == 'center') {
        p = p1;
        pad = 1 / 2;
    } else {
        var left = alignment == 'center';
        p = left ? p1 : p2;
        pad = padding / len * (left ? -1 : 1);
    }


    var ctx1 = chart.ctx;
    ctx1.save();
    ctx1.textAlign = alignment;
    ctx1.translate(lineCoordinates.x2 - 50, lineCoordinates.y1);
    // alert(lineCoordinates.x1,lineCoordinates.y1);
    ctx1.rotate(Math.atan2(dy, dx));
    ctx1.fillStyle = "black"
    ctx1.fillText(text1, 0, 0);
    ctx1.restore();
}

function drawLine(chart, lineCoordinates, alignment, padding, section_number, type) {
    line_count++;
    if (!alignment) alignment = 'center';
    if (!padding) padding = 0;

    var dx = lineCoordinates.x2 - lineCoordinates.x1;
    var dy = lineCoordinates.y2 - lineCoordinates.y1;
    var p, pad;
    if (alignment == 'center') {
        p = [lineCoordinates.x1, lineCoordinates.y1];
        pad = 1 / 2;
    } else {
        var left = alignment == 'center';
        p = left ? [lineCoordinates.x1, lineCoordinates.y1] : [lineCoordinates.x2, lineCoordinates.y2];
        pad = padding / Math.sqrt(dx * dx + dy * dy) * (left ? 1 : -1);
    }
    // for (var index_s = 0; index_s < file.get("designData.strataName.gm" + current_gm).length; index_s++) {
    // for (var index_s = 0; index_s < file.get("soilData.groundModelData.gm" + current_gm).length; index_s++) {
    //     // var val = file.get("designData.strataName.gm" + current_gm + "." + index_s);
    //     // arrofstrata[val] = '' + val + ''
    //     console.log(file.get("soilData.groundModelData.gm" + current_gm+"."+index_s));
    // }


    ctx1.beginPath();
    if (type == 'ground_water') {
        ctx1.strokeStyle = "#0000ff"; //Change Line Color
        // ctx1.lineWidth = $('#selWidth').val();
    } else {

        ctx1.strokeStyle = "#000000"; //Change Line Color
        ctx1.lineWidth = "#000000";
    }

    ctx1.moveTo(lineCoordinates.x1, lineCoordinates.y1);
    ctx1.lineTo(lineCoordinates.x2, lineCoordinates.y2);
    var design_plot = Math.round(chart.axisY[0].convertPixelToValue(lineCoordinates.y1));

    // file.set("section_deign")
    // ctx.textAlign = alignment;
    // ctx.translate(p.x+dx*pad,p.y+dy*pad);
    // ctx.rotate(Math.atan2(dy,dx));
    // ctx.fillText(text,0,0);
    // ctx.restore();
    // arr.push({
    // arr.push({

    // });
    if (type == 'ground_water') {
        set(design_plot, "ground_Water", sec_curr, lineCoordinates.y1 , line_count);
        drawLabel(chart, "Ground Water Line", lineCoordinates, "center", 10,line_count);
        arr[sec_curr].push({
            line_no: line_count,
            x1: lineCoordinates.x1,
            x2: lineCoordinates.x2,
            y1: lineCoordinates.y1,
            y2: lineCoordinates.y2,
            plot_value: design_plot,
            line_top_of: "Ground Water",
            gm: sec_curr
        });
        console.log(arr);
        file.set("designData.designLines.Coordinates.gm" + sec_curr, arr[sec_curr]);
        var total = 0;
        for (var g = 1; g <= gm_count; g++) {
            total = total + arr[g].length;
        }

        file.set("designData.designLines.total", total);
        file.set("designData.designLines.lastLineNo", line_count);
        console.log(design_array_section[sec_curr]);
        file.set("designData.stratumData.gm" + sec_curr, design_array_section[sec_curr]);
        file.set("designData.strataName.gm" + sec_curr, strata_name_array[sec_curr]);
        // file.set("designData.designLines.Coordinates.line_top_of", r);
        // file.set("designData.designLines.Coordinates.gm", section_number);
        file.set("designData.designLines.save", 1);
        file.set("designData.save", 1);
        file.save();
        ctx1.stroke();
    } else {
        Swal.fire({
            title: 'Select the Stratum',
            input: 'select',
            inputOptions:  arrofstrata[sec_curr],
            inputAttributes: {
                autocapitalize: 'off'
            },
            showCancelButton: false,
            confirmButtonText: 'Select',
            showLoaderOnConfirm: true,
            allowOutsideClick: false
        }).then((result) => {
            if (result) {
                //   Swal.fire({
                //     title: result
// console.log(result);

                //   })
                var res = result.value;
                // res.replace(/ +/g, "");
                 delete arrofstrata[sec_curr].res;
                console.log(result.value);
                set(design_plot,result.value, sec_curr, lineCoordinates.y1 , line_count);
                drawLabel(chart,result.value, lineCoordinates, "center", 10 ,line_count);
                arr[sec_curr].push({
                    line_no: line_count,
                    x1: lineCoordinates.x1,
                    x2: lineCoordinates.x2,
                    y1: lineCoordinates.y1,
                    y2: lineCoordinates.y2,
                    line_top_of:result.value,
                    plot_value: design_plot,
                    gm: sec_curr
                });
                file.set("designData.designLines.Coordinates.gm" + sec_curr, arr[sec_curr]);
                var total = 0;
                for (var g = 1; g <= gm_count; g++) {
                    total = total + arr[g].length;
                }
                console.log(design_array_section[sec_curr]);

                file.set("designData.designLines.total", total);
                file.set("designData.designLines.lastLineNo", line_count);
                file.set("designData.stratumData.gm" + sec_curr, design_array_section[sec_curr]);
                file.set("designData.strataName.gm" + sec_curr, strata_name_array[sec_curr]);
                // file.set("designData.designLines.Coordinates.line_top_of", r);
                // file.set("designData.designLines.Coordinates.gm", section_number);
                file.set("designData.designLines.save", 1);
                file.set("designData.save", 1);
                file.save();

                // }

                ctx1.stroke();



            }
        })
        // prompt({
        //         title: 'Stratum input',
        //         label: 'Stratum:',
        //         id: 'gg',
        //         value: '',
        //         type: 'select',
        //         selectOptions: arrofstrata
        //     })
        // .then((r) => {
        //     if (r === null) {
        //         console.log('user cancelled');
        //     } else {
        //         // alert(sec_key);
        //         // alert(r.value);
        //         // if (type == 'ground_water') {

        //         // set(design_plot, "ground_Water", sec_curr, lineCoordinates.y1);
        //         // drawLabel(chart, "Ground Water Line", lineCoordinates, "center", 10);
        //         // arr[sec_curr].push({
        //         //     line_no: line_count,
        //         //     x1: lineCoordinates.x1,
        //         //     x2: lineCoordinates.x2,
        //         //     y1: lineCoordinates.y1,
        //         //     y2: lineCoordinates.y2,
        //         //     plot_value: design_plot,
        //         //     line_top_of: "Ground Water",
        //         //     gm: sec_curr
        //         // });
        //         // console.log(arr);
        //         // file.set("designData.designLines.Coordinates.gm" + sec_curr, arr[sec_curr]);
        //         // var total = 0;
        //         // for (var g = 1; g <= gm_count; g++) {
        //         //     total = total + arr[g].length;
        //         // }

        //         // file.set("designData.designLines.total", total);
        //         // file.set("designData.designLines.lastLineNo", line_count);
        //         // console.log( design_array_section[sec_curr]);
        //         // file.set("designData.stratumData.gm"+sec_curr, design_array_section[sec_curr]);
        //         // file.set("designData.strataName.gm"+sec_curr, strata_name_array[sec_curr]);
        //         // // file.set("designData.designLines.Coordinates.line_top_of", r);
        //         // // file.set("designData.designLines.Coordinates.gm", section_number);
        //         // file.set("designData.designLines.save", 1);
        //         // file.set("designData.save", 1);

        //         // } else {

        // set(design_plot, r, sec_curr, lineCoordinates.y1);
        // drawLabel(chart, r, lineCoordinates, "center", 10);
        // arr[sec_curr].push({
        //     line_no: line_count,
        //     x1: lineCoordinates.x1,
        //     x2: lineCoordinates.x2,
        //     y1: lineCoordinates.y1,
        //     y2: lineCoordinates.y2,
        //     line_top_of: r,
        //     plot_value: design_plot,
        //     gm: sec_curr
        // });
        // file.set("designData.designLines.Coordinates.gm" + sec_curr, arr[sec_curr]);
        // var total = 0;
        // for (var g = 1; g <= gm_count; g++) {
        //     total = total + arr[g].length;
        // }
        // console.log(design_array_section[sec_curr]);

        // file.set("designData.designLines.total", total);
        // file.set("designData.designLines.lastLineNo", line_count);
        // file.set("designData.stratumData.gm" + sec_curr, design_array_section[sec_curr]);
        // file.set("designData.strataName.gm" + sec_curr, strata_name_array[sec_curr]);
        // // file.set("designData.designLines.Coordinates.line_top_of", r);
        // // file.set("designData.designLines.Coordinates.gm", section_number);
        // file.set("designData.designLines.save", 1);
        // file.set("designData.save", 1);


        // // }

        // ctx1.stroke();
        //     }
        // })
        // .catch(console.error);
    }
}
var x = document.createElement("datalist");
x.setAttribute("id", "stratum");
for (var l = 0; l < stratum_defined_list.length; l++) {
    var aa = document.createElement("option");
    aa.setAttribute("value", stratum_defined_list[l]);
    x.appendChild(aa);
}
// document.getElementById("kk").appendChild(x);

function redo() {

    ctx1.redo();
}

function undo() {

    ctx1.undo();
}

function eraseAll() {

    w = chart_two.width;
    h = chart_two.height;

    var ctx = chart_two.ctx;

    var m = confirm("Want to clear");

    if (m) {
        chart_two.render();
    }
}

function gm_divide(e) {
    section_array[sec].push(e.dataPoint.label);
}