    // General Vaiable Declaration
    const CanvasJS = require('./assets/javascript/canvasjs.min.js');
    var slide = $("#scale");
    var Swal = require("sweetalert2");
    var Swal1 = require("sweetalert2");
    var del_section_arr = [];
    var range = ['1000', '2000', '3000', '4000', '5000', '6000', '7000', '8000', '9000', '10000', '20000', '30000', '40000', '50000', '60000', '70000', '80000', '90000', '100000', '150000', '100000', '200000', '300000', '400000', '500000', '600000', '700000', '800000', '900000', '1000000'];
    // var  constt;
    var lim_x_start, lim_x_end, lim_y_end, lim_y_start, actual_x_end, actual_x_start, actual_y_end, actual_y_start;
    var ipcRenderer = require('electron').ipcRenderer;
    var editJsonFile = require("edit-json-file");
    var Color = ["#aeabab", "#ffff00", "#9cc2e5", "#c55a11"];
    var remote = require('electron').remote;
    var inter_f_x, inter_f_y;
    const app = remote.app;
    var valof_inter_x, valof_inter_y, valof_max_x, valof_max_y, valof_min_x, valof_min_y;
    // var tempFilePath = app.getPath('temp');
    var tempFilePath = remote.getGlobal('path_file');
    var myData = new Array();
    var test_data_storage = new Array();
    var val = new Array();
    var section = new Array();
    var section_list = new Array();
    var borehole_count;
    var i;
    var zoom_text = document.getElementById("zoom_text");
    var max_x, max_y, min_x, min_y;
    var click_Section_number = 0;
    var sec = 0;
    var del = false;
    var ctx1;
    var dps_x = [];
    var dps_y = [];
    var section_list = [];
    var item, prevsec = 0;
    var oldsec, newsec;
    var section_array = new Array();
    var line_array = new Array();
    var undo_array = new Array();
    var distance_section_array = new Array();
    // var line_count = 0;
    var store = new Array();
    var arr = new Array();
    var lc_arr = new Array();
    var line_count = 0;
    var curr_sec;
    myData = localStorage.getItem(myData);
    test_data_storage = localStorage.getItem(test_data_storage);
    console.log(test_data_storage);
    console.log(myData);
    let file = editJsonFile(tempFilePath);
    let win = remote.getCurrentWindow();
    const {
        dialog
    } = require("electron").remote;
    require('electron').ipcRenderer.on('save', (event, message) => {
        // fifth(message); // Prints 'whoooooooh!'
        if (message == 1) {
            fifth(1);
        } else if (message == 2) {
            // alert("Kindly Proceed in Selecting the Module you want !");
            // submit();
            saveChanges();
        } else {
            alert("Kindly Contact the support@vzoft.com");
        }

    });

    win.on('close', function (e) {

        fifth(1);
    });


    var type_of_mode = file.get("projectDetails.typeOfMode");
    const {
        Menu,
        MenuItem
    } = remote

    const menu = new Menu()

    menu.append(new MenuItem({
        label: 'Section Tools',
        submenu: [{
                'label': 'Add Section Line'
            },
            {
                'label': ' Line'
            }
        ]
    }))
    menu.append(new MenuItem({
        label: 'Add Section',
        click() {
            section_fun()
        }
    }))

    menu.append(new MenuItem({
        label: 'Delete Section',
        click() {
            del_section_var()
        }
    }))

    menu.append(new MenuItem({
        type: 'separator'
    }))
    menu.append(new MenuItem({
        label: 'Show Tour',
        click() {
            introJs().start();
        }
    }))

    // Prevent default action of right click in chromium. Replace with our menu.
    window.addEventListener('contextmenu', (e) => {
        e.preventDefault()
        menu.popup(remote.getCurrentWindow())
    }, false)
    // To check the Section Saved or Not
    if (file.get("soilData.sectionData.save") == 0) {
        var sec = 0;
    } else {
        var sec = file.get("soilData.sectionData.noOfSection");
        // alert(sec);
        $(document).ready(function () {
            // your code

            load_section();
        });

    }


    function load_line() {
        // plotly();
        if (file.get("sectionLines.save") == 0) {
            // var arr = new Array();
        } else {
            alignment = "center"
            arr = file.get("sectionLines.Coordinates");
            // lc_arr = file.get("sectionLines.Coordinates");
            store = file.get("sectionLines.Coordinates");
            line_count = file.get("sectionLines.lastLineNo");
            console.log(arr);
            console.log(store);
            chart.render();
            for (var r = 0; r < arr.length; r++) {
                if (arr[r] == null) {
                    continue;
                } else {
                    var ctx = chart.ctx;
console.log(arr[r].line_no);
                    ctx.beginPath();
                    console.log(arr[r]);

                    var p1 = [arr[r].x1, arr[r].y1];
                    var p2 = [arr[r].x2, arr[r].y2];
                    var textt = "Section " + arr[r].line_no;
                    console.log(arr[r].x1 + ","+arr[r].x2+","+arr[r].y1+","+arr[r].y2);
                    var dx = arr[r].x2 - arr[r].x1;
                    var dy = arr[r].y2 - arr[r].y1;
                    var len = Math.sqrt(dx * dx + dy * dy);
                    var avail = len - 2 * 10;
                    var angle = Math.atan2(dy, dx);
                    if (angle < -Math.PI / 2 || angle > Math.PI / 2) {
                        var p = p1;
                        p1 = p2;
                        p2 = p;
                        dx *= -1;
                        dy *= -1;
                        angle -= Math.PI;
                    }

                    var p, pad;
                    if (alignment == 'center') {
                        p = p1;
                        pad = 1 / 2;
                    } else {
                        var left = alignment == 'left';
                        p = left ? p1 : p2;
                        pad = padding / len * (left ? 1 : -1);
                    }

                    ctx.save();
                    ctx.textAlign = "center";
                    ctx.translate(arr[r].x1, arr[r].y1);
                    // alert(lineCoordinates.x1,lineCoordinates.y1);
                    ctx.rotate(Math.atan2(dy, dx));
                    ctx.fillStyle = "black"
                    // ctx.fillText(textt, 0, 0);
                    ctx.restore();


                    ctx.strokeStyle = "#000"; //Change Line Color
                    ctx.lineWidth = 2; //Change Line Width/Thickness
                    ctx.moveTo(chart.axisX2[0].convertValueToPixel(arr[r].x1), chart.axisY[0].convertValueToPixel(arr[r].y1));
                    // ctx.moveTo(store[r].x1, store[r].y1);
                    ctx.lineTo(chart.axisX2[0].convertValueToPixel(arr[r].x2), chart.axisY[0].convertValueToPixel(arr[r].y2));
                    // ctx.lineTo(store[r].x2, store[r].y2);
                    ctx.stroke();
                }

            }


        }

    }
    // document.getElementById("but_add_line").hidden = true;

    // Add Section Calls from HTML
    function section_fun() {

        Swal.mixin({

            confirmButtonText: 'Next &rarr;',
            showCancelButton: true,
            cancelButtonText: 'Thanks !',
            progressSteps: ['1', '2', '3', '4', '5']
        }).queue([{
                title: 'Click on the Section Tab ',

            },
            {
                title: 'Select the Boreholes from the Plot using Cursor',

            },
            {
                title: 'You can Delete the Boreholes in Section using Delete borhole Button',

            },
            {
                title: 'Then Click on the Section Line Button to Draw Section Line ',

            },
            {
                icon: 'success',
                title: 'Section Created Successfully ',

            }
        ]);



        // Increment of Section
        if(del_section_arr.length == 0){
            sec = sec + 1;
            var curr = sec;
            section_list[sec] = "Section " + sec;
            // Swal.mixin({
            //     toast: true,
            //     position: 'bottom-end',
            //     showConfirmButton: false,
            //     timer: 3000,
            //     timerProgressBar: true,
            //     onOpen: (toast) => {
            //       toast.addEventListener('mouseenter', Swal.stopTimer)
            //       toast.addEventListener('mouseleave', Swal.resumeTimer)
            //     }
            //   }).fire({
            //     icon: 'success',
            //     title: 'Signed in successfully'
            //   });
            // Menu Functions

            menu.append(new MenuItem({
                'label': 'Section ' + sec,
                submenu: [{
                    'label': 'Add Section Line'
                },
                {
                    'label': ' Line'
                }
            ],
                click() {
                    sec_call('' + curr + '')
                },
                'id': curr,


                type: 'checkbox',
                checked: false
            }))

            section_array[sec] = new Array();
            distance_section_array[sec] = new Array();


            sec_but.setAttribute("id", "section_but_" + sec);
            // sec_but.setAttribute("style", "");
            // Section is Called Here
            sec_but.setAttribute("onclick", "sec_call(" + sec + ")");
            // sec_but.setAttribute("data-tooltip", "Click Here to View Borehole of Section" + sec + " ");
            // sec_but.setAttribute("data-tooltip-position", "top");

            var sec_but_text = document.createTextNode("S" + sec);

            var del_sec_but = document.createElement("button");
            // del_sec_but.setAttribute('type', 'button');
            del_sec_but.setAttribute('class', 'btn  btn-outline-success');
            del_sec_but.setAttribute('onclick', 'delete_section(this,' + sec + ')');
            del_sec_but.innerHTML = '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path fill="#555" d="M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zM19 4h-3.5l-1-1h-5l-1 1H5v2h14V4z"/><path d="M0 0h24v24H0z" fill="none"/></svg>'



            div_section_tool.appendChild(sec_but);
            div_section_tool.appendChild(add_sec_line_but);
            div_section_tool.appendChild(del_sec_but);
            sec_tool.appendChild(div_section_tool);
        }else{
var min_del = Math.min(...del_section_arr);
var min_del_index = del_section_arr.indexOf(min_del);
del_section_arr.splice(min_del_index,1);
line_count = min_del;
// sec = min_del;
var curr = min_del;
            section_list[min_del] = "Section " + min_del;
            // Swal.mixin({
            //     toast: true,
            //     position: 'bottom-end',
            //     showConfirmButton: false,
            //     timer: 3000,
            //     timerProgressBar: true,
            //     onOpen: (toast) => {
            //       toast.addEventListener('mouseenter', Swal.stopTimer)
            //       toast.addEventListener('mouseleave', Swal.resumeTimer)
            //     }
            //   }).fire({
            //     icon: 'success',
            //     title: 'Signed in successfully'
            //   });
            // Menu Functions

            menu.append(new MenuItem({
                'label': 'Section ' + min_del,
                submenu: [{
                    'label': 'Add Section Line'
                },
                {
                    'label': ' Line'
                }
            ],
                click() {
                    sec_call('' + curr + '')
                },
                'id': curr,


                type: 'checkbox',
                checked: false
            }))

            section_array[min_del] = new Array();
            distance_section_array[min_del] = new Array();

            var sec_tool = document.getElementById("section_toolbar");

            var div_section_tool = document.createElement("div");
            div_section_tool.setAttribute("class", "btn-group ");
            div_section_tool.setAttribute("style", "margin-left:10px;margin-top:5px;");
            div_section_tool.setAttribute("role", "group");
            div_section_tool.setAttribute("id", "sec_div_" + min_del);
            var sec_but = document.createElement("Button");
            // sec_but.setAttribute("type", "button");
            sec_but.setAttribute("class", "btn btn-outline-success");
            sec_but.setAttribute("id", "section_but_" + min_del);
            // sec_but.setAttribute("style", "");
            // Section is Called Here
            sec_but.setAttribute("onclick", "sec_call(" + min_del + ")");
            // sec_but.setAttribute("data-tooltip", "Click Here to View Borehole of Section" + sec + " ");
            // sec_but.setAttribute("data-tooltip-position", "top");

            var sec_but_text = document.createTextNode("S" + min_del);

            var del_sec_but = document.createElement("button");
            // del_sec_but.setAttribute('type', 'button');
            del_sec_but.setAttribute('class', 'btn  btn-outline-success');
            del_sec_but.setAttribute('onclick', 'delete_section(this,' + min_del + ')');
            del_sec_but.innerHTML = '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path fill="#555" d="M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zM19 4h-3.5l-1-1h-5l-1 1H5v2h14V4z"/><path d="M0 0h24v24H0z" fill="none"/></svg>'

            var add_sec_line_but = document.createElement("img");
            // add_sec_line_but.setAttribute('type', 'button');
            add_sec_line_but.setAttribute('class', 'btn btn-outline-success ');
            add_sec_line_but.setAttribute('id', 'line_'+min_del);
            add_sec_line_but.setAttribute('style', 'width:50px;height :50px;');
            add_sec_line_but.setAttribute('src', './assets/img/add_line.svg');
            add_sec_line_but.hidden = true;
            add_sec_line_but.setAttribute('onclick', 'add_sec_line(this,' + min_del + ')');
            // del_sec_but.innerHTML = '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path fill="#555" d="M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zM19 4h-3.5l-1-1h-5l-1 1H5v2h14V4z"/><path d="M0 0h24v24H0z" fill="none"/></svg>'

            sec_but.appendChild(sec_but_text);


            div_section_tool.appendChild(sec_but);
            div_section_tool.appendChild(add_sec_line_but);
            div_section_tool.appendChild(del_sec_but);
            sec_tool.appendChild(div_section_tool);
        }

        // Assigining Current Section


    }
    // Load Section for Saved File
    function section_fun_load(sec_key_load, section_Arr_load) {

        // sec = sec + 1;

        section_array[sec_key_load] = section_Arr_load;
        // section_array[sec_key] = new Array();
        console.log(section_array[sec_key_load]);
        var sec_tool = document.getElementById("section_toolbar");


        // var sec_but = document.createElement("Button");
        // sec_but.setAttribute("class", "btn  btn-outline-success");
        // sec_but.setAttribute("style", "margin:10px");
        // sec_but.setAttribute("onclick", "sec_call_load(" + sec_key_load + "," + sec_key_load + ")");
        // var sec_but_text = document.createTextNode("Section " + sec_key_load);
        // sec_but.appendChild(sec_but_text);
        // sec_tool.appendChild(sec_but);
        // var div_but = document.getElementById("section_toolbar");
        // var buts = div_but.getElementsByClassName("btn btn-outline-success");
        // // console.log(el);
        // for (var i = 0; i < buts.length; i++) {
        //     console.log(buts[i]);
        //     buts[i].addEventListener("click", function () {
        //         var current = document.getElementsByClassName("active");
        //         current[0].className = current[0].className.replace(" active", "");
        //         this.className += " active";
        //     });
        // }


        var div_section_tool = document.createElement("div");
        div_section_tool.setAttribute("class", "btn-group ");
        div_section_tool.setAttribute("style", "margin-left:10px;margin-top:5px;");
        div_section_tool.setAttribute("role", "group");
        div_section_tool.setAttribute("id", "sec_div_" + sec_key_load);

        var sec_but = document.createElement("Button");
        // sec_but.setAttribute("type", "button");
        sec_but.setAttribute("class", "btn btn-outline-success");
        sec_but.setAttribute("id", "section_but_" + sec_key_load);
        // sec_but.setAttribute("style", "");
        // Section is Called Here
        sec_but.setAttribute("onclick", "sec_call_load(" + sec_key_load + "," + sec_key_load + ")");
        // sec_but.setAttribute("data-tooltip", "Click Here to View Borehole of Section" + sec + " ");
        // sec_but.setAttribute("data-tooltip-position", "top");

        var sec_but_text = document.createTextNode("S" + sec_key_load);

        var del_sec_but = document.createElement("button");
        // del_sec_but.setAttribute('type', 'button');
        del_sec_but.setAttribute('class', 'btn  btn-outline-success');
        del_sec_but.setAttribute('onclick', 'delete_section(this,' +sec_key_load     + ')');
        del_sec_but.innerHTML = '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path fill="#555" d="M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zM19 4h-3.5l-1-1h-5l-1 1H5v2h14V4z"/><path d="M0 0h24v24H0z" fill="none"/></svg>'

        var add_sec_line_but = document.createElement("img");
        // add_sec_line_but.setAttribute('type', 'button');
        add_sec_line_but.setAttribute('class', 'btn btn-outline-success ');
        add_sec_line_but.setAttribute('id', 'line_'+sec_key_load);
        add_sec_line_but.setAttribute('style', 'width:50px;height :50px;');
        add_sec_line_but.setAttribute('src', './assets/img/add_line.svg');
        add_sec_line_but.hidden = true;
        add_sec_line_but.setAttribute('onclick', 'add_sec_line(this,' + sec_key_load + ')');
        // del_sec_but.innerHTML = '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path fill="#555" d="M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zM19 4h-3.5l-1-1h-5l-1 1H5v2h14V4z"/><path d="M0 0h24v24H0z" fill="none"/></svg>'

        sec_but.appendChild(sec_but_text);


        div_section_tool.appendChild(sec_but);
        div_section_tool.appendChild(add_sec_line_but);
        div_section_tool.appendChild(del_sec_but);
        sec_tool.appendChild(div_section_tool);

    }
    // Section Call for Borehole Included
    function delete_section(element, sec_key) {
        Swal.fire({
            title: 'Are you sure to delete <br> Section ' + sec_key + ' ?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,

            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                var bt = document.getElementById("sec_div_" + sec_key);
                // bt.hidden = true;
                bt.parentNode.removeChild(bt);
                // arr.splice(sec_key-1,1);


                var sec_det = document.getElementById("section_details");
                sec_det.innerHTML = "Section " + sec_key + ": Deleted";
                // section_array[sec_key] = null;
                // section_array.splice(sec_key, 1);
                del_section_arr.push(sec_key);
                if (file.get("sectionLines.save") == 0) {
                    // store[sec_key-1
                    console.log(file.get("sectionLines.save"));
                    section_array[sec_key] = null;

                    file.set("soilData.sectionData.section_" + sec_key, null);
                    file.set("soilData.distanceData.section_" + sec_key, null);
                    file.set("sectionLines.data.section_" + sec_key, null);
                    for (var k = 0; k < store.length; k++) {
                        if (store[k].section == sec_key) {
                            store[k] = null;
                        }
                    }
                    for (var k = 0; k < arr.length; k++) {
                        if (arr[k].section == sec_key) {
                            arr[k] = null;
                        }
                    }
                    chart.render();
                    refresh();
                } else {
                    console.log(file.get("sectionLines.save"));
                    file.set("soilData.sectionData.section_" + sec_key, null);
                    file.set("soilData.distanceData.section_" + sec_key, null);
                    file.set("sectionLines.data.section_" + sec_key, null);
                    console.log(store);
                    for (var k = 0; k < store.length; k++) {
                        if (store[k].section == sec_key) {
                            store[k] = null;
                        }
                    }
                    for (var k = 0; k < arr.length; k++) {
                        if (arr[k].section == sec_key) {
                            arr[k] = null;
                        }
                    }
                    chart.render();
                    // load_line();
                }
                Swal.fire(
                    'Deleted!',
                    'Your file has been deleted.',
                    'success'
                )

                // window.reload();
            }
        })
    }

    function sec_call(sec_key) {
        var but = document.getElementById("line_"+sec_key);
        but.hidden = false;
        var sec_det = document.getElementById("section_details");
        sec_det.innerHTML = "Section " + sec_key + ": " + section_array[sec_key] + "";
        curr_sec = sec_key;
        menu.getMenuItemById(sec).checked = true;

    }
    // Section Call for Saved file for Borehole Included
    function sec_call_load(section_Arr_load, sec_key_id) {
        // alert("Yess");
        var but = document.getElementById("line_"+sec_key_id);
        but.hidden = false;
        var sec_det = document.getElementById("section_details");
        console.log(section_Arr_load);
        // section_array[sec_key] = new Array();
        console.log(section_array[sec_key_id]);
        sec_det.innerHTML = "Section " + sec_key_id + ": " + section_array[sec_key_id] + "";
        curr_sec = sec_key_id;
        // alert("Selected :" + curr_sec);
    }

    function get_sec() {


    }

    // Erase Selected
    function eraseSel() {

        var disp = document.getElementById("displayData");

        var tn = document.createTextNode("Enter the line number that you want to erase");

        var ip = document.createElement("input");
        ip.setAttribute("id", "ln");
        ip.setAttribute("type", "number");
        ip.setAttribute("value", "");

        disp.appendChild(tn);
        disp.appendChild(ip);

        var button = document.createElement('button');
        button.setAttribute('type', 'button');
        button.setAttribute('id', 'btn');
        button.innerHTML = "remove";
        button.setAttribute('onclick', 'remove()');

        disp.appendChild(button);
    }
    // Load Sections
    function load_section() {
        for (var i = 1; i <= sec; i++) {
            if(file.get("soilData.sectionData.section_" + i) == null){
                continue;
            }else{
                section_fun_load(i, file.get("soilData.sectionData.section_" + i));
            }

            // section_array[i] = new Array();
        }

    }
    // To Delete Lines Not Needed
    function remove() {

        var l_num = document.getElementById("ln").value;
        console.log(l_num);
        // alert(l_num);
        // var removed = arr.splice(l_num - 1, l_num - 1);
        var coords = file.get("sectionLines.Coordinates");
        // var removed = arr.splice(l_num - 1, l_num - 1);
        for (var i = 0; i < coords.length; i++) {
            if (coords[i].line_no == l_num) {
                arr.splice(i, 1);
            }
        }

        console.log(arr);
        var remove_val = file.get("sectionLines.total");




        getLines();
    }
    // Get Lines to Verify New file or Saved File
    function getLines() {
        chart.render();
        if (file.get("sectionLines.save") == 0) {
            // var arr = new Array();
        } else {
            // var arr = new Array();
            arr = file.get("sectionLines.Coordinates");
            store = file.get("sectionLines.Coordinates");
            // store = file
            console.log(arr);
            drawdefinedLine(chart, file.get("sectionLines.total"));
        }

    }
    // Old Code
    var save_bh_count = file.get("soilData.noOfBh");

    if (myData) {
        val = JSON.parse(myData);
    }
    i = val[0];


    function myFunction() {
        click_Section_number++;

        var myDiv = document.getElementById("demo");


        var array = new Array();
        for (var j = 1; j <= save_bh_count; j++) {
            array.push("Borehole " + j);

        }

        var selection_id = "select_" + click_Section_number + "";
        var selectList = document.createElement("select");
        selectList.setAttribute("id", selection_id);
        selectList.setAttribute("multiple", "multiple");
        selectList.setAttribute("name", "selectlist");
        selectList.setAttribute("class", "col-sm-2 form-control");

        myDiv.appendChild(selectList);
        var btn = document.createElement("BUTTON");
        btn.setAttribute("onclick", "printAll('" + selection_id + "')"); // Create a <button> element
        btn.setAttribute("class", "btn btn-primary"); // Create a <button> element
        var t = document.createTextNode("Group this for Section " + click_Section_number + ""); // Create a text node
        btn.appendChild(t); // Append the text to <button>
        myDiv.appendChild(btn);
        //Create and append the options
        for (var m = 0; m < save_bh_count; m++) {
            var option = document.createElement("option");

            option.value = array[m];
            option.text = array[m];
            selectList.appendChild(option);
        }
    }

    // NNext Screen Transistion
    function fifth(dd) {
        console.log(dd);
        // if (0 == 0 ) {
        //     if (dd == 1) {

        //     } else {
        //         // alert("Kindly Create a Section Before Proceeding ");
        //     }
        // } else {
            // alert("okay");
            console.log(sec);
            for (var l = 1; l <= sec; l++) {
                // alert(section_array[l]);
                file.set("soilData.sectionData.section_" + l + "", section_array[l]);
                // file.set("soilData.distanceData.section_" + l + "", distance_section_array);
                // alert("Options selected are " + str);
                file.save();
            }
            file.set("soilData.sectionData.noOfSection", section_array.length - 1);
            file.set("sectionLines.Coordinates", arr);
            file.set("soilData.sectionData.save", 1);
            file.set("sectionLines.save", 1);
            file.save();
            if (dd == 1) {

            } else {
                location.href = "fifth.html";
            }

        // }

        //
    }

    // old Code
    function printAll(num_selection) {
        // alert(click_Section_number);
        var str = "";
        var bh_section_arr = new Array();
        var i;
        var selection_section = [];
        var select = document.getElementById(num_selection);
        // var arr_+click_Section_number = new Array();
        for (i = 0; i < select.options.length; i++) {
            if (select.options[i].selected) {
                str = str + i + " ";
                selection_section.push(i + 1);
                var s = i + 1;
                bh_section_arr.push("bh_" + s);
            }
        }
        console.log(selection_section);
        console.log(bh_section_arr);



        localStorage.setItem("" + num_selection + "", JSON.stringify(selection_section));

    }
    // Chart Code Comes Here
    var chart;
    var points = [];
    // window.onload =
    window.onload = function () {
        // dataPoints
        var s_one = []; // dataPoints
        var s_two = []; // dataPoints
        var s_three = []; // dataPoints
        // Borehole Scatter Plot
        chart = new CanvasJS.Chart("soilplot", {
            zoomEnabled: true,
            zoomType: "xy",

            title: {

                fontFamily: "Roboto",
                fontWeight: 'bold',
                padding: 10,
                horizontalAlign: "center"
            },
            toolTip: {
                content: "{name_to_call}<br> Eatings : {x} <br> Northings : {y}"
            },
            axisX: {
                title: "Geographic Plot Graph ",
                titleFontSize: 12
            },
            axisX2: {


                title: "Eastings",
                titleFontSize: 12,
                labelFontSize: 8,
                titleFontStyle: "oblique",
                titleFontColor: "black",
                labelFontColor: "black",
                minimum: 0,
                maximum: 3000000,
                intervalType: "number",
                valueFormatString: "#,##0.##",
                gridDashType: "dot",
                gridThickness: 0.1,
                crosshair: {
                    enabled: true //disable here
                }
                // includeZero: true
            },
            axisY: {
                title: "Northings",
                titleFontSize: 12,
                titleFontColor: "black",
                labelFontColor: "black",
                titleFontStyle: "oblique",
                labelFontSize: 8,
                minimum: 0,
                maximum: 3000000,

                intervalType: "number",

                valueFormatString: "#,##0.##",
                gridDashType: "dot",
                gridThickness: 0.1,
                crosshair: {
                    enabled: true //disable here
                }


            },
            legend: {

                horizontalAlign: "center",
                fontSize: 15,

            },
            rangeChanged: function (e) {
                if (e.trigger === "reset") {
                    changeToPanMode();
                }
            },
            // rangeChanging: selectedDataPoints,
            data: [{
                cursor: "crosshair",
                type: "scatter",
                color: "#778899",
                click: section_divide,
                axisXType: "secondary",
                legendText: "Each Cross represents a borehole",


                showInLegend: "true",
                markerType: "circle",
                markerColor: "#2098c5",
                dataPoints: data_get(100)

            }]
        });

        var y_data = [];
        var save_data = new Array();
        var grownd_water = new Array();
        // Borehole Stratum Wise Plot
        var chart_two = new CanvasJS.Chart("chartContainer", {
            animationEnabled: true,
            title: {
                // text: "BoreHole Soil Cross Section",
                fontFamily: "Roboto",
                fontWeight: 'bold',
                padding: 10,
                horizontalAlign: "center"
            },
            axisX2: {
                title: "Chainage ( m )",
                titleFontSize: 12,
                labelFontSize: 8,
                titleFontStyle: "oblique",
                titleFontColor: "black",
                labelFontColor: "black",
                // minimum : 0,
                //   maximum: 30000000,
                // title: "Eastings",
                // interval: 1500000,
                intervalType: "number",
                valueFormatString: "#,##0.##",
                gridDashType: "dot",
                gridThickness: 0.1,
                crosshair: {
                    enabled: true //disable here
                }
            },
            axisY: y_data,
            toolTip: {
                content: "{name} in {label}<br> (Start,End) : {y}"
            },
            legend: {
                cursor: "crosshair"
            },
            data: save_data


        });
        if (type_of_mode == "mbgl") {
            y_data.push({
                'title': "Depth ( m bgl )",
                'titleFontSize': 12,
                'labelFontSize': 8,
                'titleFontStyle': "oblique",
                'titleFontColor': "black",
                'labelFontColor': "black",
                'includeZero': false,
                // minimum : 0,
                //   maximum: 30000000,
                // title: "Eastings",
                // interval: 1500000,
                'intervalType': "number",
                'valueFormatString': "#,##0.##",
                'gridDashType': "dot",
                'gridThickness': 0.1,
                'crosshair': {
                    enabled: true //disable here
                },
                'reversed': true
            })
        } else {
            y_data.push({
                'title': "Elevation ( mAOD )",
                'titleFontSize': 12,
                'labelFontSize': 8,
                'titleFontStyle': "oblique",
                'titleFontColor': "black",
                'labelFontColor': "black",
                'includeZero': false,
                // minimum : 0,
                //   maximum: 30000000,
                // title: "Eastings",
                // interval: 1500000,
                'intervalType': "number",
                'valueFormatString': "#,##0.##",
                'gridDashType': "dot",
                'gridThickness': 0.1,
                'crosshair': {
                    enabled: true //disable here
                },
                'reversed': false
            })
        }
        var strata_name_dps = new Array();
        var gw_name_dps = new Array();
        save_data.push({
            'type': 'rangeColumn',
            'axisXType': "secondary",
            'showInLegend': false,
            'LegendText': 'Stratum',
            'dataPoints': strata_name_dps
        });
        save_data.push({
            'type': 'scatter',
            'lineDashType': 'dash',
            'lineColor': 'blue',
            'markerType': 'none',
            'markerColor': '#0000ff',
            // 'animationEnabled': true,
            'axisXType': "secondary",
            'showInLegend': false,
            'LegendText': 'Ground Water Level',
            'dataPoints': gw_name_dps
        });
        // grownd_water.push({
        //   'type': 'rangeColumn',
        //   'axisXType': "secondary",
        //   'showInLegend': true,
        //   'LegendText': 'Stratum',
        //   'dataPoints': strata_name_dps
        // });

        // Loop to Push in the values

        for (var g = 1; g <= save_bh_count; g++) {
            var jj = g - 1;
            var bh = file.get("soilData.boreholeArray." + jj);
            for (var f = 3; f < file.get("soilData.params." + bh + ".length"); f++) {

                var strata_name = file.get("soilData.params." + bh + "." + f);

                var val1 = file.get("soilData." + bh + ".stratumData." + strata_name + ".start");
                var val2 = file.get("soilData." + bh + ".stratumData." + strata_name + ".end");

                strata_name_dps.push({

                    label: String(bh),

                    name_to_call: String(bh),

                    name: strata_name,
                    x: parseInt(file.get("soilData." + bh + ".stratumData.Eastings"), 10),
                    y: [parseInt(val1, 10), parseInt(val2, 10)],
                    color: file.get("soilData." + bh + ".stratumData." + strata_name + ".strataColor")
                });
            }
        }
        for (var g = 1; g <= save_bh_count; g++) {
            var jj = g - 1;
            var bh = file.get("soilData.boreholeArray." + jj);
            // for (var f = 3; f < file.get("soilData.noOfParams"); f++) {
            var pos_x = parseInt(file.get("soilData." + bh + ".stratumData.Eastings"), 10);
            var val_x = pos_x + 0;

            gw_name_dps.push({

                // label: "GroundWater",
                // name: strata_name,
                x: val_x,
                y: parseInt(file.get("soilData." + bh + ".stratumData.Groundwater"), 10),
                markerImageUrl: "./assets/img/1.png"
                // color: Color[f - 3]
            });
            //  }
        }

        // UndoCanvas.enableUndo(context)

        chart.render();
        // var vMinx   = document.getElementById("min_input_x");
        // var vMaxx   = document.getElementById("max_input_x");
        // var vInterx = document.getElementById("intr_input_x");
        // var vMiny   = document.getElementById("min_input_y");
        // var vMaxy   = document.getElementById("max_input_y");
        // var vIntery = document.getElementById("intr_input_y");

        // vMinx.addEventListener( "change",  setViewportXMin);
        // vMaxx.addEventListener( "change",  setViewportXMax);
        // vInterx.addEventListener( "change",  setViewportXIntr);
        // vMiny.addEventListener( "change",  setViewportYMin);
        // vMaxy.addEventListener( "change",  setViewportYMax);
        // vIntery.addEventListener( "change",  setViewportYIntr);
        // console.log(vInterx.value);
        function setViewportXMin() {
            if (vMinx.value)
                chart.axisX2[0].set("viewportMinimum", vMinx.value); //Subtracting 5 so as to give some padding between axis and dataPoint
            else
                console.log("null");
            chart_call();
        }

        function setViewportXMax() {
            if (vMaxx.value)
                chart.axisX2[0].set("viewportMaximum", vMaxx.value); //Subtracting 5 so as to give some padding between axis and dataPoint
            else
                console.log("null");
            chart_call();
        }

        function setViewportXIntr() {
            if (vInterx.value) {
                chart.axisX2[0].set("interval", vInterx.value); //Subtracting 5 so as to give some padding between axis and dataPoint
                chart.axisY[0].set("interval", vInterx.value); //Subtracting 5 so as to give some padding between axis and dataPoint
            } else
                console.log("null");
            chart_call();
        }

        function setViewportYMax() {
            if (vMaxy.value)
                chart.axisY[0].set("viewportMaximum", vMaxy.value); //Subtracting 5 so as to give some padding between axis and dataPoint
            else
                console.log("null");
            chart_call();
        }

        function setViewportYMin() {
            if (vMiny.value) {
                chart.axisY[0].set("viewportMinimum", vMiny.value);
                console.log(vMiny.value); //Subtracting 5 so as to give some padding between axis and dataPoint
            } else
                console.log("null");
            chart_call();
        }

        function setViewportYIntr() {
            if (vIntery.value)
                chart.axisY[0].set("interval", vIntery.value); //Subtracting 5 so as to give some padding between axis and dataPoint
            else
                console.log("null");
            chart_call();
        }

        changeToPanMode();
        var zoom_factor = 1;
        document.getElementsByClassName("canvasjs-chart-canvas")[1].addEventListener("wheel", function (e) {
            e.preventDefault();
            var zoomLevel = parseInt(slide.val());
            console.log(e);
            if (e.wheelDelta < 0) {
                console.log("zoom in");
                //scroll down
                zoomLevel--;
                //   zoomChart(zoomLevel);
            } else {
                console.log("zoom up");
                //scroll up
                zoomLevel++;
                // zoomChart(zoomLevel);
            }



            console.log(zoomLevel);


            // if (e.pageX < chart.plotArea.x1 || e.pageX > chart.plotArea.x2 || e.pageY < chart.plotArea.y1 || e.pageY > chart.plotArea.y2)
            //     return;
            // var maxyzoom = chart.axisY[0].get("maximum");
            // var maxxzoom = chart.axisX2[0].get("maximum");
            // var minxzoom = chart.axisX2[0].get("minimum");
            // var minyzoom = chart.axisY[0].get("minimum");
            // var interx = chart.axisX2[0].get("interval");
            // var intery = chart.axisY[0].get("interval");
            // console.log(e.deltaY);
            // if (e.deltaY == -1250) {
            //     if (zoom_factor == 5) {
            //         console.log("delta is positive zoom  equals to 5");
            //         chart.axisY[0].set("maximum", Math.round(valof_max_y / Math.abs(5)));
            //         chart.axisY[0].set("interval", Math.round(valof_inter_y / Math.abs(5)));
            //         chart.axisY[0].set("minimum", Math.round(valof_min_y / Math.abs(5)));
            //         chart.axisX2[0].set("maximum", Math.round(valof_max_x / Math.abs(5)));
            //         chart.axisX2[0].set("interval", Math.round(valof_inter_x / Math.abs(5)));
            //         chart.axisX2[0].set("minimum", Math.round(valof_min_x / Math.abs(5)));
            //     } else {
            //         // chart.axisY[0].set("maximum",Math.round(maxyzoom / Math.abs(zoom_factor)));
            //         // chart.axisY[0].set("interval", Math.round(valof_inter_y / Math.abs(zoom_factor)));
            //         // chart.axisY[0].set("minimum", Math.round(minyzoom / Math.abs(zoom_factor)));
            //         // chart.axisX2[0].set("maximum", Math.round(maxxzoom / Math.abs(zoom_factor)));
            //         // chart.axisX2[0].set("interval", Math.round(valof_inter_x / Math.abs(zoom_factor)));
            //         // chart.axisX2[0].set("minimum", Math.round(minxzoom / Math.abs(zoom_factor)));
            //         console.log("delta is positive zoom not equals to 5");
            //         chart.axisY[0].set("maximum", Math.round(valof_max_y / Math.abs(zoom_factor)));
            //         chart.axisY[0].set("interval", Math.round(valof_inter_y / Math.abs(zoom_factor)));
            //         chart.axisY[0].set("minimum", Math.round(valof_min_y / Math.abs(zoom_factor)));
            //         chart.axisX2[0].set("maximum", Math.round(valof_max_x / Math.abs(zoom_factor)));
            //         chart.axisX2[0].set("interval", Math.round(valof_inter_x / Math.abs(zoom_factor)));
            //         chart.axisX2[0].set("minimum", Math.round(valof_min_x / Math.abs(zoom_factor)));
            //     }

            //     // console.log(zoom_factor);
            // } else if (e.deltaY == 1250) {
            //     if (zoom_factor == -5) {
            //         console.log("delta is negative zoom  equals to 5");
            //         chart.axisY[0].set("maximum", Math.round(valof_max_y / Math.abs(5)));
            //         chart.axisY[0].set("interval", Math.round(valof_inter_y / Math.abs(5)));
            //         chart.axisY[0].set("minimum", Math.round(valof_min_y / Math.abs(5)));
            //         chart.axisX2[0].set("maximum", Math.round(valof_max_x / Math.abs(5)));
            //         chart.axisX2[0].set("interval", Math.round(valof_inter_x / Math.abs(5)));
            //         chart.axisX2[0].set("minimum", Math.round(valof_min_x / Math.abs(5)));
            //     } else {
            //         console.log("delta is negative zoom not equals to 5");
            //         zoom_factor--;
            //         chart.axisY[0].set("maximum", Math.round(valof_max_y / Math.abs(zoom_factor)));
            //         chart.axisY[0].set("interval", Math.round(valof_inter_y / Math.abs(zoom_factor)));
            //         chart.axisY[0].set("minimum", Math.round(valof_min_y / Math.abs(zoom_factor)));
            //         chart.axisX2[0].set("maximum", Math.round(valof_max_x / Math.abs(zoom_factor)));
            //         chart.axisX2[0].set("interval", Math.round(valof_inter_x / Math.abs(zoom_factor)));
            //         chart.axisX2[0].set("minimum", Math.round(valof_min_x / Math.abs(zoom_factor)));
            //     }
            //     // zoom_factor--;
            //     // console.log(Math.abs(zoom_factor));
            // } else {
            //     // console.log(Math.abs(zoom_factor));
            //     console.log("what to do ");
            // }
            // var axis_change = chart.axisX2[0];
            // var viewportMin = axis_change.get("viewportMinimum"),
            //     viewportMax = axis_change.get("viewportMaximum"),
            //     interval = axis_change.get("minimum");

            // var newViewportMin, newViewportMax;

            // if (e.deltaY < 0) {
            //     newViewportMin = viewportMin + interval;
            //     newViewportMax = viewportMax - interval;
            // } else if (e.deltaY > 0) {
            //     newViewportMin = viewportMin - interval;
            //     newViewportMax = viewportMax + interval;
            // }

            // if (newViewportMin >= chart.axisX2[0].get("minimum") && newViewportMax <= chart.axisX2[0].get("maximum") && (newViewportMax - newViewportMin) > (2 * interval)) {
            //     chart.axisX2[0].set("viewportMinimum", newViewportMin, false);
            //     chart.axisX2[0].set("viewportMaximum", newViewportMax);
            // }
            // console.log("Mouse Wheel Movements : "+e.deltaY);
            // var change;
            // if (e.deltaY > 0) {
            //     // Zoom Out
            //     change = Math.round(Math.abs(e.deltaY) / 10);
            // } else {
            //     //Zoom In
            //     change = Math.round(Math.abs(e.deltaY) / 10);
            // }
            // if (min_x > min_y) {
            //     inter_f_x = Math.round(min_x / change);
            //     inter_f_y = Math.round(min_y / change);

            // } else {
            //     inter_f_x = Math.round(min_x / change);
            //     inter_f_y = Math.round(min_y / change);
            // }
            // console.log(inter_f_x);
            // console.log(inter_f_y);
            // console.log((max_x + inter_f_x) - (min_x - inter_f_x));
            // var diff_x = (max_x + inter_f_x) - (min_x - inter_f_x);
            // console.log((max_y + inter_f_y) - (min_y - inter_f_y));
            // var diff_y = (max_y + inter_f_y) - (min_y - inter_f_y);

            // target.on("mousewheel DOMMouseScroll",console.log("Sss"));
            // console.log(change);
            // console.log(e.deltaY);
            // console.log();

            // console.log(maxyzoom);
            // var int_y_val = (max_y + min_y)/2;
            // var int_x_val = (max_x + min_x)/2;
            //             chart.axisY[0].set("maximum", max_y / zoom_factor);
            //             chart.axisY[0].set("interval", int_y_val / zoom_factor * 10);
            //             chart.axisY[0].set("minimum", min_y / zoom_factor);
            //             chart.axisX2[0].set("maximum", max_x / zoom_factor);
            //             chart.axisX2[0].set("interval", int_x_val / zoom_factor * 30);
            //             chart.axisX2[0].set("minimum", min_x / zoom_factor);


            // console.log( valof_inter_x / Math.abs(zoom_factor));
            // console.log( valof_inter_y / Math.abs(zoom_factor));
            // chart.axisY[0].set("maximum", max_y / zoom_factor);
            // chart.axisY[0].set("interval", minyzoom / zoom_factor);
            // chart.axisY[0].set("minimum", min_y / zoom_factor);
            // chart.axisX2[0].set("maximum", max_x / zoom_factor);
            // chart.axisX2[0].set("interval", minxzoom / zoom_factor);
            // chart.axisX2[0].set("minimum", min_x / zoom_factor);

            // chart.axisY[0].set("maximum", max_y / zoom_factor);
            // chart.axisY[0].set("interval", minyzoom / zoom_factor);
            // chart.axisY[0].set("minimum", min_y / zoom_factor);
            // chart.axisX2[0].set("maximum", max_x / zoom_factor);
            // chart.axisX2[0].set("interval", minxzoom / zoom_factor);
            // chart.axisX2[0].set("minimum", min_x / zoom_factor);

            // chart.axisY[0].set("maximum", max_y);
            // chart.axisY[0].set("interval", intery / zoom_factor);
            // chart.axisY[0].set("minimum", min_y);
            // chart.axisX2[0].set("maximum",max_x);
            // chart.axisX2[0].set("interval", interx / zoom_factor);
            // chart.axisX2[0].set("minimum", min_x);
            //
        });

        function changeToPanMode() {
            // console.log("dasdsadasd");
            var parentElement = document.getElementsByClassName("canvasjs-chart-toolbar");
            var childElement = document.getElementsByTagName("button");
            if (childElement[0].getAttribute("state") === "pan") {
                childElement[0].click();
            }
        }

        // document.getElementById("set_Scale").addEventListener("click", function () {

        // zoomChart(5);
        // }, false);
        // Lines Drwan are Noted for Saved File
        if (file.get("sectionLines.save") == 0) {
            // var arr = new Array();
        } else {

            arr = file.get("sectionLines.Coordinates");
            store = file.get("sectionLines.Coordinates");
            line_count = file.get("sectionLines.lastLineNo");
            console.log(arr);
            drawdefinedLine(chart, file.get("sectionLines.total"));
        }


        chart_two.render();

        var customMarkers = [];
        var customMarkerstwo = [];
        ctx1 = chart.ctx;
        UndoCanvas.enableUndo(ctx1);
        // Cutom Markers for the Borehole Scatter Plot

        addMarkerImages(chart_two);
        // addMarkerImagestwo(chart);
        function selectedDataPoints(e) {
            // alert("ds");
            var data = e.chart.options.data;
            var axisX = e.axisX2[0],
                axisY = e.axisY[0];;
            var dataPoints = [];

            if (!e.chart.options.axisX)
                e.chart.options.axisX = {};

            if (!e.chart.options.axisY)
                e.chart.options.axisY = {};

            for (var i = 0; i < data.length; i++) {
                for (var j = 0; j < data[i].dataPoints.length; j++) {
                    if (data[i].dataPoints[j].x > axisX.viewportMinimum && data[i].dataPoints[j].x < axisX.viewportMaximum && data[i].dataPoints[j].y > axisY.viewportMinimum && data[i].dataPoints[j].y < axisY.viewportMaximum) {
                        section_array[curr_sec].push(data[i].dataPoints[j].name_to_call);

                        if (del == true) {
                            for (var i = 0; i < section_array[curr_sec].length; i++) {
                                if (section_array[curr_sec][i] === data[i].dataPoints[j].name_to_call) {
                                    section_array[curr_sec].splice(i, 1);
                                }
                            }

                            function getUnique(arr) {

                                const final = [];

                                arr.map((e, i) => !final.includes(e) && final.push(e))

                                return final
                            }
                            section_array[curr_sec] = getUnique(section_array[curr_sec]);
                            del = false;
                        } else {
                            section_array[curr_sec].push(data[i].dataPoints[j].name_to_call);

                            function getUnique(arr) {

                                const final = [];

                                arr.map((e, i) => !final.includes(e) && final.push(e))

                                return final
                            }
                            section_array[curr_sec] = getUnique(section_array[curr_sec]);
                        }




                        // alert(curr_sec);
                    }
                }
            }

            e.chart.options.axisX.viewportMinimum = e.chart.options.axisX.viewportMaximum = null;
            e.chart.options.axisY.viewportMinimum = e.chart.options.axisY.viewportMaximum = null;
            // section_array[sec].push(dataPoints)
            // console.log(dataPoints);
        }



        function addMarkerImages(chart_two) {
            for (var i = 0; i < chart_two.data[1].dataPoints.length; i++) {

                customMarkers.push($("<img>").attr("src", chart_two.data[1].dataPoints[i].markerImageUrl)
                    .css("display", "none")
                    .css("height", 15)
                    .css("width", 15)
                    .appendTo($("#chartContainer>.canvasjs-chart-container"))
                );
                positionMarkerImage(customMarkers[i], i);
            }
        }

        function addMarkerImagestwo(chart) {
            for (var i = 0; i < chart.data[0].dataPoints.length; i++) {

                customMarkerstwo.push($("<img>").attr("src", chart.data[0].dataPoints[i].markerImageUrl)
                    .css("display", "absolute")
                    .css("height", 15)
                    .css("width", 15)
                    // .css("z-index", )

                    .appendTo($("#soilplot>.canvasjs-chart-container"))
                );
                // positionMarkerImagetwo(customMarkerstwo[i], i);
            }
        }

        function positionMarkerImage(customMarker, index) {
            var pixelX = chart_two.axisX2[0].convertValueToPixel(chart_two.options.data[1].dataPoints[index].x);
            var pixelY = chart_two.axisY[0].convertValueToPixel(chart_two.options.data[1].dataPoints[index].y);

            customMarker.css({
                "position": "absolute",
                "display": "block",
                "top": pixelY - customMarker.height() / 2,
                "left": pixelX - customMarker.width() / 2
            });
        }

        function positionMarkerImagetwo(customMarker, index) {
            var pixelX = chart.axisX2[0].convertValueToPixel(chart.options.data[0].dataPoints[index].x);
            var pixelY = chart.axisY[0].convertValueToPixel(chart.options.data[0].dataPoints[index].y);

            customMarker.css({
                "position": "absolute",
                "display": "block",
                "top": pixelY - customMarker.height() / 2,
                "left": pixelX - customMarker.width() / 2
            });
        }

        $(window).resize(function () {
            for (var i = 0; i < chart_two.data[1].dataPoints.length; i++) {
                positionMarkerImage(customMarkers[i], i);
            }
        });
        $(window).resize(function () {
            for (var i = 0; i < chart.data[0].dataPoints.length; i++) {
                // positionMarkerImagetwo(customMarkerstwo[i], i);
            }
        });


        // TODO: For Cursor Changes
        chart.axisX2[0].set("interval", chart.axisY[0].get("interval"));
        // set_scale_chart();
        set_scale_chart_test();
        // zoomChart(1);
    }
    // Scaling Borehole Scatter Plot
    function set_to() {
        // chart.render();
        // alert("Fasdf");
    }
    var zoom_limit_x, zoom_limit_y, inter;
    // function plotly(){

    //     var trace1 = {
    //         x: [1, 2, 3, 4, 5],
    //         y: [1, 6, 3, 6, 1],
    //         mode: 'markers',
    //         type: 'scatter',
    //         // name: 'Team A',
    //         text: ['A-1', 'A-2', 'A-3', 'A-4', 'A-5'],
    //         marker: { size: 12 }
    //       };

    //       var trace2 = {
    //         x: [1.5, 2.5, 3.5, 4.5, 5.5],
    //         y: [4, 1, 7, 1, 4],
    //         mode: 'markers',
    //         type: 'scatter',
    //         name: 'Team B',
    //         text: ['B-a', 'B-b', 'B-c', 'B-d', 'B-e'],
    //         marker: { size: 12 }
    //       };

    //       var data = [ trace1 ];

    //       var layout = {
    //         xaxis: {
    //           range: [ 0    , 15 ]
    //         },
    //         yaxis: {
    //           range: [0, 5]
    //         }
    //         // title:'Data Labels Hover'
    //       };

    //       Plotly.newPlot('tester', data, layout,{displaylogo: false});
    // }

    function set_scale_chart_test() {
        if (max_x > max_y) {
            var t;
            // alert("X is Maximum");
            for (var i = 0; i < range.length; i++) {
                if (range[i] > max_x) {
                    // alert(range[i]);
                    t = range[i];
                    break;
                }

            }
            // alert(t);
            // chart.axisY[0].set("maximum",3000000)
            chart.axisY[0].set("viewportMaximum", t);
            chart.axisX2[0].set("viewportMaximum", t * 3);
            chart.axisX2[0].set("interval", chart.axisY[0].get("interval"));
        } else if (max_y > max_x) {
            // alert("Y is Maximum");
            for (var i = 0; i < range.length; i++) {
                if (range[i] > max_y) {
                    // alert(range[i]);
                    t = range[i];
                    break;
                }

            }
            chart.axisY[0].set("viewportMaximum", t);
            chart.axisX2[0].set("viewportMaximum", t * 3);
            chart.axisX2[0].set("interval", chart.axisY[0].get("interval"));
        } else {
            for (var i = 0; i < range.length; i++) {
                if (range[i] > max_y) {
                    alert(range[i]);
                    t = range[i];
                    break;
                }

            }
            chart.axisY[0].set("viewportMaximum", t);
            chart.axisX2[0].set("viewportMaximum", t * 3);
            chart.axisX2[0].set("interval", chart.axisY[0].get("interval"));
        }
    }

    function set_scale_chart() {
        var parentElement = document.getElementsByClassName("canvasjs-chart-toolbar");
        var childElement = document.getElementsByTagName("button");
        // console.log(childElement);
        //    console.log(childElement[0]);
        childElement[1].click();
        // }
        if (max_x > max_y) {
            console.log("Max X is maximum");
            console.log(max_x, max_y);
            var limit = (max_x / 2);
            console.log(limit);
            chart.axisX2[0].set("viewportMaximum", max_x + 50000);
            zoom_limit_x = max_x * 3;

            // console.log(chart.axisX2[0].set("viewportMaximum",limit * 6));
            chart.axisX2[0].set("viewportMinimum", 0);
            chart.axisY[0].set("viewportMinimum", 0);
            chart.axisY[0].set("includeZero", false);
            // console.log(chart.axisY[0].set("viewportMaximum",limit * 2));
            chart.axisY[0].set("viewportMaximum", max_y + 50000);
            zoom_limit_y = max_x;
            chart.axisY[0].set("interval", chart.axisX2[0].get("interval"));
            inter = chart.axisY[0].get("interval");
        } else if (max_x < max_y) {

            console.log("Max X is minimum");
            // console.log(max_x, max_y);
            console.log(max_x, max_y);
            var limit = (max_x / 2);
            console.log(limit);
            chart.axisX2[0].set("viewportMaximum", max_y * 4.5);
            // console.log(chart.axisX2[0].set("viewportMaximum",limit * 6));
            zoom_limit_x = max_y * 4.5;
            // chart.axisX2[0].set("viewportMinimum", 0);
            // chart.axisY[0].set("viewportMinimum", 0);
            chart.axisY[0].set("includeZero", true);
            // console.log(chart.axisY[0].set("viewportMaximum",limit * 2));
            chart.axisY[0].set("viewportMaximum", max_y * 1.5);
            chart.axisY[0].set("interval", chart.axisX2[0].get("interval"));
            inter = chart.axisY[0].get("interval");
            zoom_limit_y = max_y * 1.5;

            console.log(zoom_limit_x);
            console.log(zoom_limit_y);
        } else {
            console.log("Both are Equal");
            console.log(max_x, max_y);
            chart.axisX2[0].set("viewportMaximum", max_y * 4.5);
            // console.log(chart.axisX2[0].set("viewportMaximum",limit * 6));
            // chart.axisX2[0].set("viewportMinimum", 0);
            // chart.axisY[0].set("viewportMinimum", 0);
            chart.axisY[0].set("includeZero", false);
            // console.log(chart.axisY[0].set("viewportMaximum",limit * 2));
            chart.axisY[0].set("viewportMaximum", max_y * 1.5);
            inter = chart.axisY[0].get("interval");
            zoom_limit_x = max_y * 4.5;
            zoom_limit_y = max_y * 1.5;
            console.log(zoom_limit_x);
            console.log(zoom_limit_y);
        }
    }

    function chartt() {

        if (max_x > max_y) {
            inter_f_x = Math.round(max_x / 40);
            inter_f_y = Math.round(max_x / 10);

        } else {
            inter_f_x = Math.round(max_y / 10);
            inter_f_y = Math.round(max_y / 10);
        }
        console.log(inter_f_x);
        chart.axisY[0].set("maximum", max_y);

        chart.axisY[0].set("minimum", min_y);

        chart.axisX2[0].set("maximum", max_x);
        chart.axisX2[0].set("minimum", min_x);

    }

    // Getting Svaed Lines if available
    function drawdefinedLine(chart, total) {
        // console.log
        var ctx = chart.ctx;

        for (var i = 0; i < total; i++) {
            // ctx.beginPath();     //Change Line Width/Thickness
            var ff = [];
            // ff = file.get("sectionLines.Coordinates."+i);
            ff = arr[i];
            if (ff == null) {
                continue;
            } else {
                drawLinedefined(arr[i], ctx, i);

            }

            console.log(arr[i]);

        }
    }
    // Drwaing the Lines got from saved file
    function drawLinedefined(ff, ctx, i) {
        console.log(chart.axisX2[0].convertValueToPixel(ff.x1));
        console.log(chart.axisX2[0].convertPixelToValue(ff.x1));
        console.log(ff.x1);
        // var ctx = chart.ctx;
        store.push({
            x1: ff.x1,
            y1: ff.y1,
            x2: ff.x2,
            y2: ff.y2,
            section: i + 1
        });
        refresh();


        // drawLabel(ctx, ff.line_no, ff, "center", 0);
    }

    // Add line Funciton Call
    var line, isDown;

    var undoRedo = new Array();

    // document.getElementById("but_add_line").addEventListener("click", function () {
    // document.getElementById("but_add_line").hidden = true;
    function add_sec_line(element, sec_key) {
        console.log(arr);
        chart.set("zoomEnabled", false);
        var temp = 0;

        var lineCoordinates = {};


        var parentOffset = $(chart.container).offset();
        jQuery(chart.container).on({
            mousedown: function (e) {

                isDown = true;
                // chart.set("zoomEnabled", false);
                lineCoordinates.x1 = e.pageX - parentOffset.left;
                lineCoordinates.y1 = e.pageY - parentOffset.top;
            },
            mouseup: function (e) {
                isDown = false;
                console.log(lineCoordinates);




                if (temp == 0) {
                    // chart.set("zoomEnabled", false);
                    drawLine(chart, lineCoordinates, "line_count", "center", 10, sec_key);
                    store.push({
                        x1: lineCoordinates.x1,
                        y1: lineCoordinates.y1,
                        x2: lineCoordinates.x2,
                        y2: lineCoordinates.y2,
                        section: sec_key
                    });
                    line_array.push({
                        x1: lineCoordinates.x1,
                        y1: lineCoordinates.y1,
                        x2: lineCoordinates.x2,
                        y2: lineCoordinates.y2
                    });

                    // drawLabel(chart,line_count,lineCoordinates, "center",10);
                    temp++;
                    // chart.set("zoomEnabled", true);
                }

            },
            mousemove: function (e) {
                if (!isDown) return;
                lineCoordinates.x2 = e.pageX - parentOffset.left;
                lineCoordinates.y2 = e.pageY - parentOffset.top;
                refresh();
                drawLine_mov(chart, lineCoordinates);
            }
        });
    }
    // });

    // }
    // alert("done level one");


    // }

    function refresh() {
        console.log(store);
        chart.render();
        for (var r = 0; r < store.length; r++) {
            if (store[r] == null) {
                continue;
            } else {


                var ctx = chart.ctx;

                ctx.beginPath();
                console.log(store[r]);
                ctx.strokeStyle = "#000"; //Change Line Color
                ctx.lineWidth = 2; //Change Line Width/Thickness
                ctx.moveTo(store[r].x1, store[r].y1);
                // ctx.moveTo(store[r].x1, store[r].y1);
                ctx.lineTo(store[r].x2, store[r].y2);
                // ctx.lineTo(store[r].x2, store[r].y2);
                ctx.stroke();
                alignment = "center";
                if (!alignment) alignment = 'center';
                // if (!padding) padding = 0;
                var p1 = [store[r].x1, store[r].y1];
                var p2 = [store[r].x2, store[r].y2];
                // alert(text);
                var textt = "Section " + store[r].section;
                var dx = store[r].x2 - store[r].x1;
                var dy = store[r].y2 - store[r].y1;
                var len = Math.sqrt(dx * dx + dy * dy);
                var avail = len - 2 * 0;
                var angle = Math.atan2(dy, dx);
                if (angle < -Math.PI / 2 || angle > Math.PI / 2) {
                    var p = p1;
                    p1 = p2;
                    p2 = p;
                    dx *= -1;
                    dy *= -1;
                    angle -= Math.PI;
                }

                var p, pad;
                if (alignment == 'center') {
                    p = p1;
                    pad = 1 / 2;
                } else {
                    var left = alignment == 'left';
                    p = left ? p1 : p2;
                    pad = 0 / len * (left ? 1 : -1);
                }


                var ctx2 = chart.ctx;
                ctx1.save();
                ctx1.textAlign = alignment;
                ctx1.translate(store[r].x1, store[r].y1);
                // alert(lineCoordinates.x1,lineCoordinates.y1);
                ctx1.rotate(Math.atan2(dy, dx));
                ctx1.fillStyle = "black"
                ctx1.fillText(textt, 0, 0);
                ctx1.restore();
                // drawLabel(ctx1, line_count, store[r], "center", 0,text_on_line);
            }
        }
    }

    function refreshline() {
        console.log(store);
        chart.render();
        for (var r = 0; r <= store.length; r++) {
            var ctx = chart.ctx;

            ctx.beginPath();
            console.log(store[r]);
            ctx.strokeStyle = "#000"; //Change Line Color
            ctx.lineWidth = 2; //Change Line Width/Thickness
            ctx.moveTo(store[r].x1, store[r].y1);
            // ctx.moveTo(store[r].x1, store[r].y1);
            ctx.lineTo(store[r].x2, store[r].y2);
            // ctx.lineTo(store[r].x2, store[r].y2);
            ctx.stroke();
        }
    }
    // Marker Image Url
    function drawLine_mov(chart, lineCoordinates) {
        var ctx = chart.ctx;

        ctx.beginPath();
        ctx.strokeStyle = "#000"; //Change Line Color
        ctx.lineWidth = 2; //Change Line Width/Thickness
        ctx.moveTo(lineCoordinates.x1, lineCoordinates.y1);
        ctx.lineTo(lineCoordinates.x2, lineCoordinates.y2);
        ctx.stroke();
    }
    var iteration = 0;
    var ymaxy, yminy;

    function zoomChart_test(valzoom) {
        if (max_x > max_y) {
            var t;
            // alert(valzoom);
            for (var i = 0; i < range.length; i++) {
                if (range[i] > max_x) {
                    // alert(range[i]);
                    t = range[i];
                    break;
                }

            }
            // alert(t);
            chart.axisY[0].set("viewportMaximum", (t) / valzoom);
            chart.axisY[0].set("maximum", t);
            chart.axisX2[0].set("viewportMaximum", ((t / valzoom) * 3));
            chart.axisX2[0].set("interval", chart.axisY[0].get("interval"));
        } else if (max_y > max_x) {
            // alert("Y is Maximum");
            for (var i = 0; i < range.length; i++) {
                if (range[i] > max_y) {
                    // alert(range[i]);
                    t = range[i];
                    break;
                }
                chart.axisY[0].set("viewportMaximum", t);
                chart.axisX2[0].set("viewportMaximum", t * 3);
                chart.axisX2[0].set("interval", chart.axisY[0].get("interval"));
            }
            alert(t);
        } else {
            alert("Equals");
        }
    }

    function zoomChart(valzoom) {
        var parentElement = document.getElementsByClassName("canvasjs-chart-toolbar");
        var childElement = document.getElementsByTagName("button");
        // console.log(childElement);
        //    console.log(childElement[0]);
        childElement[1].click();

        console.log(chart.axisY[0].get("interval"));
        // switch(valzoom){
        //     case 1:
        //     case 2:
        //     case 3:
        //     case 4:
        //     default:
        // }
        const gg = chart.axisX2[0].get("viewportMaximum");
        vall = 1 - (valzoom / 10);

        if (vall == 0)
            vall = 0.1;
        zoom_text.innerHTML = valzoom + "x";

        graph_max_x = chart.axisX2[0].get("viewportMaximum");
        // chart.axisY[0].set("maximum", 10000000);
        // chart.axisY[0].set("minimum", 0);
        // chart.axisX2[0].set("maximum", 10000000);
        // chart.axisX2[0].set("mainimum", 0);
        chart.axisX2[0].set("viewportMinimum", 0);
        chart.axisX2[0].set("viewportMaximum", zoom_limit_x * vall);
        console.log(zoom_limit_x);
        chart.axisY[0].set("viewportMinimum", 0);
        chart.axisY[0].set("viewportMaximum", zoom_limit_y * vall);
        // chart.axisX2[0].set("", zoom_limit_x * vall);
        chart.axisX2[0].set("interval", inter / valzoom);
        chart.axisY[0].set("interval", inter / valzoom);

        console.log(gg);
        // chart.axisX2[0].set("interval",chart.axisY[0].get("interval"));
        // if (max_x > max_y) {

        // chart.axisX2[0].set("interval", chart.axisX2[0].get("interval"));
        // chart.axisY[0].set("viewportMaximum", lim_y_end - (valzoom * chart.axisX2[0].get("interval")));
        // chart.axisX2[0].set("viewportMaximum", lim_x_end - (valzoom * chart.axisX2[0].get("interval")));
        // chart.axisY[0].set("viewportMinimum", lim_y_start + (valzoom * chart.axisX2[0].get("interval")));
        // chart.axisX2[0].set("viewportMinimum", lim_x_start + (valzoom * chart.axisX2[0].get("interval")));
        // chart.axisY[0].set("interval", chart.axisX2[0].get("interval"));
        // chart.render;
        // } else {

        // chart.axisY[0].set("interval", lim_y_end - (valzoom * chart.axisY[0].get("interval")));

        // chart.axisY[0].set("interval", chart.axisY[0].get("interval"));
        // chart.axisY[0].set("viewportMaximum", lim_y_end - (valzoom * chart.axisY[0].get("interval")));
        // chart.axisX2[0].set("viewportMaximum", lim_x_end - (valzoom * chart.axisY[0].get("interval")));
        // chart.axisY[0].set("viewportMinimum", lim_y_start + (valzoom * chart.axisY[0].get("interval")));
        // chart.axisX2[0].set("viewportMinimum", lim_x_start + (valzoom * chart.axisY[0].get("interval")));
        // chart.axisX2[0].set("interval", chart.axisY[0].get("interval"));
        // chart.render;
    }

    // chart.axisX2[0].set("interval",chart.axisY[0].get("interval"));
    // chart.axisY[0].set("interval",chart.axisX2[0].get("interval")/valzoom);
    // console.log("DSsd");
    // iteration++;
    // document.getElementById("scale").value = valzoom;
    // console.log(valzoom);
    // //    valzoom = 10 - valzoom;
    // console.log(valzoom);
    // // console.log("delta is positive zoom  equals to 5");
    // var x_val = valzoom * 2;
    // if (iteration == 1) {
    //     ymaxy = chart.axisY[0].get("viewportMaximum");
    //     yminy = chart.axisY[0].get("viewportMinimum");
    // }

    //       var maxyzoom = chart.axisY[0].get("viewportMaximum");
    // var maxxzoom = chart.axisX2[0].get("viewportMaximum");
    // var minxzoom = chart.axisX2[0].get("viewportMinimum");
    // var minyzoom = chart.axisY[0].get("viewportMinimum");
    // var interx = chart.axisX2[0].get("interval");
    // var intery = chart.axisY[0].get("interval");

    // chart.axisY[0].set("viewportMaximum",Math.round(valof_max_y / Math.abs(valzoom)));
    // if (max_x > max_y) {
    //     var zoom_valuee = inter_f_x / 4;
    //     var get_inter = chart.axisX2[0].get("interval");
    //     chart.axisY[0].set("viewportMinimum", (get_inter * valzoom));
    //     // chart.axisY[0].set("interval", Math.round(inter_f_x / Math.abs(valzoom)));
    //     // chart.axisY[0].set("viewportMinimum", Math.round(valof_min_y / Math.abs(valzoom)));
    //     // chart.axisX2[0].set("viewportMaximum", Math.round(valof_max_x / Math.abs(valzoom)));
    //     chart.axisY[0].set("viewportMaximum", chart.axisY[0].get("interval") * 24 - (valzoom * get_inter));
    //     // chart.axisX2[0].set("interval", Math.round(inter_f_x / Math.abs(valzoom)));
    //     // chart.axisX2[0].set("viewportMinimum", Math.round(valof_min_x / Math.abs(valzoom)));
    // } else {
    //     var zoom_valuee = inter_f_y / 4;
    //     // console.log(constt);
    //     var get_inter = chart.axisY[0].get("interval");
    //     var get_y_max = chart.axisY[0].get("viewportMaximum");
    //     var get_y_min = chart.axisY[0].get("viewportMinimum");

    //     console.log(get_y_max, get_y_min);
    //     // chart.axisY[0].set("interval", Math.round(zoom_valuee / Math.abs(valzoom)));
    //     // chart.axisY[0].set("interval", Math.round(inter_f_y / Math.abs(valzoom)));
    //     // chart.axisY[0].set("viewportMinimum", Math.round(valof_min_y / Math.abs(valzoom)));
    //     chart.axisX2[0].set("viewportMinimum", (get_inter * valzoom));
    //     // chart.axisY[0].set("viewportMinimum", get_y_min + (get_inter * valzoom));
    //     chart.axisY[0].set("viewportMaximum", ymaxy - (get_inter * valzoom));
    //     console.log(ymaxy);
    //     // chart.axisY[0].set("viewportMinimum", chart.axisY[0].get("viewportMinimum") + (get_inter * valzoom));
    //     // chart.axisX2[0].set("viewportMaximum", Math.round(valof_max_x / Math.abs(valzoom)));
    //     // chart.axisX2[0].set("viewportMaximum", chart.axisY[0].get("interval") * 24 - (valzoom * get_inter));
    //     // chart.axisY[0].set("viewportMaximum", chart.axisY[0].get("interval") * 24 - (valzoom * get_inter));
    //     // chart.axisY[0].set("viewportMaximum", chart.axisY[0].get("interval") * 24 - (valzoom * get_inter));
    //     // var get_inter = chart.axisY[0].get("interval");

    //     // var get_max = chart.axisX2[0].get("viewportMaximum");
    //     // console.log(get_max);
    //     // var round1 = Math.floor(get_max/10) *10;
    //     // console.log(round1*10);
    //     // console.log(CanvasJS.formatNumber(round1, "##,000"));
    //     // console.log(chart.axisX2[0].get("viewportMaximum"))
    //     // chart.axisX2[0].set("viewportMinimum",0);
    //     // chart.axisX2[0].set("viewportMinimum", Math.round(zoom_valuee / Math.abs(valzoom)));
    //     // chart.axisX2[0].set("interval", Math.round(inter_f_y / Math.abs(valzoom)));
    // }
    // chart.axisY[0].set("viewportMaximum",Math.round(maxyzoom / Math.abs(valzoom)));
    // chart.axisY[0].set("interval", Math.round(interx / Math.abs(valzoom/10)));
    // chart.axisY[0].set("viewportMinimum", Math.round(minyzoom / Math.abs(valzoom)));
    // chart.axisX2[0].set("viewportMaximum", Math.round(maxxzoom / Math.abs(valzoom)));
    // chart.axisX2[0].set("interval", Math.round(intery / Math.abs(valzoom/10)));
    // chart.axisX2[0].set("viewportMinimum", Math.round(minxzoom / Math.abs(valzoom)));

    // }

    function data_get(limit) {
        var dps = [];

        for (var j = 1; j <= save_bh_count; j++) {
            var jj = j - 1;
            var bh = file.get("soilData.boreholeArray." + jj + "");
            var x_val = parseInt(file.get("soilData." + bh + ".stratumData.Eastings"), 10);
            var y_val = parseInt(file.get("soilData." + bh + ".stratumData.Northings"), 10);
            // alert(x_val+','+y_val);
            dps.push({
                // indexLabel: String(bh),
                name_to_call: String(bh),
                markerImageUrl: "./assets/img/bh.png",
                indexLabelFontSize: 12,
                x: x_val,

                markerSize: 5,

                y: y_val

            });
            dps_x.push(parseInt(x_val, 10));
            dps_y.push(parseInt(y_val, 10));


            // var points = [];




            // }
            // console.log(points);
            // return points;


        }
        console.log(dps_x);
        var chart_min_x = Math.min(...dps_x);
        var chart_min_y = Math.min(...dps_y);
        var chart_max_y = Math.max(...dps_y);
        var chart_max_x = Math.max(...dps_x);

        max_x = chart_max_x;
        max_y = chart_max_y;
        min_x = chart_min_x;
        min_y = chart_min_y;
        console.log(min_x);
        console.log(min_x / 10);
        console.log(min_y);
        console.log(min_y / 10);
        console.log(max_x);
        console.log(max_x / 10);
        console.log(max_y);
        console.log(max_y / 10);
        // chart.axisY[0].set("maximum", max_y);
        // chart.axisX2[0].set("maximum", max_x);
        // chart.axisY[0].set("minimum", min_y);
        // chart.axisX2[0].set("minimum", min_x);
        // chartt();
        return dps;

    }
    // Data Generator for calling the Custom markers
    function dataGenerator(limit) {
        var y = 100;
        var points = [];
        for (var i = 0; i < limit; i += 1) {
            y += (Math.random() * 100 - 50);
            points.push({
                x: i - limit / 2,
                y: y
            });
        }
        return points;
    }

    // Fucntion that writes on the drwan Line
    function drawLabel(ctx1, text, lineCoordinates, alignment, padding, text_on_line) {
        if (!alignment) alignment = 'center';
        if (!padding) padding = 0;
        var p1 = [lineCoordinates.x1, lineCoordinates.y1];
        var p2 = [lineCoordinates.x2, lineCoordinates.y2];
        // alert(text);
        var textt = "Section " + text_on_line;
        var dx = lineCoordinates.x2 - lineCoordinates.x1;
        var dy = lineCoordinates.y2 - lineCoordinates.y1;
        var len = Math.sqrt(dx * dx + dy * dy);
        var avail = len - 2 * padding;
        var angle = Math.atan2(dy, dx);
        if (angle < -Math.PI / 2 || angle > Math.PI / 2) {
            var p = p1;
            p1 = p2;
            p2 = p;
            dx *= -1;
            dy *= -1;
            angle -= Math.PI;
        }

        var p, pad;
        if (alignment == 'center') {
            p = p1;
            pad = 1 / 2;
        } else {
            var left = alignment == 'left';
            p = left ? p1 : p2;
            pad = padding / len * (left ? 1 : -1);
        }


        var ctx2 = chart.ctx;
        ctx1.save();
        ctx1.textAlign = alignment;
        ctx1.translate(lineCoordinates.x1, lineCoordinates.y1);
        // alert(lineCoordinates.x1,lineCoordinates.y1);
        ctx1.rotate(Math.atan2(dy, dx));
        ctx1.fillStyle = "black"
        ctx1.fillText(textt, 0, 0);
        ctx1.restore();
    }
    // Function that drwas the line
    function drawLine(chart, lineCoordinates, text, alignment, padding, text_on_line) {

        line_count++;
        arr.push({
            // arr.push({
            line_no: line_count,
            x1: chart.axisX2[0].convertPixelToValue(lineCoordinates.x1),
            x2: chart.axisX2[0].convertPixelToValue(lineCoordinates.x2),
            y1: chart.axisY[0].convertPixelToValue(lineCoordinates.y1),
            y2:  chart.axisY[0].convertPixelToValue(lineCoordinates.y2),
            section:text_on_line
        });
        // arr.push(lc_arr);
        console.log(lc_arr);

        var sec_x1 = chart.axisX2[0].convertPixelToValue(lineCoordinates.x1);
        var sec_x2 = chart.axisX2[0].convertPixelToValue(lineCoordinates.x2);
        var sec_y1 = chart.axisY[0].convertPixelToValue(lineCoordinates.y1);
        var sec_y2 = chart.axisY[0].convertPixelToValue(lineCoordinates.y2);
        var arr_Sec_points = new Array();
        arr_Sec_points.push({
            x1: sec_x1,
            x2: sec_x2,
            y1: sec_y1,
            y2: sec_y2
        });
        console.log(arr_Sec_points);
        console.log(section_array.length);
        console.log(sec);
        // alert(sec);
        for (var i = 1; i <= text_on_line; i++) {
            if (i == text_on_line) {
                console.log(i, text_on_line);
                console.log(section_array[i]);
                console.table(section_array[i]);
                console.log(section_array[i].length);
                for (var j = 0; j < section_array[sec].length; j++) {


                    var slope = ((sec_y2 - sec_y1) / (sec_x2 - sec_x1));
                    console.log(slope);
                    var intercept = (sec_y1 - (slope * sec_x1));
                    console.log(intercept);
                    var bh_slope = -1 * (1 / slope);
                    var bh_y = file.get("soilData." + section_array[sec][j] + ".stratumData.Northings");
                    var bh_x = file.get("soilData." + section_array[sec][j] + ".stratumData.Eastings");
                    var bh_intercept = bh_y - (bh_slope * bh_x);
                    var intersect_x = (bh_intercept - intercept) / (slope - bh_slope);
                    var intersect_y = (slope * intersect_x) + intercept;
                    var section_dist_x = intersect_x - sec_x1;
                    var section_dist_y = intersect_y - sec_y1;
                    var dist = Math.sqrt(section_dist_x * section_dist_x + section_dist_y * section_dist_y);
                    var bh_name = section_array[text_on_line][j];
                    // console.log(bh_name);
                    // alert(bh_name);
                    console.log("bh x :" + bh_x);
                    console.log("bh y :" + bh_y);
                    console.log("bh slope :" + bh_slope);
                    console.log("bh intercept :" + bh_intercept);
                    console.log("Section Slope :" + slope);
                    console.log("Section intercept :" + intercept);
                    console.log("Intersection X :" + intersect_x);
                    console.log("Intersection Y :" + intersect_y);
                    console.log("Distance :" + dist);
                    console.log("Section Dist X :" + section_dist_x);
                    console.log("Section Dist Y :" + section_dist_y);
                    distance_section_array[text_on_line][bh_name] = dist;
                    console.log(i);
                    file.set("sectionLines.data.section_" + i + "." + bh_name, dist);
                    file.save();
                    // ctx1.beginPath();
                    // ctx1.moveTo(chart.axisX2[0].convertValueToPixel(bh_x), chart.axisY[0].convertValueToPixel(bh_y));
                    // ctx1.lineTo(chart.axisX2[0].convertValueToPixel(intersect_x), chart.axisY[0].convertValueToPixel(intersect_y));
                    // ctx1.stroke();
                }

                // var slope = ((sec_y2 - sec_y1) / (sec_x2 - sec_x1));
                // console.log(slope);
                // var intercept = (sec_y1 - (slope * sec_x1));
                // console.log(intercept);
                // console.log(distance_section_array);
                // file.set("sectionLines.Coordinates", arr);
                // file.set("sectionLines.total", arr.length);
                // file.set("sectionLines.lastLineNo", line_count);




                console.log(arr);

                // ctx1.beginPath();
                ctx1.strokeStyle = $('#selColor').val(); //Change Line Color
                ctx1.lineWidth = $('#selWidth').val();
                // ctx1.moveTo(sec_x1, sec_y1);
                // ctx1.moveTo(lineCoordinates.x1, lineCoordinates.y1);
                // ctx1.lineTo(sec_x2, sec_y2);
                // ctx1.lineTo(lineCoordinates.x2, lineCoordinates.y2);
                // ctx1.stroke();

                drawLabel(ctx1, line_count, lineCoordinates, "center", 0, text_on_line);
            }

        }




        // ctx1.stroke();


    }
    // var zoom_limit;
    function chart_call() {
        if (max_x > max_y) {
            chart.axisY[0].set("interval", chart.axisX2[0].get("interval"));
            chart.axisY[0].set("viewportMaximum", actual_y_end * chart.axisX2[0].get("interval"));
            chart.axisX2[0].set("viewportMaximum", actual_x_end);
            chart.axisX2[0].set("viewportMinimum", actual_x_start);
            chart.axisY[0].set("viewportMinimum", actual_y_start * chart.axisX2[0].get("interval"));
            chart.render;
        } else if (max_x < max_y) {
            chart.axisX2[0].set("interval", chart.axisY[0].get("interval"));
            chart.axisY[0].set("viewportMaximum", actual_y_end);
            chart.axisX2[0].set("viewportMaximum", actual_x_end * chart.axisY[0].get("interval"));
            chart.axisX2[0].set("viewportMinimum", actual_x_start * chart.axisY[0].get("interval"));
            chart.axisY[0].set("viewportMinimum", actual_y_start);
            chart.render;
        } else {

        }
    }
    // Fucntion Redo
    function set_size() {


    }

    function redo() {

        // alert("Redo");
        // console.lo
        var f = undo_array.length;
        // console.log("redo f"+f);
        // console.log();
        if (f != 0) {
            store.push(undo_array[f - 1]);
            undo_array.splice(f - 1, 1);
        }
        refreshline();


        // ctx1.redo();
    }
    // Undo Funciton
    function undo() {
        // console.log(arr);
        // arr.pop();
        // console.log(arr);
        // alert("Undo")
        // line_array
        var f = store.length;
        if (f != 0) {
            undo_array.push(store[f - 1]);
            store.splice(f - 1, 0);
        }


        refreshline();
        // Should be done
        // ctx1.undo();
    }
    // Function to erase All
    function eraseAll() {

        w = chart.width;
        h = chart.height;

        var ctx = chart.ctx;

        var m = confirm("Want to clear");

        if (m) {
            chart.render();
        }
    }
    // Deleting the Fucniton ( same as edit )
    function del_section_var() {
        del = true;
    }
    // Picking the Section By Click with Duplicate Array Validation
    function section_divide(e) {

        if (del == true) {
            for (var i = 0; i < section_array[curr_sec].length; i++) {
                if (section_array[curr_sec][i] === e.dataPoint.name_to_call) {
                    section_array[curr_sec].splice(i, 1);
                }
            }

            function getUnique(arr) {

                const final = [];

                arr.map((e, i) => !final.includes(e) && final.push(e))

                return final
            }
            section_array[curr_sec] = getUnique(section_array[curr_sec]);

            del = false;
            sec_call('' + curr_sec + '');
        } else {
            section_array[curr_sec].push(e.dataPoint.name_to_call);

            function getUnique(arr) {

                const final = [];

                arr.map((e, i) => !final.includes(e) && final.push(e))

                return final
            }
            section_array[curr_sec] = getUnique(section_array[curr_sec]);
            sec_call('' + curr_sec + '');
        }




        //   console.log(getUnique(jobs))

    }
