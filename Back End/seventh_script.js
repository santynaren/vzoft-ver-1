var ipcRenderer = require('electron').ipcRenderer;
var editJsonFile = require("edit-json-file");
var type_Cart = ["scatter", "rangeColumn", "scatter"];
var axisXtype = ["secondary", "secondary", "secondary"];
var data_points = new Array();
var chart_Axisx2 = new Array();
var title_val;
var y_data_test = [];
var max_x,max_y;
var test_valll = ["SPT", "Cu", "Ics", "UcMap"];
var range = ['1000', '2000', '3000', '4000', '5000', '6000', '7000', '8000', '9000', '10000', '20000', '30000', '40000', '50000', '60000', '70000', '80000', '90000', '100000', '150000', '100000', '200000', '300000', '400000', '500000', '600000', '700000', '800000', '900000', '1000000'];
const {
    BrowserWindow
} = require('electron').remote;
const printer = require('electron-print');
const ipc = require('electron').ipcRenderer;

var strips = [];
var strips1 = [];
var strips2 = [];


var test_arr = ["SPT", "Cu", "Ics", "UcMap"];

function myFunction() {

}
var test_curr = 0;
var title_chart = ["Borehole Plot", "Geographic Plot", "Test Plot"];
var table = document.getElementById("res_table");
var remote = require('electron').remote;
const app = remote.app;

var tempFilePath = remote.getGlobal('path_file');
const CanvasJS = require('./assets/javascript/canvasjs.min.js');
let file = editJsonFile(tempFilePath);

var type_of_mode = file.get("projectDetails.typeOfMode");
if (type_of_mode == "mbgl") {
    title_val = "Depth (mbgl)";
} else {
    title_val = "Elevation (mAOD)";
}
for (var index_gm_val = 1; index_gm_val <= file.get("soilData.groundModelData.noOfGm"); index_gm_val++) {
    strips[index_gm_val] = [];
    strips2[index_gm_val] = [];
    // for (var d_s = 0; d_s < file.get("designData.strataName.gm" + index_gm_val + ".length"); d_s++) {
    //     var design_Strata = file.get("designData.strataName.gm" + index_gm_val + "." + d_s);
    //     if (design_Strata != "ground_Water") {
    //         strips2[index_gm_val].push({
    //             value: file.get("designData.stratumData.gm" + index_gm_val + "." + d_s + "." + design_Strata),
    //             label: design_Strata,
    //             labelPlacement:"outside"

    //         })
    //     }

    // }
    for (var d_s = 0; d_s < file.get("designData.strataName.gm" + index_gm_val + ".length"); d_s++) {
        var design_Strata = file.get("designData.strataName.gm" + index_gm_val + "." + d_s);
        // console.log(design_Strata);
        if (design_Strata == "ground_Water") {
            strips[index_gm_val].push({


                value: file.get("designData.stratumData.gm" + index_gm_val + "." + d_s + "." + design_Strata),
                color: "#0000ff"
                // label: file.get("designData.stratumData.gm" + index_gm_val + "." + d_s).stratumName,

            });
        } else {
            strips[index_gm_val].push({


                value: file.get("designData.stratumData.gm" + index_gm_val + "." + d_s + "." + design_Strata),
                label: file.get("designData.stratumData.gm" + index_gm_val + "." + d_s).stratumName,
                color: "#000",
                labelFontColor: "#000"

            });
        }

    }

}



var dp3 = {
    title: "SPT",
    gridDashType: "dot",
    gridThickness: 0.1,
    minimum: 0,
    viewportMaximum: 60,
    // tickLength: 15,
    // tickColor: "red",
    crosshair: {
        enabled: true
    }
};
var dp10 = {
    title: "Cu",
    gridDashType: "dot",
    gridThickness: 0.1,
    minimum: 0,
    viewportMaximum: 250,
    // tickLength: 15,
    // tickColor: "red",
    crosshair: {
        enabled: true
    }
};
var dp11 = {
    title: "Ics",
    gridDashType: "dot",
    gridThickness: 0.1,
    minimum: 0,
    viewportMaximum: 50,
    // tickLength: 15,
    // tickColor: "red",
    crosshair: {
        enabled: true
    }
};
var dp12 = {
    title: "UcMap",
    gridDashType: "dot",
    gridThickness: 0.1,
    minimum: 0,
    viewportMaximum: 100,
    // tickLength: 15,
    // tickColor: "red",
    crosshair: {
        enabled: true
    }
};
var dp6 = {
    title: "Eastings",
    titleFontSize: 12,
    labelFontSize: 8,
    titleFontStyle: "oblique",
    titleFontColor: "black",
    labelFontColor: "black",
    gridDashType: "dot",
    gridThickness: 0.1,
    minimum:0,
    // tickLength: 15,
    // tickColor: "red",
    crosshair: {
        enabled: true
    }
};
var dp7 = {
    title: "Chainage",
    titleFontSize: 12,
    labelFontSize: 8,
    titleFontStyle: "oblique",
    titleFontColor: "black",
    labelFontColor: "black",
    gridDashType: "dot",
    gridThickness: 0.1,

    crosshair: {
        enabled: true
    }
};

chart_Axisx2.push(dp6); //0 borehole plot
chart_Axisx2.push(dp7); //1 borehole geogrpahic
chart_Axisx2.push(dp3); //2 SPT
chart_Axisx2.push(dp10); //3 Cu
chart_Axisx2.push(dp11); //4 IcS
chart_Axisx2.push(dp12); //5 UcMap




function res() {
// alert("jk");
    document.getElementById("pname").innerHTML = file.get("projectName");
    document.getElementById("pdate").innerHTML = file.get("projectDetails.date");
    document.getElementById("jnum").innerHTML = file.get("projectDetails.jobNumber");
    document.getElementById("eng").innerHTML = file.get("projectDetails.engineer");
    document.getElementById("client").innerHTML = file.get("projectDetails.client");
    document.getElementById("location").innerHTML = file.get("projectDetails.location");
    document.getElementById("nbh").innerHTML = file.get("soilData.noOfBh");
    document.getElementById("ngm").innerHTML = file.get("soilData.groundModelData.noOfGm");

var strata_arr = [];
    for (var i = 1; i <= file.get("soilData.groundModelData.noOfGm"); i++) {
        // alert("fd");
        if(file.get("soilData.groundModelData.gm"+i)){

strata_arr[i] =[];
        var tabs_div = document.getElementById("myTab");
        var tabs_body = document.getElementById("myTabContent");

        var tabs = document.createElement("li");
        tabs.setAttribute("class", "nav-item");

        var tabs_link = document.createElement("button");
        tabs_link.setAttribute("href", "#gm_" + i);
        tabs_link.setAttribute("ondblclick", "load_chart(" + i + ")");


        tabs_link.setAttribute("data-toggle", "tab");
        tabs_link.setAttribute("role", "tab");
        tabs_link.setAttribute("class", "nav-link");
        // if (i == 1) {

        // }

        var tabs_text = document.createTextNode("Ground Model " + i);
        tabs_link.appendChild(tabs_text);
        tabs.appendChild(tabs_link);
        tabs_div.appendChild(tabs);




        var h1_tag = document.createElement("h1");
        var h1_tag_text = document.createTextNode("Output");
        h1_tag.appendChild(h1_tag_text);


        var table_dynamic = document.createElement("table");
        table_dynamic.setAttribute("id", "output_tab_of_gm" + i);
        table_dynamic.setAttribute("class", " main-table chart_tab");
        table_dynamic.setAttribute("style", "width:100%;margin-top:380px;");
        var th_head = document.createElement("thead");
        th_head.setAttribute("id", "th_head_" + i);
        th_head.setAttribute("class", "th_head_" + i);
        var th_spt = document.createElement("th");
        var th_params = document.createElement("th");
        var th_depth = document.createElement("th");
        var th_depth_end = document.createElement("th");
        var th_cu = document.createElement("th");
        var th_ics = document.createElement("th");
        var th_ucmap = document.createElement("th");
        var th_spt_text = document.createTextNode("SPT");
        var th_params_text = document.createTextNode("Parameters");
        var th_cu_text = document.createTextNode("Cu");
        var th_ics_text = document.createTextNode("Ics");
        var th_ucmap_text = document.createTextNode("UcMap");
        var th_depth_text = document.createTextNode("Start");
        var th_depth_text_end = document.createTextNode("End");
        th_spt.appendChild(th_spt_text);
        th_params.appendChild(th_params_text);
        th_depth.appendChild(th_depth_text);
        th_depth_end.appendChild(th_depth_text_end);
        th_cu.appendChild(th_cu_text);
        th_ics.appendChild(th_ics_text);
        th_ucmap.appendChild(th_ucmap_text);

        th_head.appendChild(th_params);
        th_head.appendChild(th_depth);
        th_head.appendChild(th_depth_end);
        th_head.appendChild(th_spt);
        th_head.appendChild(th_cu);

        th_head.appendChild(th_ics);
        th_head.appendChild(th_ucmap);

        table_dynamic.appendChild(th_head);
        console.log(file.get("designData.strataName.gm" + i));
        for (var test_index_val = 0; test_index_val < 4; test_index_val++) {

for(var tt_in = 0; tt_in<file.get("result.gm_"+i+"."+test_valll[test_index_val]+".Stratum.length"); tt_in++)
{
strata_arr[i].push(file.get("result.gm_"+i+"."+test_valll[test_index_val]+".Stratum")[tt_in]);
}
console.log(strata_arr);
        }
        var new_arr = removeDuplicate(strata_arr[i]);
for (var test_index_val = 0; test_index_val < new_arr.length; test_index_val++) {

            // for (var g = 0; g < file.get("result.gm_" + i + "." + test_valll[test_index_val] + ".Stratum.length"); g++) {
                var test;
                var tr = document.createElement("tr");
                // test = file.get("result.gm_" + i + "." + test_valll[test_index_val] + ".Stratum." + g);
                for (var j = 0; j < 7; j++) {

                    if (j == 0) {

                        var td = document.createElement("td");
                        td.setAttribute("class", "param");

                        var val = document.createTextNode(new_arr[test_index_val]);


                        td.appendChild(val);
                        tr.appendChild(td);

                    } else {
                        if (j == 1) {
                            var td = document.createElement("td");
                            td.setAttribute("class", "param");



                            if (typeof file.get("result.gm_" + i + ".SPT.Depth." + new_arr[test_index_val] + ".start") === "undefined") {
                                var val = document.createTextNode("");

                            } else {
                                // var test_stra = file.get("result.gm_" + i + ".Depth.Stratum." + g);
                                var val = document.createTextNode(file.get("result.gm_" + i + ".SPT.Depth." + new_arr[test_index_val] + ".start"));
                            }

                            td.appendChild(val);
                            tr.appendChild(td);
                        }
                        if (j == 2) {
                            var td = document.createElement("td");
                            td.setAttribute("class", "param");



                            if (typeof file.get("result.gm_" + i + ".SPT.Depth." + new_arr[test_index_val] + ".end") === "undefined") {
                                var val = document.createTextNode("");

                            } else {
                                // var test_stra = file.get("result.gm_" + i + ".Depth.Stratum." + g);
                                var val = document.createTextNode(file.get("result.gm_" + i + ".SPT.Depth." + new_arr[test_index_val] + ".end"));
                            }

                            td.appendChild(val);
                            tr.appendChild(td);
                        }
                        if (j == 3) {
                            var td = document.createElement("td");
                            td.setAttribute("class", "param");



                            if (typeof file.get("result.gm_" + i + ".SPT." + new_arr[test_index_val] + ".slope") === "undefined") {
                                var val = document.createTextNode("- ");
                            } else {
                                var val = document.createTextNode(file.get("result.gm_" + i + ".SPT." + new_arr[test_index_val] + ".slope"));
                            }
                            td.appendChild(val);
                            tr.appendChild(td);
                        }
                        if (j == 4) {
                            var td = document.createElement("td");
                            td.setAttribute("class", "param");


                            if (typeof file.get("result.gm_" + i + ".Cu." + new_arr[test_index_val] + ".slope") === "undefined") {
                                var val = document.createTextNode("- ");
                            } else {
                                var val = document.createTextNode(file.get("result.gm_" + i + ".Cu." + new_arr[test_index_val] + ".slope"));
                            }
                            td.appendChild(val);
                            tr.appendChild(td);
                        }
                        if (j == 5) {
                            var td = document.createElement("td");
                            td.setAttribute("class", "param");


                            if (typeof file.get("result.gm_" + i + ".IcS." + new_arr[test_index_val] + ".slope") === "undefined") {
                                var val = document.createTextNode("- ");
                            } else {
                                var val = document.createTextNode(file.get("result.gm_" + i + ".IcS." + new_arr[test_index_val] + ".slope"));
                            }
                            td.appendChild(val);
                            tr.appendChild(td);
                        }
                        if (j == 6) {
                            var td = document.createElement("td");
                            td.setAttribute("class", "param");


                            if (typeof file.get("result.gm_" + i + ".UcMap." + new_arr[test_index_val] + ".slope") === "undefined") {
                                var val = document.createTextNode("- ");
                            } else {
                                var val = document.createTextNode(file.get("result.gm_" + i + ".UcMap." + new_arr[test_index_val] + ".slope"));
                            }

                            td.appendChild(val);
                            tr.appendChild(td);
                        }

                    }

                }

                table_dynamic.appendChild(tr);
            // }
        }


        var test_tabs_div = document.createElement("div");
        test_tabs_div.setAttribute("id", "gm_" + i);
        test_tabs_div.setAttribute("class", "container tab-pane fade");

        var main_div = document.createElement("div");
        main_div.setAttribute("class", "container-fluid");
        for(var t = 1 ; t <= file.get("soilData.groundModelData.noOfGm"); t++){
            // alert(t);
            var dd = document.createElement("h6");
            dd.innerHTML ="GM "+t+" :"+" "+file.get("soilData.groundModelData.gm"+t);
            test_tabs_div.appendChild(dd);

        }


        for (var ii = 0; ii < 3; ii++) {


            var col_div = document.createElement("div");
            var chart_div = document.createElement("div");
            chart_div.setAttribute("id", "chartContainer_" + i + "_" + ii + "");
            chart_div.setAttribute("style", "display: inline-block;");


            col_div.setAttribute("class", "row col-sm-12 ");
            var div_test_but = document.createElement("div");
            div_test_but.setAttribute("class", "row");
            col_div.appendChild(document.createElement("br"));
            if (ii == 2) {
                col_div.appendChild(document.createElement("br"));
                chart_div.setAttribute("class", "row chart_div container-fluid");
                for (var t = 0; t < 4; t++) {
                    var btn = document.createElement("button");
                    btn.setAttribute("onclick", "settest(" + t + "," + i + ")");
                    btn.setAttribute("class", "btn btn-primary");
                    btn.setAttribute("style", "margin-right:10px;");

                    var val = test_arr[t];

                    var text = document.createTextNode(val);
                    btn.appendChild(text);
                    div_test_but.appendChild(btn);
                }


            } else {
                if (ii == 2) {
                    chart_div.setAttribute("class", "row chart_div container-fluid");
                    chart_div.setAttribute("style", "  margin-top:20px;padding-left:200px;");

                    col_div.appendChild(document.createElement("br"));
                } else {
                    chart_div.setAttribute("class", "row chart_div container-fluid");
                    chart_div.setAttribute("style", " display: inline-block; margin-top:20px;");

                    col_div.appendChild(document.createElement("br"));
                }

            }

            col_div.appendChild(div_test_but);
            col_div.appendChild(chart_div);

            test_tabs_div.appendChild(col_div);
            test_tabs_div.appendChild(document.createElement("br"));





        }

        test_tabs_div.appendChild(table_dynamic);

        tabs_body.appendChild(test_tabs_div);
    }
    }






}
var datapoints_gm = new Array();
function removeDuplicate(arr) {
    var c;
    var len = arr.length;
    var result = [];
    var obj = {};
    for (c = 0; c < len; c++) {
        obj[arr[c]] = 0;
    }
    for (c in obj) {
        result.push(c);
    }
    return result;
}
function settest(gg, jj) {
    test_curr = gg;

    load_chart(jj);
}



function show_div(i) {

}
var dps_x =[];
var dps_y =[];




function drawTestLine(chart, gm, test, maxxi_x) {
    // alert("Dasdfs");

    var max_val = 0;
    for (var i = 0; i < file.get("soilData.groundModelData.gm" + gm).length; i++) {
        var bh = file.get("soilData.groundModelData.gm" + gm + "." + i);
        var bh_len = file.get("soilData.params." + bh).length;
        var bh_lent = file.get("soilData.params." + bh).length - 1;
        var bh_lentt = file.get("soilData.params." + bh).length - 2;
        if (file.get("soilData.params." + bh + "." + bh_lent) == "") {
            var bh_Strata = file.get("soilData.params." + bh + "." + bh_lentt);
            var bh_max = file.get("soilData." + bh + ".stratumData." + bh_Strata + ".end");
        } else {
            var bh_Strata = file.get("soilData.params." + bh + "." + bh_lent);
            var bh_max = file.get("soilData." + bh + ".stratumData." + bh_Strata + ".end");
        }
        if (bh_max > max_val) {
            max_val = bh_max;
        }
    }
    console.log(max_val);
    var maxxi = parseInt(max_val) + 10;
    chart.axisY[0].set("maximum", maxxi);
    chart.axisX2[0].set("maximum", parseInt(maxxi_x) + 20);





    console.log(file.get("result.gm_" + gm));
    console.log(file.get("result.gm_" + gm + "." + test_valll[test]));
    console.log(file.get("result.gm_" + gm + "." + test_valll[test] + ".Depth"));
    var t_i = file.get("result.gm_" + gm + "." + test_valll[test] + ".Depth");
    var st_i = file.get("result.gm_" + gm + "." + test_valll[test] + ".Stratum");
    // console.log(t_i);
    var ctx = chart.ctx;
    for (var i = 0; i < st_i.length; i++) {
        ctx.beginPath();
        ctx.strokeStyle = "#000"; //Change Line Color
        ctx.lineWidth = 2; //Change Line Width/Thickness
        var ele = t_i[st_i[i]];
        ctx.moveTo(chart.axisX2[0].convertValueToPixel(ele.x1), chart.axisY[0].convertValueToPixel(ele.y1));
        ctx.lineTo(chart.axisX2[0].convertValueToPixel(ele.x2), chart.axisY[0].convertValueToPixel(ele.y2));
        ctx.stroke();
    }

}

function drawSectionLine(chart, gm) {
    // alert("Dasdfs");
    //    console.log(file.get("result.gm_"+gm));
    console.log(file.get("soilData.sectionData.noOfSection"));
    //    console.log(file.get("result.gm_"+gm+"."+test_valll[test]));
    //    console.log(file.get("result.gm_"+gm+"."+test_valll[test]+".Depth"));
    //    var t_i = file.get("result.gm_"+gm+"."+test_valll[test]+".Depth");
    //    var st_i = file.get("result.gm_"+gm+"."+test_valll[test]+".Stratum");
    // console.log(t_i);
    // chart.render();
    var ctx = chart.ctx;
    var st_i = file.get("soilData.sectionData.noOfSection");
    console.log(st_i);
    for (var i = 0; i < st_i; i++) {
        var ele = file.get("sectionLines.Coordinates." + i);
        // for(var j = 0 ; j<file.get("soilData.section_"+i).length;j++){
if(ele){
    console.log(ele);
    // console.log(chart.axisX2[0].convertPixelToValue(ele.x1));
    ctx.beginPath();
    ctx.strokeStyle = "#000"; //Change Line Color
    ctx.lineWidth = 2; //Change Line Width/Thickness
    // var ele = t_i[st_i[i]];convertPixelToValue
    ctx.moveTo(chart.axisX2[0].convertValueToPixel(ele.x1), chart.axisY[0].convertValueToPixel(ele.y1));
    ctx.lineTo(chart.axisX2[0].convertValueToPixel(ele.x2), chart.axisY[0].convertValueToPixel(ele.y2));
    ctx.stroke();
}
        // }

    }

}