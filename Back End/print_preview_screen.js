var ipcRenderer = require('electron').ipcRenderer;
var editJsonFile = require("edit-json-file");
var type_Cart = ["scatter", "rangeColumn", "scatter"];
var axisXtype = ["secondary", "secondary", "secondary"];
var data_points = new Array();
var chart_Axisx2 = new Array();
var path = require('path');
const Swal = require('sweetalert2');
var strips = [];
var strips1 = [];
var y_data_test = [];
var test_valll = ["SPT", "Cu", "Ics", "UcMap"];
const {
    dialog
} = require("electron").remote;
var remote = require('electron').remote;
const {
    BrowserWindow
} = require('electron').remote;
const printer = require('electron-print');
const ipc = require('electron').ipcRenderer;
var print_confirm_but = document.getElementById("print_but_confirm");
var print_but_pdf = document.getElementById("print_but_pdf");
var back_but = document.getElementById("back_but");
var page_content = document.getElementById("page_content");
var title_val;
var range = ['1000', '2000', '3000', '4000', '5000', '6000', '7000', '8000', '9000', '10000', '20000', '30000', '40000', '50000', '60000', '70000', '80000', '90000', '100000', '150000', '100000', '200000', '300000', '400000', '500000', '600000', '700000', '800000', '900000', '1000000'];
var max_x,max_y;
var dps_x =[];
var dps_y =[];
// const printButton = document.getElementById('print-but');
// printButton.addEventListener('click',function(){
// ipc.send('print');
// });
var test_arr = ["SPT", "Cu", "Ics", "UcMap"];
var test_arr_title = ["Depth VS SPT", "Depth Vs Cu", "Depth Vs Ics", "Depth Vs UcMap"];
// console.log(test);
// console.log(test[1]);
function printConfirm() {
    document.getElementById("page_header").style.position = "fixed";
    print_confirm_but.style.visibility = "hidden";
    print_but_pdf.style.visibility = "hidden";
    back_but.style.visibility = "hidden";
    // ipc.send('print');
    // printer.print("Text sent to printer.");
    // BrowserWindow.webContents.print();
    window.print({
        "header":"Computer Generated Output",
        "footer":"vzoft a Unit of vGeo Suite"    });
    document.getElementById("page_header").style.position = "none";
// window.reload();
location.href="print_preview.html";


}



function back_but() {
    console.log("Back");
    // ipc.send('print');
    // printer.print("Text sent to printer.");
    // BrowserWindow.webContents.print();
    // window.print();
    location.href = "seventh.html";

}

function exportPDF() {
    Swal.fire({
        title: 'Chose the Format you Want !',
        input: 'radio',
        allowOutsideClick:false,
        inputOptions:{
            "0":"A3",
            "1":"A4"
        }
    }).then((result) => {
// alert(result.value);
if(result.value){
    document.getElementById("page_header").style.position = "fixed";
    // document.getElementById("page_content").style.marginTop = "350px";
//    document.getElementsByClassName("page").style.marginTop="250px";
    print_confirm_but.style.visibility = "hidden";
    print_but_pdf.style.visibility = "hidden";
    back_but.style.visibility = "hidden";
    if(result.value == '0'){
        ipc.send('print','A3');
        ipcRenderer.on('pdf-status', (event, arg) => {
            // console.log(arg)
            console.log("fadfasdf");
            if(arg == 'error'){
                Swal.fire({
                    // position: 'top-end',
                    icon: 'error',
                    title: 'Something is wrong',
                    showConfirmButton: false,
                    timer: 1500
                   })
            }else{
                Swal.fire({
                    // position: 'top-end',
                    icon: 'success',
                    title: 'PDF Exported',
                    showConfirmButton: false,
                    timer: 1500
                   })
            }
         })
    }else{
        ipc.send('print','A4');
        ipcRenderer.on('pdf-status', (event, arg) => {
            // console.log(arg)
            if(arg == 'error'){
                Swal.fire({
                    // position: 'top-end',
                    icon: 'error',
                    title: 'Something is wrong',
                    showConfirmButton: false,
                    timer: 1500
                   })
            }else{
                Swal.fire({
                    // position: 'top-end',
                    icon: 'success',
                    title: 'PDF Exported',
                    showConfirmButton: false,
                    timer: 1500
                   })
            }
         })
    }

}else{
   Swal.fire({
    // position: 'top-end',
    icon: 'warning',
    title: 'You Should Chose any one !',
    showConfirmButton: false,
    timer: 1500
   })
}
    });

    // document.getElementById("page_header").style.position = "none";
    // location.href="print_preview.html";
    // printer.print("Text sent to printer.");
    // BrowserWindow.webContents.print();
    // window.print();




// dialog.showSaveDialog({
//     title:"Create a New vGeo Sigma Soil Profile",
//     filters: [{
//         name: "vGeo",
//         extensions: ['vg']

//     }]
// }, function (filename, filePath, options) {
//     if (filename == undefined) {
//         return;
//     } else {
//         // console.log(filePath);
//         // console.log(options);
//         console.log(path.parse(filename).base);
//         // let data = fs.readFileSync(tempFilePath);
//         // // fs.readFileSync(tempFilePath,'utf-8',(err,data)=>{
//         // //     if(err){

//         // //     }
//         // //      content = data;
//         // let content = JSON.parse(data);
//         // console.log(content);
//         // let writedata = JSON.stringify(content);
//         // //   });
//         // fs.writeFileSync(filename, writedata);
//         // //  fs.writeFile(filename,content,(err)=>{
//         // //  console.log(err);
//         // //  console.log(content);
//         // //      })
//         // console.log(filePath, filename);
//         // //  remote.getGlobal('path_file') = ;
//         // ipcRenderer.send("setPath", filename);

//         // // Read MyGlobalVariable.
//         // // remote.getGlobal( "path_file" ); // => "Hi There!"
//         // //  var ipcRenderer = require('electron').ipcRenderer;
//         // //  ipcRenderer.send('show-prop1');
//         // console.log(remote.getGlobal('path_file'));
//         // tempFilePath = filename;
//         // submit();

//     }

// });
}
var test_curr = 0;
var title_chart = ["Borehole Plot", "Geographic Plot", "Test Plot"];
var table = document.getElementById("res_table");
var remote = require('electron').remote;
const app = remote.app;

// var tempFilePath = app.getPath('temp');
var tempFilePath = remote.getGlobal('path_file');
const CanvasJS = require('./assets/javascript/canvasjs.min.js');
let file = editJsonFile(tempFilePath);

var type_of_modee = file.get("projectDetails.typeOfMode");

if (type_of_modee == "mbgl") {
    title_val = "Depth (mbgl)";
} else {
    title_val = "Elevation (mOD)";
}

// var gm_bh = file.get()
var dp = [{
        x: 10,
        y: 71
    },
    {
        x: 20,
        y: 55
    },
    {
        x: 30,
        y: 50
    },
    {
        x: 40,
        y: 65
    },
    {
        x: 50,
        y: 95
    },
    {
        x: 60,
        y: 68
    },
    {
        x: 70,
        y: 28
    },
    {
        x: 80,
        y: 34
    },
    {
        x: 90,
        y: 14
    }
];

var dp4 = [{
        x: 20,
        y: [-9.7, 10.4]
    }, // y: [Low ,High]
    {
        x: 20,
        y: [-8.7, 16.5]
    },
    {
        x: 20,
        y: [-3.5, 19.4]
    },
    {
        x: 30,
        y: [5.0, 22.6]
    },
    {
        x: 30,
        y: [7.9, 29.5]
    },
    {
        x: 30,
        y: [9.2, 34.7]
    },
    {
        x: 40,
        y: [7.3, 30.5]
    },
    {
        x: 40,
        y: [4.4, 25.5]
    },
    {
        x: 40,
        y: [-3.1, 20.4]
    },
    {
        x: 50,
        y: [-5.2, 15.4]
    },
    {
        x: 50,
        y: [-13.5, 9.8]
    },
    {
        x: 50,
        y: [-15.5, 5.8]
    }
];
var dp3 = {

    gridDashType: "dot",
    gridThickness: 0.1,
    // tickLength: 15,
    minimum:0,
    viewportMaximum:60,
    // tickColor: "red",
    crosshair: {
        enabled: true //disable here
    }
};
var dp10 = {

    gridDashType: "dot",
    gridThickness: 0.1,
    // tickLength: 15,
    minimum :0,
    viewportMaximum:250,
    // tickColor: "red",
    crosshair: {
        enabled: true //disable here
    }
};
var dp11 = {

    gridDashType: "dot",
    gridThickness: 0.1,
    minimum :0,
    viewportMaximum:50,
    // tickLength: 15,
    // tickColor: "red",
    crosshair: {
        enabled: true //disable here
    }
};
var dp12 = {

    gridDashType: "dot",
    gridThickness: 0.1,
    minimum :0,
    viewportMaximum:100,
    // tickLength: 15,
    // tickColor: "red",
    crosshair: {
        enabled: true //disable here
    }
};
var dp6 = {
    title: "Eastings",
    titleFontSize: 12,
    labelFontSize: 8,
    titleFontStyle: "oblique",
    titleFontColor: "black",
    labelFontColor: "black",
    gridDashType: "dot",
    gridThickness: 0.1,
    minimum:0,
    // tickLength: 15,
    // tickColor: "red",
    crosshair: {
        enabled: true //disable here
    }
};
var dp7 = {
    title: "Chainage",
    titleFontSize: 12,
    labelFontSize: 8,
    titleFontStyle: "oblique",
    titleFontColor: "black",
    labelFontColor: "black",
    gridDashType: "dot",
    gridThickness: 0.1,
    // tickLength: 15,
    // tickColor: "red",
    crosshair: {
        enabled: true //disable here
    }
};
data_points.push(dp);
data_points.push(dp);
data_points.push(dp4);
chart_Axisx2.push(dp6);
chart_Axisx2.push(dp7);
chart_Axisx2.push(dp3);
chart_Axisx2.push(dp10);
chart_Axisx2.push(dp11);
chart_Axisx2.push(dp12);


// window.onload = function () {

// }
function get_data() {
// alert("ll");
    for (var i = 1; i <= file.get("soilData.groundModelData.noOfGm"); i++) {
        if(file.get("soilData.groundModelData.gm"+i)){


        strips[i] = new Array();
        strips1[i] = new Array();
        load_chart(i);
        }
    }



}


var datapoints_gm = new Array();

function settest(gg, jj) {
    test_curr = gg;
    // alert(test_curr);
    // load_chart(jj);
}



function show_div(i) {
    // alert(i);
    // $("#gm"+i).show();
    // document.getElementById("gm"+i).style.display = "block";
}
function data_get(limit) {
    var dps = [];

    for (var j = 1; j <= file.get("soilData.noOfBh"); j++) {
        var jj = j - 1;
        var bh = file.get("soilData.boreholeArray." + jj + "");
        var x_val = parseInt(file.get("soilData." + bh + ".stratumData.Eastings"), 10);
        var y_val = parseInt(file.get("soilData." + bh + ".stratumData.Northings"), 10);
        // alert(x_val+','+y_val);
        dps.push({
            // indexLabel: String(bh),
            name_to_call: String(bh),
            markerImageUrl: "./assets/img/bh.png",
            indexLabelFontSize: 12,
            x: x_val,

            markerSize: 5,

            y: y_val

        });
        dps_x.push(parseInt(x_val, 10));
        dps_y.push(parseInt(y_val, 10));


        // var points = [];




        // }
        // console.log(points);
        // return points;


    }
    console.log(dps_x);
    // var chart_min_x = Math.min(...dps_x);
    // var chart_min_y = Math.min(...dps_y);
    var chart_max_y = Math.max(...dps_y);
    var chart_max_x = Math.max(...dps_x);

    max_x = chart_max_x;
    max_y = chart_max_y;
    // min_x = chart_min_x;
    // min_y = chart_min_y;
    // console.log(min_x);
    // console.log(min_x / 10);
    // console.log(min_y);
    // console.log(min_y / 10);
    // console.log(max_x);
    // console.log(max_x / 10);
    // console.log(max_y);
    // console.log(max_y / 10);
    // chart.axisY[0].set("maximum", max_y);
    // chart.axisX2[0].set("maximum", max_x);
    // chart.axisY[0].set("minimum", min_y);
    // chart.axisX2[0].set("minimum", min_x);
    // chartt();
    return dps;

}
function set_scale_chart_test(chart) {
    if (max_x > max_y) {
        var t;
        // alert("X is Maximum");
        for (var i = 0; i < range.length; i++) {
            if (range[i] > max_x) {
                // alert(range[i]);
                t = range[i];
                break;
            }

        }
        // alert(t);
        // chart.axisY[0].set("maximum",3000000)
        chart.axisY[0].set("viewportMaximum", t);
        chart.axisX2[0].set("viewportMaximum", t * 3);
        chart.axisX2[0].set("interval", chart.axisY[0].get("interval"));
    } else if (max_y > max_x) {
        // alert("Y is Maximum");
        for (var i = 0; i < range.length; i++) {
            if (range[i] > max_y) {
                // alert(range[i]);
                t = range[i];
                break;
            }

        }
        chart.axisY[0].set("viewportMaximum", t);
        chart.axisX2[0].set("viewportMaximum", t * 3);
        chart.axisX2[0].set("interval", chart.axisY[0].get("interval"));
    } else {
        for (var i = 0; i < range.length; i++) {
            if (range[i] > max_y) {
                // alert(range[i]);
                t = range[i];
                break;
            }

        }
        chart.axisY[0].set("viewportMaximum", t);
        chart.axisX2[0].set("viewportMaximum", t * 3);
        chart.axisX2[0].set("interval", chart.axisY[0].get("interval"));
    }
}

function drawTestLine(chart, gm, test) {
    // alert(test_valll[test-3]);

    var max_val = 0;
    for (var i = 0; i < file.get("soilData.groundModelData.gm" + gm).length; i++) {
        var bh = file.get("soilData.groundModelData.gm" + gm + "." + i);
        var bh_len = file.get("soilData.params." + bh).length;
        var bh_lent = file.get("soilData.params." + bh).length - 1;
        var bh_lentt = file.get("soilData.params." + bh).length - 2;
        if (file.get("soilData.params." + bh + "." + bh_lent) == "") {
            var bh_Strata = file.get("soilData.params." + bh + "." + bh_lentt);
            var bh_max = file.get("soilData." + bh + ".stratumData." + bh_Strata + ".end");
        } else {
            var bh_Strata = file.get("soilData.params." + bh + "." + bh_lent);
            var bh_max = file.get("soilData." + bh + ".stratumData." + bh_Strata + ".end");
        }
        if (bh_max > max_val) {
            max_val = bh_max;
        }
    }
    console.log(max_val);
    var maxxi = parseInt(max_val) + 10;
    chart.axisY[0].set("maximum", maxxi);
    // chart.axisX2[0].set("maximum", parseInt(maxxi_x) + 20);





    console.log(file.get("result.gm_" + gm));
    console.log(file.get("result.gm_" + gm + "." + test_valll[test-3]));
    console.log(file.get("result.gm_" + gm + "." + test_valll[test-3] + ".Depth"));
    var t_i = file.get("result.gm_" + gm + "." + test_valll[test-3] + ".Depth");
    var st_i = file.get("result.gm_" + gm + "." + test_valll[test-3] + ".Stratum");
    if(typeof st_i === "undefined"){

    }else{
        var ctx = chart.ctx;
        for (var i = 0; i < st_i.length; i++) {
            ctx.beginPath();
            ctx.strokeStyle = "#000"; //Change Line Color
            ctx.lineWidth = 2; //Change Line Width/Thickness
            var ele = t_i[st_i[i]];
            ctx.moveTo(chart.axisX2[0].convertValueToPixel(ele.x1), chart.axisY[0].convertValueToPixel(ele.y1));
            ctx.lineTo(chart.axisX2[0].convertValueToPixel(ele.x2), chart.axisY[0].convertValueToPixel(ele.y2));
            ctx.stroke();
        }
    }
    // console.log(t_i);


}


function drawSectionLine(chart, gm) {
    // alert("Dasdfs");
    //    console.log(file.get("result.gm_"+gm));
    console.log(file.get("soilData.sectionData.noOfSection"));
    //    console.log(file.get("result.gm_"+gm+"."+test_valll[test]));
    //    console.log(file.get("result.gm_"+gm+"."+test_valll[test]+".Depth"));
    //    var t_i = file.get("result.gm_"+gm+"."+test_valll[test]+".Depth");
    //    var st_i = file.get("result.gm_"+gm+"."+test_valll[test]+".Stratum");
    // console.log(t_i);
    // chart.render();
    var ctx = chart.ctx;
    var st_i = file.get("soilData.sectionData.noOfSection");
    console.log(st_i);
    for (var i = 0; i < st_i; i++) {
        var ele = file.get("sectionLines.Coordinates." + i);
        // for(var j = 0 ; j<file.get("soilData.section_"+i).length;j++){

        // }
        console.log(ele);
        console.log(chart.axisX2[0].convertPixelToValue(ele.x1));
        ctx.beginPath();
        ctx.strokeStyle = "#000"; //Change Line Color
        ctx.lineWidth = 2; //Change Line Width/Thickness
        // var ele = t_i[st_i[i]];convertPixelToValue
        ctx.moveTo(chart.axisX2[0].convertValueToPixel(ele.x1), chart.axisY[0].convertValueToPixel(ele.y1));
        ctx.lineTo(chart.axisX2[0].convertValueToPixel(ele.x2), chart.axisY[0].convertValueToPixel(ele.y2));
        ctx.stroke();
    }

}